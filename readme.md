# Metacat Customisation Patch

Customised KNB Metacat by adding:

1. A custom skin called 'tern' that supports adding DOI and QC
2. A OAI-PMH provider that support RIF-CS crosswalk

## Installation

Make sure Tomcat is stopped.
Prepare parent directory for the metacat-patch source:

    mkdir /var/src
    sudo chmod 777 /var/src
    cd /var/src

Clone the repository:

    sudo -u tomcat git clone https://bitbucket.org/asn-ltern/metacat-patch.git
    # or
    sudo -u tomcat git pull

Run Ant and adjust the metacat.dir property and all the paths accordingly:

    cd metacat-patch
    sudo -u tomcat ant -f patch.xml -Dmetacat.dir=/var/lib/tomcat/webapps/knb
    

## Configuration

Copy the context.xml.example to the Tomcat config directory and change the settings accordingly.
Make sure that the path specified in tern.layout.url property is valid and the template file exists.
Otherwise, use default template by deleting the property from the context.xml.
The template file provides the "parent" layout, structure, and appearance that can be used 
to customise the look and feel of Metacat. An example of the file is provided in `web/style/skins/tern/layout.jsp`.
The template file must be a valid XML (syntax-wise) and contains a div tag with id `metacatContainer`:

    <div id="metacatContainer"></div>

in which the all the Metacat generated contents will be put inside the div tag.

Start Tomcat.
To refresh the template after any modification, go to the url `<domain or ip address>/<metacat context>?refresh`
For example: http://www.aceas.org.au/knb?refresh