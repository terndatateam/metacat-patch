<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:exslt="http://exslt.org/common">
>

  <!-- the registry object group -->
  <xsl:param name="groupName">TERN</xsl:param>
  <xsl:param name="emlNamespace">eml://ecoinformatics.org/eml-2.1.1</xsl:param>
  <xsl:param name="repositoryIdentifier"/>
  <!--xsl:param name="serverUrl"/-->
  <!--xsl:param name="contextUrl"/-->
  <xsl:param name="servletUrl"/>
  <xsl:param name="lastModified" />
  <xsl:param name="dateCreated" />
  <xsl:param name="predefinedLicences"/>
  <xsl:param name="defaultLicence"/>
  
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />

  <xsl:strip-space elements="*" />
  
  <xsl:variable name="packageId" select="/*[local-name()='eml']/@packageId"/>
  <xsl:variable name="datasetUrl" select="concat($servletUrl,'/', $packageId ,'/html')"/>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@packageId">
    <xsl:attribute name="packageId">
      <xsl:value-of select="substring-after($datasetUrl, '://')"/>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template match="dataset/creator[last()]">
    <xsl:copy-of select="."/>
    <xsl:copy-of select="../metadataProvider"/>
    <xsl:copy-of select="../associatedParty"/>
    <xsl:element name="pubDate">
      <xsl:value-of select="../pubDate"/>
      <xsl:if test="not(normalize-space(../pubDate))">
        <xsl:value-of select="$dateCreated"/>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <xsl:template match="dataset/contact[1]">
    <xsl:element name="distribution">
      <xsl:element name="online">
        <xsl:element name="url">
          <xsl:value-of select="$datasetUrl" />
        </xsl:element>
      </xsl:element>
    </xsl:element>
    <xsl:copy-of select="../distribution"/>
    <xsl:copy-of select="../coverage"/>
    <xsl:copy-of select="../purpose"/>
    <xsl:copy-of select="../maintenance"/>
    <xsl:copy-of select="."/>
  </xsl:template>
  
  <xsl:template match="dataset/metadataProvider|dataset/associatedParty|dataset/pubDate">
  </xsl:template>
  
  <xsl:template match="dataset/distribution|dataset/coverage|dataset/purpose|dataset/maintenance">
  </xsl:template>
  
  <xsl:template match="access">
  </xsl:template>
  
  <xsl:template match="dataTable/physical/distribution/online/url">
    <xsl:element name="url">
      <xsl:attribute name="function">download</xsl:attribute>

      <xsl:choose>
        <xsl:when test="starts-with(.,'ecogrid://knb/')">
          <xsl:value-of select="$datasetUrl" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="." />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>

  <xsl:template match="intellectualRights/para">
    <xsl:element name="para">
      <!--parse the first line of licence paragraph-->
      <xsl:variable name="firstLine" select="normalize-space(substring-before(concat(., '&#x0A;'), '&#x0A;'))"/>
      <xsl:variable name="licenceCode">
        <xsl:choose>
          <xsl:when test="$firstLine!=''">
            <xsl:choose>
              <xsl:when test="string-length($firstLine) &lt; 25">
                <xsl:value-of select="translate($firstLine, $uppercase, $smallcase)" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>Other</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$defaultLicence" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="licences" select="exslt:node-set($predefinedLicences)"/>
      <xsl:variable name="licence1" select="$licences//*[local-name()=$licenceCode]"/>
      <xsl:variable name="licence2">
        <xsl:if test="not($licence1)">
          <xsl:variable name="licenceText" select="."/>
          <xsl:for-each select="$licences/*">
            <xsl:variable name="uriPart" select="substring-after(@uri, '://')"/>
            <xsl:if test="contains($licenceText, $uriPart)">
              <xsl:copy-of select="." />
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
      </xsl:variable>
      <xsl:variable name="licence" select="$licence1 | exslt:node-set($licence2)/*"/>

      <xsl:choose>
        <xsl:when test="$licence1">
          <xsl:value-of select="$licence"/>
          <xsl:value-of select="substring-after(., $firstLine)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="."/>
          <xsl:if test=".=''">
            <xsl:text>Permission required from data owner</xsl:text>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
      
    </xsl:element>
  </xsl:template>
  
</xsl:stylesheet>
