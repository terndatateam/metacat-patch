<%@ page language="java" trimDirectiveWhitespaces="true" %>
<%@ include file="settings.jsp"%>
<%@ include file="session_vars.jsp"%>
<%@ include file="refresh.jsp"%>
<%
pageContext.setAttribute("title", "Home");
pageContext.setAttribute("showSearch", true);
pageContext.setAttribute("showMap", true);
%>
<!DOCTYPE html>
<%@ include file="template_common.jsp"%>