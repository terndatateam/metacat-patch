<%@ page language="java" trimDirectiveWhitespaces="true"%>
<div class="login-box">
  <!--h1>Login</h1-->
  <div class="login-alert alert alert-danger hidden" role="alert">
    <button type="button" class="close"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>  
    <span class="message-denied hidden">Incorrect username or password. Please try again</span>
    <span class="message-problem hidden">ERROR logging in: connection problems. Please try again later</span>
  </div>
  <div class="request-account"><a target="_blank" href="<%=REQUEST_ACCOUNT_URL%>">Request an account</a></div>          
  <div class="login-status">
    <span class="message-login hidden">You ARE logged in as <span class="message-login-username"></span>.</span>
    <span class="message-logout">You are NOT logged in.</span>
  </div>
  <div class="login-control">
    <span class="login-btn-modal"><a href="#" class="btn btn-default">Sign in</a></span>
    <form class="form-inline" role="form" id="loginform" name="loginform" method="post" action="<%=SERVLET_URL%>">
      <input type="hidden" name="organization" value="<%=ORGANIZATION%>" />
      <input type="hidden" name="ldapUserPattern" value="<%=LDAP_PATTERN%>" />
      <div class="formpart-login">
        <div class="form-group">
          <label class="sr-only" for="username">Username</label>
          <input type="text" class="form-control" id="username" name="username" value="" placeholder="Username"/>
        </div>
        <div class="form-group">
          <label class="sr-only" for="password">Password</label>
          <input type="password" class="form-control" id="password" name="password"  maxlength="50" value="" placeholder="Password"/>
        </div>
        <input type="submit" class="btn btn-default btn-login" name="btnlogin" value="Sign in"/>
      </div>
      <div class="formpart-logout">
        <input type="submit" class="btn btn-default btn-logout hidden" name="btnlogout" value="Sign out"/>
      </div>
      <button type="button" class="btn btn-default login-wait disabled hidden"><img src="<%=SKIN_URL%>/images/spinner.gif" height="16" width="16"/></button>
    </form>
  </div>
</div>