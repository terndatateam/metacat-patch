<%@ page import="javax.servlet.http.*,edu.ucsb.nceas.metacat.properties.*,edu.ucsb.nceas.metacat.util.SystemUtil" %>
<%
/*
 *     Authors: Alvin Sebastian
 *   Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
 *
 * Settings file for the default metacat skin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
%>
<%@ include file="../../common/configure-check.jsp"%>
<%

// GLOBAL CONSTANTS FOR KNB PORTAL PAGE /////////////////////////////////////

// if true, POST variables echoed at bottom of client's browser window in a big yellow box
// Set this value to override the global value
boolean DEBUG_TO_BROWSER = false;

// Choose simple or advanced search. Simple search uses UNION operator while advance search uses INTERSECT operator.
String SEARCH_TYPE = "simple";

// show map on main page, default value is overriden by the property setting `spatial.runSpatialOption`
boolean SHOW_MAP = true;

// the pattern of LDAP username
String LDAP_PATTERN = "uid=username,o=organization,dc=ecoinformatics,dc=org";

String KNB_SITE_URL = "http://knb.ecoinformatics.org";
String CGI_DIR = "";
String CONTEXT_NAME = "knb";
String GEOSERVER_CONTEXT_NAME = "geoserver";
String DEFAULT_STYLE = "tern";
String REPOSITORY_NAME = "TERN";
String REPOSITORY_URL = "";
boolean TOOLS_DOI = false;
boolean TOOLS_QC = false;
boolean USE_SIMPLE_URL = false;
boolean USE_RELATIVE_URL = false;

String SEARCH_OPT_CLASS = "";
String SEARCH_OPT_ALL = "";
String SEARCH_OPT_QUICK = "checked=\"\"";

try {
  SHOW_MAP = PropertyService.getProperty("spatial.runSpatialOption").toLowerCase().equals("true");
} catch (Exception e) {}
try {
  LDAP_PATTERN = "uid=username,o=organization," + PropertyService.getProperty("auth.base");
} catch (Exception e) {}
try {
  KNB_SITE_URL = PropertyService.getProperty("application.knbSiteURL");
} catch (Exception e) {}
try {
  CGI_DIR = PropertyService.getProperty("application.cgiDir");
} catch (Exception e) {}
try {
  CONTEXT_NAME = PropertyService.getProperty("application.context");
} catch (Exception e) {}
try {
  GEOSERVER_CONTEXT_NAME = PropertyService.getProperty("geoserver.context");
} catch (Exception e) {}
try {
  DEFAULT_STYLE = PropertyService.getProperty("application.default-style");
} catch (Exception e) {}
try {
  REPOSITORY_NAME = PropertyService.getProperty("Identify.repositoryName");
} catch (Exception e) {}
try {
  TOOLS_DOI = PropertyService.getProperty("tern.doi.enabled").toLowerCase().equals("true");
} catch (Exception e) {}
try {
  TOOLS_QC = PropertyService.getProperty("tern.qc.enabled").toLowerCase().equals("true");
} catch (Exception e) {}
try {
  USE_SIMPLE_URL = PropertyService.getProperty("tern.useSimpleUrl").toLowerCase().equals("true");
} catch (Exception e) {}
try {
  USE_RELATIVE_URL = PropertyService.getProperty("tern.useRelativeUrl").toLowerCase().equals("true");
} catch (Exception e) {}

String LAYOUT_CONTAINERID = "metacatContainer";
String SITE_TITLE = REPOSITORY_NAME + " Data Portal";

try {
  LAYOUT_CONTAINERID = PropertyService.getProperty("tern.layout.containerId");
} catch (Exception e) {
  try {
    LAYOUT_CONTAINERID = SkinPropertyService.getProperty(DEFAULT_STYLE, "layout.containerId");
  } catch (Exception e1) {}
}
try {
  SITE_TITLE = PropertyService.getProperty("tern.layout.siteTitle");
} catch (Exception e) {
  try {
    SITE_TITLE = SkinPropertyService.getProperty(DEFAULT_STYLE, "layout.siteTitle");
  } catch (Exception e1) {}
}
try {
  String mode = PropertyService.getProperty("tern.layout.searchMode");
  if (mode.equals("all")) {
    SEARCH_OPT_CLASS = "class=\"hidden\"";
    SEARCH_OPT_ALL = "checked=\"\"";
    SEARCH_OPT_QUICK = "";
  } else {
    SEARCH_OPT_CLASS = "class=\"hidden\"";
    SEARCH_OPT_QUICK = "checked=\"\"";
    SEARCH_OPT_ALL = "";
  }
} catch (Exception e) {
}

String ABS_SERVER_URL = "";
try {
  ABS_SERVER_URL = SystemUtil.getServerURL();
} catch (Exception e) {
}

final String LOCAL_URL = "http://localhost";
final String SERVER_URL = USE_RELATIVE_URL ? "" : ABS_SERVER_URL;
final String CGI_URL = SERVER_URL + "/" + CGI_DIR; //SystemUtil.getCGI_URL();
final String CONTEXT_URL = SERVER_URL + "/" + CONTEXT_NAME; //SystemUtil.getContextURL();
final String ABS_CONTEXT_URL = ABS_SERVER_URL + "/" + CONTEXT_NAME; //SystemUtil.getContextURL();
final String SERVLET_URL = CONTEXT_URL + "/metacat";//SystemUtil.getServletURL();
final String STYLE_SKINS_URL = CONTEXT_URL + "/style/skins"; //SystemUtil.getStyleSkinsURL();
final String STYLE_COMMON_URL = CONTEXT_URL + "/style/common"; //SystemUtil.getStyleCommonURL();
final String DEFAULT_STYLE_URL = STYLE_COMMON_URL + "/" + DEFAULT_STYLE; //SystemUtil.getDefaultStyleURL();
final String GEOSERVER_URL = SERVER_URL + "/" + GEOSERVER_CONTEXT_NAME;
final String ORGANIZATION = "unaffiliated";
final String SKIN_NAME = DEFAULT_STYLE;
final String SKIN_URL = STYLE_SKINS_URL + "/" + SKIN_NAME;
final String MAIN_SITE_URL = SERVER_URL + "/";
final String PERMISSION_URL = SKIN_URL + "/permission.jsp";

try {
  REPOSITORY_URL = PropertyService.getProperty("server.url") + "/" + CONTEXT_NAME;
} catch (Exception e) {}

String LAYOUT_URL = SKIN_URL + "/layout.jsp";
String REQUEST_ACCOUNT_URL = SKIN_URL + "/request_account.jsp";
String REQUEST_ACCESS_URL = SKIN_URL + "/request_access.jsp";
String FAIRUSEPOLICY_URL = "";
String LICENCES_URL = CONTEXT_URL + "/licences";
String DEFAULT_LICENCE = "";

try {
  LAYOUT_URL = PropertyService.getProperty("tern.layout.url");
} catch (Exception e) {
  try {
    LAYOUT_URL = SkinPropertyService.getProperty(DEFAULT_STYLE, "layout.url");
  } catch (Exception e1) {
  }
}
if (!LAYOUT_URL.toLowerCase().startsWith("http")) {
  if (LAYOUT_URL.startsWith("/")) LAYOUT_URL = LOCAL_URL + LAYOUT_URL;
  else LAYOUT_URL = LOCAL_URL + SKIN_URL + "/" + LAYOUT_URL;
}
try {
  REQUEST_ACCOUNT_URL = PropertyService.getProperty("tern.url.requestAccount");
} catch (Exception e) {}
try {
  REQUEST_ACCESS_URL = PropertyService.getProperty("tern.url.requestAccess");
} catch (Exception e) {}
try {
  FAIRUSEPOLICY_URL = PropertyService.getProperty("tern.url.fairUsePolicy");
} catch (Exception e) {}
try {
  LICENCES_URL = CONTEXT_URL + "/" + PropertyService.getProperty("tern.licencesDir");
} catch (Exception e) {}
try {
  DEFAULT_LICENCE = PropertyService.getProperty("tern.defaultLicence");
} catch (Exception e) {}


pageContext.setAttribute("MAIN_SITE_URL", MAIN_SITE_URL);

String DATA_FILE_PATH = "/var/metacat/data";
try {
  DATA_FILE_PATH = PropertyService.getProperty("application.datafilepath");
} catch (Exception e) {}

final String R_SCHEME = request.getScheme();
final String R_HOST = request.getServerName();
//import="java.util.Vector,edu.ucsb.nceas.metacat.properties.PropertyService,edu.ucsb.nceas.metacat.util.OrganizationUtil"
//Vector<String> organizationList = OrganizationUtil.getOrganizations();

%>
