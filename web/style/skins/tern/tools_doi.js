jQuery(function($){
  //console.log('tools doi');
  //if ($('#metadataTools').length === 0) return;
  var doiUrl = metacat.url.skin + '/doi.jsp';
  var landingPage = metacat.url.servlet + '/' + metacat.docidNoRev + '/html';
  var $modal = $('#toolsModal');
  var $modalTitle = $('#toolsModalTitle');
  var $modalBody = $('.modal-body-main', $modal);
  var $modalBodyWait = $('.modal-body-wait', $modal);
  var $okButton = $('.btn-primary', $modal);
  
  function queryTernDoi(cb) {
    var postData = {
      action : 'query',
      docid : metacat.docid
    };
    $.ajax({type:"POST", url:doiUrl, data:postData}).always(function(res, status, obj){
      var el, dm, err, i, j, nameparts;
      if (status == 'success') {
        if ((el = res.getElementsByTagName('identifier')).length) {
          dm = { creators : [] };
          dm.doi = el[0].textContent;
          if ((el = res.getElementsByTagName('title')).length) dm.title = el[0].textContent;
          if ((el = res.getElementsByTagName('publisher')).length) dm.publisher = el[0].textContent;
          if ((el = res.getElementsByTagName('date')).length) dm.pubdate = el[0].textContent;
          var name;
          var contributors = res.getElementsByTagName('contributor');
          for (i=0; i<contributors.length; ++i) {
            nameparts = contributors[i].getElementsByTagName('namePart');
            name = {};
            for (j=0; j<nameparts.length; ++j) {
              name[nameparts[j].getAttribute('type')] = nameparts[j].textContent;
            }
            dm.creators.push(name);
          }
        }
      } else {
        err = new Error(obj);
      }
      return cb(err, dm);
    });
  }
  
  function createTernDoi(title, creators, publisher, year, cb) {
    //$.ajax({type:"POST", url:doiUrl, data:postData}).always(function(res, status, obj){
    var params = createDoiParams('create', title, creators, publisher, year);
    $.ajax({type:"POST", url:doiUrl, data:params}).always(function(res, status, obj){
      var err;
      if (status == 'success') {
        try {
          params.doi = res.getElementsByTagName('doi')[0].textContent;
          return updateEml(params, cb);
        } catch (ex) {
          err = ex;
        }
      } else {
        err = new Error(obj);
      }
      return cb(err);
    });
  }
  
  function updateTernDoi(title, creators, publisher, year, cb) {
    var params = createDoiParams('update', title, creators, publisher, year);
    $.ajax({type:"POST", url:doiUrl, data:params}).always(function(res, status, obj){
      var err;
      if (status == 'success') {
        try {
          params.doi = res.getElementsByTagName('doi')[0].textContent;
          return updateEml(params, cb);
        } catch (ex) {
          err = ex;
        }
      } else {
        err = new Error(obj);
      }
      return cb(err);
    });
  }
  
  function createDoiParams(action, title, creators, publisher, year) {
    return {
      action : action,
      docid : metacat.docid,
      title : title,
      publisher : publisher,
      year : year,
      pubDate : year,
      creator : creators.split(';')
    };
  }
  
  function findDoi(xml) {
    var alternateIdentifiers = xml.getElementsByTagName('alternateIdentifier');
    for (var i = alternateIdentifiers.length; i--; ) {
      var alternateIdentifier = alternateIdentifiers[i];
      if (alternateIdentifier.getAttribute('system') == 'doi') return alternateIdentifier;
    }
  }
  
  function getCreators(xml) {
    var element, gn, sn;
    var str = '';
    var elements = xml.getElementsByTagName('creator');
    for (var i=0; i<elements.length; ++i) {
      element = elements[i];
      gn = element.getElementsByTagName('givenName')[0];
      sn = element.getElementsByTagName('surName')[0];
      if (str.length) str += '; ';
      if (sn && sn.textContent) str += sn.textContent;
      if (gn && gn.textContent) str += (', ' + gn.textContent);
    }
    return str;
  }
  
  function getTextContent(xml, name) {
    var element;
    var elements = xml.getElementsByTagName(name);
    if (elements.length) element = elements[0];
    if (element) return element.textContent;
  }
  
  function setTextContent(xml, name, value) {
    var element, priorElements;
    if (name == 'alternateIdentifier') {
      element = findDoi(xml);
    } else {
      var elements = xml.getElementsByTagName(name);
      if (elements.length) element = elements[0];
      if (name == 'publisher') {
        elements = xml.getElementsByTagName('organizationName');
        if (elements.length) element = elements[0];
        else element = element.appendChild(xml.createElement('organizationName'));
      }
    }
    if (element == null) {
      element = xml.createElement(name);
      var datasetElement =  xml.getElementsByTagName('dataset')[0];
      if (name == 'alternateIdentifier') {
        datasetElement.insertBefore(element, datasetElement.firstElementChild);
        element.setAttribute('system', 'doi');
      } else if (name == 'publisher') {
        priorElements = datasetElement.getElementsByTagName('contact');
        datasetElement.insertBefore(element, priorElements[priorElements.length - 1].nextSibling);
        element = element.appendChild(xml.createElement('organizationName'));
      } else if (name == 'pubDate') {
        priorElements = datasetElement.getElementsByTagName('associatedParty');
        if (priorElements.length === 0) priorElements = datasetElement.getElementsByTagName('metadataProvider');
        if (priorElements.length === 0) priorElements = datasetElement.getElementsByTagName('creator');
        datasetElement.insertBefore(element, priorElements[priorElements.length - 1].nextSibling);
      }
    }
    element.textContent = value;
    return element;
  }
  
  function updateEml(params, cb) {
    metacat.eml.get(function(err, eml){
      var packageId;
      if (err == null && eml != null) {
        try {
          if (params.pubDate) setTextContent(eml, 'pubDate', params.pubDate);
          if (params.publisher) setTextContent(eml, 'publisher', params.publisher);
          setTextContent(eml, 'alternateIdentifier', params.doi);
          packageId = $.trim(eml.documentElement.getAttribute('packageId'));
          if (packageId == params.docid) {
            packageId = metacat.increaseRevId(packageId);
            eml.documentElement.setAttribute('packageId', packageId);
          } else {
            err = new Error('docid is out of sync');
          }
        } catch (ex) {
          err = ex;
        }
      }
      if (err) return cb(err);
      else return metacat.eml.upload(packageId, eml, cb);
    });
  }
  
  
  function selectMintOrUpdate(err, dm) {
    if (err != null) return showResult(err);
    metacat.eml.get(function(err, eml){
      var creators = getCreators(eml);
      var title = getTextContent(eml, 'title');
      var publisher = getTextContent(eml, 'publisher') || metacat.defaultPublisher;
      //var pubPlace = getTextContent(eml, 'pubPlace');
      var pubDate = getTextContent(eml, 'pubDate');
      var currentYear = (new Date()).getFullYear();
      var div = $('<div><div/>');
      var dl = $('<dl></dl>');
      var inputPubDate = $('<input type="text" size="20"/>');
      dl.append('<dt>Landing page URL: </dt><dd>'+landingPage+'</dd>');
      dl.append('<dt>Title: </dt><dd>'+title+'</dd>');
      dl.append('<dt>Creators: </dt><dd>'+creators+'</dd>');
      dl.append('<dt>Publisher: </dt><dd>'+publisher+'</dd>');
      dl.append('<dt>Publication date: </dt>');
      $('<dd></dd>').append(inputPubDate).appendTo(dl);
      if (dm == null) {
        // mint new doi
        $modalTitle.text(oplabels.title + ' - ' + oplabels.mint);
        div.append('<p>Please confirm that you want to mint an new DOI through TERN DOI service with the following metadata:</p>');
        div.append(dl);
        inputPubDate.val(pubDate || currentYear);
        showConfirmation(div, 'Minting new DOI..', function(){
          createTernDoi(title, creators, publisher, inputPubDate.val(), showResult);
        });
      } else {
        // assign existing doi and send update to the doi provider
        $modalTitle.text(oplabels.title + ' - ' + oplabels.update);
        div.append('<p>This dataset has been registered with TERN DOI Service Provider. Minting a new DOI for this dataset is not allowed.</p>');
        div.append('<p>This operation will insert the DOI into the EML metadata record and update the relevant DOI information to TERN DOI Service according to the following details:</p>');
        div.append(dl);
        dl.prepend('<dt>DOI: </dt><dd>'+dm.doi+'</dd>');
        inputPubDate.val(pubDate || dm.pubdate || currentYear);
        showConfirmation(div, 'Updating DOI..', function(){
          updateTernDoi(title, creators, publisher, inputPubDate.val(), showResult);
        });
      }
    });
  }
  
  function showMintDialog(event) {
    event.preventDefault();
    toggleWait(true);
    $modalTitle.text(oplabels.title);
    $modal.modal('show');
    metacat.eml.get(function(err, eml) {
      if (err == null && eml != null) queryTernDoi(selectMintOrUpdate);
      else showResult(err);
    });
  }
  
  function showAddDialog(event) {
    event.preventDefault();
    //console.log('add');
    var inputPubDate = $('<input type="text" size="20" autofocus="autofocus"/>');
    var inputDoi = $('<input type="text" size="60" autofocus="autofocus"/>');
    inputDoi.on('input', function(){
      var doi = $(this).val();
      if (doi.match(/10[.][0-9]{4,}\/.+/)) $okButton.removeClass('disabled');
      //if (doi) $('.modal .btn-primary').removeClass('disabled');
    });
    var labelPubDate = $('<label>Publication date: </label>');
    var labelDoi = $('<label>DOI: </label>');
    labelPubDate.append(inputPubDate);
    labelDoi.append(inputDoi);
    
    toggleWait(false);
    $modalTitle.text(oplabels.title + ' - ' + oplabels.add);
    $modalBody.empty().
      append('<p>Assign an existing DOI to this dataset: <strong>' + metacat.docid + '</strong></p>').
      append('<p>Please make sure that the existing DOI will resolve to: <strong><a target="_blank" href="' + landingPage + '">' + landingPage + '</a></strong></p>').
      append('<p>Please enter the DOI number and confirm the publication date.</p>').
      append(labelDoi).
      append('<br/>').
      append(labelPubDate);
    $okButton.addClass('disabled');
    $okButton.off('click');
    $okButton.one('click', function(){
      var doi = $.trim(inputDoi.val());
      var pubDate = $.trim(inputPubDate.val());
      //console.log(doi);
      showConfirmation('<p>Are you sure you want to add DOI ' + doi + ' to ' + metacat.docidNoRev + '?</p>', 'Adding existing DOI..', function() {
        updateEml({docid:metacat.docid, doi:doi, pubDate:pubDate}, showResult);
      });
    });
    $modal.modal('show');
  }

  // Show confirmation with message `msg1`. If confirmed, a wait progress indicator with `msg2`
  // will be displayed and function `fn` will be invoked.
  var showConfirmation = function(msg1, msg2, fn){
    toggleWait(false);
    $modalBody.empty().append(msg1);
    $okButton.off('click');
    $okButton.one('click', function(){
      toggleWait(true, msg2);
      fn();
    });
  };
  
  function showResult(err, ndocid) {
    toggleWait(false);
    $('.btn-default', $modal).addClass('hidden disabled');
    if (err || !ndocid) {
      var errMsg = err.stack || 'Unknown Error';
      $modalBody.empty().
        append('<p>Error:</p>').
        append('<p>' + errMsg + '</p>');
      $okButton.off('click');
      $okButton.one('click', function() {
        $modal.modal('hide');
      });
    } else {
      $modalBody.empty().append('<p>Success!</p><p>EML has been updated to: ' + ndocid + '</p>');
      $okButton.off('click');
      $okButton.one('click', function() {
        window.location.href = metacat.url.servlet + '/' + ndocid + '/html';
      });
    }
  }

  /** @param {boolean} show */
  function toggleWait(show, msg) {
    var methods = ['removeClass', 'addClass'];
    $modalBody[methods[+show]]('hidden');
    $modalBodyWait[methods[+(!show)]]('hidden');
    $('button', $modal)[methods[+show]]('hidden disabled');
    if (msg) $('.wait-message', $modalBodyWait).html(msg);
  }
  var oplabels = {
    title : "DOI Service",
    mint : "Mint new DOI",
    update : "Update DOI",
    add : "Add existing DOI"
  };
  
  var tools = {};
  tools.label = oplabels.title;
  tools.items = [
    {label:oplabels.mint, handler:showMintDialog},
    {label:oplabels.add, handler:showAddDialog}
  ];
  tools.hasAccess = function(cb){
    var doi = $.trim($('.alt-id-doi').text());
    //console.log(doi);
    if (doi.length) return cb(false);
    // test if logged-in user has write access permission
    metacat.hasWriteAccess(function(hasWriteAccess){
      if (!hasWriteAccess) return cb(false);
      // test if displayed document is the last revision
      metacat.isLastRevision(function(isLastRevision){
        if (!isLastRevision) return cb(false);
        //test if document is public
        metacat.isPublic(function(isPublic){
          return cb(isPublic);
        });
      });
    });
  };
  
  metacat.tools.push(tools);
  
});
