<div id="mapArea">
  <h2>Search by Location</h2>
  <p>Click on the data points on the map below to access the records from the area.</p>
  <div id="map"></div>
  <form id="mapForm" name="mapForm" method="get" action="<%=SERVLET_URL%>" data-geoserver-url="<%=GEOSERVER_URL%>">
    <input type="hidden" name="action" value="spatial_query"/>
    <input type="hidden" name="skin" value="<%=SKIN_NAME%>"/>
    <input type="hidden" name="xmin" value=""/>
    <input type="hidden" name="ymin" value=""/>
    <input type="hidden" name="xmax" value=""/>
    <input type="hidden" name="ymax" value=""/>
  </form>
  <!--
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.5&amp;sensor=false"></script>
  <script type="text/javascript" src="http://openlayers.org/api/OpenLayers.js"></script>
  <script type="text/javascript">initMap(document.forms['mapForm'])</script>
  -->
</div>