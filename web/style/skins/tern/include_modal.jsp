<div id="infoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="infoModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
        <h4 class="modal-title" id="infoModalTitle">Modal title</h4>
      </div>
      <div class="modal-body metadata-root" id="infoModalBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="downloadModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
        <h3 class="modal-title" id="downloadModalTitle">Download Data</h3>
        <h4 class="modal-title" id="downloadModalFileName"></h4>
      </div>
      <div class="modal-body">
        <h4>Licence</h4>
        <div class="well well-small" id="downloadModalLicence">Licence</div>
        <form>
          <div class="checkbox check-licence">
            <label><input type="checkbox" class="download-check" data-alert-class="alert-check-licence"/> I accept and agree to the terms of the Licence.</label>
          </div>
          <div class="checkbox check-special">
            <label><input type="checkbox" class="download-check" data-alert-class="alert-check-licence"/> I have read the Special Conditions and agree to comply with these conditions.</label>
          </div>
          <div class="alert alert-warning alert-check-licence">You must accept and agree to the terms of the Licence.</div>
          <% if (!FAIRUSEPOLICY_URL.isEmpty()) { %>
          <div class="checkbox check-fupolicy">
            <label><input type="checkbox" class="download-check check-fupolicy" data-alert-class="alert-check-fupolicy"/> I accept and agree to the <a target="_blank" href="<%=FAIRUSEPOLICY_URL%>">Fair Use Policy</a>.</label>
          </div>
          <div class="alert alert-warning alert-check-fupolicy">You must accept and agree to the fair use policy.</div>
          <% } %>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Cancel</button>
        <a id="downloadButton" class="btn btn-primary hidden">Download</a>
      </div>
    </div>
  </div>
</div>

<div id="toolsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="toolsModalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
        <h4 class="modal-title" id="toolsModalTitle">Modal title</h4>
      </div>
      <div class="modal-body modal-body-main">
      </div>
      <div class="modal-body modal-body-wait hidden">
        <p class="text-center wait-message"></p>
        <p class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">OK</button>
      </div>
    </div>
  </div>
</div>