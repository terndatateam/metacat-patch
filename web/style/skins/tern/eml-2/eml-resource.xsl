<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />
  
  <!-- This module is for resource and it is self-contained (it is table)-->
  <xsl:template name="resource">
    <xsl:param name="hideCitationInfo"/>
    
    <div class="resource">
      <xsl:call-template name="apply-alternateIdentifier"/>
      <xsl:call-template name="apply-shortName"/>
      <!--
      <xsl:if test="not($hideCitationInfo)">
      </xsl:if>
      -->
      <xsl:call-template name="apply-title"/>
      <xsl:call-template name="apply-creator"/>
      <xsl:call-template name="apply-metadataProvider"/>
      <xsl:call-template name="apply-associatedParty"/>
      <xsl:call-template name="apply-pubDate"/>
      <xsl:call-template name="apply-language"/>
      <xsl:call-template name="apply-series"/>
      <xsl:call-template name="apply-abstract"/>
      <xsl:call-template name="apply-keywordSet"/>
      <xsl:call-template name="apply-additionalInfo"/>
      <xsl:call-template name="apply-intellectualRights"/>
      <xsl:call-template name="apply-distribution">
        <xsl:with-param name="level">toplevel</xsl:with-param>
      </xsl:call-template>
      <xsl:apply-templates select="coverage"/>
    </div>
  </xsl:template>

  <xsl:template name="apply-alternateIdentifier">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Alternate Identifier'"/>
      <xsl:with-param name="nodes" select="alternateIdentifier"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="alternateIdentifier">
    <xsl:if test="normalize-space(.)">
      <dl class="field-label field-label-identifier-alternate">
        <dt><xsl:value-of select="@system"/></dt>
        <dd>
          <xsl:if test="@system"><xsl:attribute name="class">alt-id-<xsl:value-of select="@system"/></xsl:attribute></xsl:if>
          <xsl:value-of select="."/>
        </dd>
      </dl>
      <xsl:if test="position() != last()"><xsl:text> &#160; </xsl:text></xsl:if>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="apply-creator">
    <xsl:param name="creator">Creators</xsl:param>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="$creator"/>
      <xsl:with-param name="nodes" select="creator"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="apply-metadataProvider">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Metadata Providers'"/>
      <xsl:with-param name="nodes" select="metadataProvider"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="apply-associatedParty">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Associated Parties'"/>
      <xsl:with-param name="nodes" select="associatedParty"/>
    </xsl:call-template>
  </xsl:template>
      
  <xsl:template match="creator | metadataProvider">
    <xsl:call-template name="party"/>
  </xsl:template>
  
  <xsl:template match="associatedParty">
    <xsl:call-template name="party">
      <xsl:with-param name="role" select="true()"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-shortName">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Short Name'"/>
      <xsl:with-param name="nodes" select="shortName"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="shortName|pubDate|language|series">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template name="apply-title">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Title'"/>
      <xsl:with-param name="nodes" select="title"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="title">
    <strong><xsl:value-of select="."/></strong>
  </xsl:template>

  <xsl:template name="apply-pubDate">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Publication Date'"/>
      <xsl:with-param name="nodes" select="pubDate"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-language">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Language'"/>
      <xsl:with-param name="nodes" select="language"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="apply-series">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Series'"/>
      <xsl:with-param name="nodes" select="series"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="apply-abstract">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Abstract'"/>
      <xsl:with-param name="nodes" select="abstract"/>
    </xsl:call-template>
  </xsl:template>
  
  <!--xsl:template match="abstract|additionalInfo|intellectualRights">
    <xsl:call-template name="text"/>
  </xsl:template-->

  <xsl:template name="apply-keywordSet">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Keywords and Subject Categories'"/>
      <xsl:with-param name="nodes" select="keywordSet"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="keywordSet">
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="keywordThesaurus"/>
      <xsl:with-param name="nodes" select="keyword"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="keywordSet/keyword">
    <xsl:value-of select="."/>
    <xsl:if test="normalize-space(@keywordType)">
      <xsl:text>(</xsl:text><xsl:value-of select="@keywordType"/><xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="apply-additionalInfo">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Additional Information'"/>
      <xsl:with-param name="nodes" select="additionalInfo"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-intellectualRights">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Intellectual Rights, Licence and Usage Conditions'"/>
      <xsl:with-param name="nodes" select="intellectualRights"/>
    </xsl:call-template>
  </xsl:template>
      
</xsl:stylesheet>
