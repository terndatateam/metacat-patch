<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="project">
    <xsl:call-template name="project"/>
  </xsl:template>
  
  <xsl:template name="project">
    <xsl:variable name="ref_id" select="references"/>
    <xsl:variable name="node" select=". | $ids[@id=$ref_id]"/>
    <xsl:for-each select="$node[not(references)]">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Title'"/>
        <xsl:with-param name="nodes" select="title"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Personnel'"/>
        <xsl:with-param name="nodes" select="personnel"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Abstract'"/>
        <xsl:with-param name="nodes" select="abstract"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Funding'"/>
        <xsl:with-param name="nodes" select="funding"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Study Area'"/>
        <xsl:with-param name="nodes" select="studyAreaDescription/descriptor"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Study Area Citation'"/>
        <xsl:with-param name="nodes" select="studyAreaDescription/citation"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Study Area Coverage'"/>
        <xsl:with-param name="nodes" select="studyAreaDescription/coverage"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Design Description'"/>
        <xsl:with-param name="nodes" select="designDescription/description"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Design Citation'"/>
        <xsl:with-param name="nodes" select="designDescription/citation"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Related Project'"/>
        <xsl:with-param name="nodes" select="relatedProject"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  

  <xsl:template match="project/title">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()"><br/></xsl:if>
  </xsl:template>

  <xsl:template match="project/personnel">
    <xsl:call-template name="party">
      <xsl:with-param name="role" select="true()"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="project/abstract | project/funding | designDescription/description">
    <xsl:call-template name="text"/>
    <xsl:if test="position() != last()"><br/></xsl:if>
  </xsl:template>

  <xsl:template match="studyAreaDescription/descriptor">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="@name"/>
      <xsl:with-param name="nodes" select="descriptorValue"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Citation'"/>
      <xsl:with-param name="nodes" select="citation"/>
    </xsl:call-template>
    <xsl:if test="not(@citableClassificationSystem)">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="''"/>
        <xsl:with-param name="value" select="'(No Citable Classification System)'"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="studyAreaDescription/descriptor/descriptorValue">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="@name_or_id"/>
      <xsl:with-param name="nodes" select="."/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="relatedProject">
    <div class="field-group">
      <xsl:call-template name="project"/>
    </div>
  </xsl:template>

</xsl:stylesheet>
