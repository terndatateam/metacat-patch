<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />
  <!-- This module is for party member and it is self contained-->

    <!--
  <xsl:template match="references" mode="party">
    <xsl:variable name="ref_id" select="."/>
    <xsl:apply-templates mode="party" select="$ids[@id=$ref_id]"/>
  </xsl:template>
    -->

  <xsl:template name="party">
    <xsl:param name="role"/>
    <xsl:param name="show-contact-details"/>
    
    <xsl:variable name="ref_id" select="references"/>
    <xsl:variable name="node" select="(. | $ids[@id=$ref_id])[not(references)]"/>
    <!--xsl:variable name="node" select=". | $ids[@id=$ref_id]"/-->
    <div class="party field-group">
      <!--xsl:apply-templates select="$node/*" mode="party"/-->
      <xsl:if test="$role">
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Role'"/>
          <xsl:with-param name="value" select="role"/>
          <xsl:with-param name="classes" select="'role'"/>
        </xsl:call-template>
      </xsl:if>
      
      <xsl:for-each select="$node">
        <xsl:call-template name="apply-field-party">
          <xsl:with-param name="label" select="'Individual'"/>
          <xsl:with-param name="nodes" select="individualName"/>
          <xsl:with-param name="classes" select="'individualName party-name'"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Position'"/>
          <xsl:with-param name="value" select="positionName"/>
          <xsl:with-param name="classes">
            <xsl:text>positionName</xsl:text>
            <xsl:if test="not(normalize-space(individualName))">
              <xsl:text> party-name</xsl:text>
            </xsl:if>
          </xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Organization'"/>
          <xsl:with-param name="value" select="organizationName"/>
          <xsl:with-param name="classes">
            <xsl:text>organizationName</xsl:text>
            <xsl:if test="not(normalize-space(individualName)) and not(normalize-space(positionName))">
              <xsl:text> party-name</xsl:text>
            </xsl:if>
          </xsl:with-param>
        </xsl:call-template>
        
        <xsl:if test="$show-contact-details">
          <xsl:call-template name="apply-field-party">
            <xsl:with-param name="label" select="'Address'"/>
            <xsl:with-param name="nodes" select="address"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field-party">
            <xsl:with-param name="label" select="'Phone'"/>
            <xsl:with-param name="nodes" select="phone"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field-party">
            <xsl:with-param name="label" select="'Email Address'"/>
            <xsl:with-param name="nodes" select="electronicMailAddress"/>
          </xsl:call-template>
        </xsl:if>
        
        <xsl:call-template name="apply-field-party">
          <xsl:with-param name="label" select="'Web Address'"/>
          <xsl:with-param name="nodes" select="onlineUrl"/>
        </xsl:call-template>

        <xsl:call-template name="apply-field-party">
          <xsl:with-param name="label" select="'User Identifier'"/>
          <xsl:with-param name="nodes" select="userId"/>
        </xsl:call-template>
        
      </xsl:for-each>
    </div>
  </xsl:template>
  
  <xsl:template name="apply-field-party">
    <xsl:param name="label"/>
    <xsl:param name="nodes"/>
    <xsl:param name="classes" select="local-name($nodes)"/>
    <xsl:if test="normalize-space($nodes)">
      <dl class="field {$classes}">
        <dt><xsl:value-of select="$label"/></dt>
        <dd><xsl:apply-templates select="$nodes" mode="party"/></dd>
      </dl>
    </xsl:if>
  </xsl:template>

  <!-- *********************************************************************** -->

  <xsl:template match="individualName" mode="party">
    <xsl:for-each select="salutation">
      <xsl:value-of select="."/><xsl:text> </xsl:text>
    </xsl:for-each>
    <xsl:for-each select="givenName">
      <xsl:value-of select="."/><xsl:text> </xsl:text>
    </xsl:for-each>
    <xsl:value-of select="surName"/>
  </xsl:template>

  <xsl:template match="address" mode="party">
    <xsl:variable name="ref_id" select="references"/>
    <xsl:variable name="node" select="(. | $ids[@id=$ref_id])[not(references)]"/>
    <div class="address-group">
      <xsl:for-each select="$node/deliveryPoint">
        <span><xsl:value-of select="."/></span><br/>
      </xsl:for-each>
      <xsl:if test="normalize-space($node/city)">
        <span><xsl:value-of select="$node/city"/></span>
        <xsl:if test="normalize-space($node/administrativeArea) or normalize-space($node/postalCode) or normalize-space($node/country)">
          <xsl:text>. </xsl:text>
        </xsl:if>
      </xsl:if>
      <xsl:if test="normalize-space($node/administrativeArea) or normalize-space($node/postalCode)">
        <span>
          <xsl:if test="normalize-space($node/administrativeArea)">
            <xsl:value-of select="$node/administrativeArea"/><xsl:text> </xsl:text>
          </xsl:if>
          <xsl:if test="normalize-space($node/postalCode)">
            <xsl:value-of select="$node/postalCode"/>
          </xsl:if>
        </span>
        <br/>
      </xsl:if>
      <xsl:if test="normalize-space($node/country)">
        <xsl:value-of select="$node/country"/>
      </xsl:if>
    </div>
  </xsl:template>
  
  <xsl:template match="phone" mode="party">
    <dl class="field-label field-label-default">
      <xsl:if test="normalize-space(@phonetype)">
        <dt><xsl:value-of select="./@phonetype"/></dt>
      </xsl:if>
      <dd><xsl:value-of select="."/></dd>
    </dl>
    <xsl:if test="position() != last()"><xsl:text> &#160; </xsl:text></xsl:if>
  </xsl:template>

  <xsl:template match="electronicMailAddress" mode="party">
    <xsl:if test="$withHTMLLinks='1'">
      <a><xsl:attribute name="href">mailto:<xsl:value-of select="."/></xsl:attribute><xsl:value-of select="./entityName"/><xsl:value-of select="."/></a>
    </xsl:if>
    <xsl:if test="$withHTMLLinks='0'">
      <dl class="field-label field-label-default">
        <dd><xsl:value-of select="."/></dd>
      </dl>
    </xsl:if>
    <xsl:if test="position() != last()"><xsl:text> &#160; </xsl:text></xsl:if>
  </xsl:template>

  <xsl:template match="onlineUrl" mode="party">
    <xsl:if test="$withHTMLLinks='1'">
      <a><xsl:attribute name="href"><xsl:if test="not(contains(.,':/'))">http://</xsl:if><xsl:value-of select="."/></xsl:attribute><xsl:value-of select="./entityName"/><xsl:value-of select="."/></a>
    </xsl:if>
    <xsl:if test="$withHTMLLinks='0'">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="position() != last()"><br/></xsl:if>
  </xsl:template>

  <xsl:template match="userId" mode="party">
    <dl class="field-label field-label-identifier-alternate">
      <dt><xsl:value-of select="@directory"/></dt>
      <dd><xsl:value-of select="."/></dd>
    </dl>
    <xsl:if test="position() != last()"><xsl:text> &#160; </xsl:text></xsl:if>
  </xsl:template>
  
  <xsl:template match="text()" mode="party" />
  
</xsl:stylesheet>
