<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />
  <!-- This module is for text module in eml2 document. -->
  
  <xsl:template name="text">
    <xsl:apply-templates select="*" mode="text"/>
    <xsl:if test="not(section) and not(para)">
      <xsl:value-of select="."/>
    </xsl:if>
  </xsl:template>
  

  <xsl:template match="section" mode="text">
    <xsl:if test="normalize-space(.)">
      <div class="text-section">
        <xsl:if test="title and normalize-space(title)">
          <div class="text-section-title"><xsl:value-of select="title"/></div>
        </xsl:if>
        <xsl:if test="para and normalize-space(para)">
          <xsl:apply-templates select="para" mode="text"/>
        </xsl:if>
        <xsl:if test="section and normalize-space(section)">
          <xsl:apply-templates select="section" mode="text"/>
        </xsl:if>
      </div>
    </xsl:if>
  </xsl:template>


  <xsl:template match="para" mode="text">
    <xsl:apply-templates select="itemizedlist | orderedlist | literalLayout" mode="para"/>
    <xsl:if test="not(itemizedlist) and not(orderedlist) and not(literalLayout)">
      <p><xsl:apply-templates mode="para"/></p>
    </xsl:if>
  </xsl:template>

  <xsl:template match="itemizedlist" mode="para">
    <ul><xsl:apply-templates select="listitem" mode="para"/></ul>
  </xsl:template>

  <xsl:template match="orderedlist" mode="para">
    <ol><xsl:apply-templates select="listitem" mode="para"/></ol>
  </xsl:template>

  <xsl:template match="listitem" mode="para">
    <li>
      <xsl:apply-templates select="para" mode="text"/>
      <xsl:apply-templates select="itemizedlist | orderedlist" mode="para"/>
    </li>
  </xsl:template>

  <xsl:template match="text()" mode="para">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="emphasis" mode="para">
    <em><xsl:value-of select="."/></em>
  </xsl:template>

  <xsl:template match="subscript" mode="para">
    <sub><xsl:apply-templates mode="para"/></sub>
  </xsl:template>

  <xsl:template match="superscript" mode="para">
    <sup><xsl:apply-templates mode="para"/></sup>
  </xsl:template>
  
  <xsl:template match="literalLayout" mode="para">
    <pre><xsl:value-of select="."/></pre>
  </xsl:template>
  
  <xsl:template match="ulink" mode="para">
    <a href="{@url}"><xsl:value-of select="citetitle"/></a>
  </xsl:template>
  
  
  <!-- para template without table structure. It does actually transfer.
       Currently, only get the text and it need more revision-->
<!--    
  <xsl:template match="para" mode="lowlevel" name="replaceNewline">
    <xsl:param name="pText" select="."/>
    <xsl:choose>
      <xsl:when test="contains($pText, '&#xA;')">
        <xsl:value-of select="substring-before($pText, '&#xA;')"/>
        <br />
        <xsl:call-template name="replaceNewline">
          <xsl:with-param name="pText" select="substring-after($pText, '&#xA;')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$pText"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="."/>
    <br/>
  </xsl:template>
-->

</xsl:stylesheet>
