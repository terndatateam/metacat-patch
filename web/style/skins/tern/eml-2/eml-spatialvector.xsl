<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <!-- This module is for spatialVector module-->
  
  <xsl:template match="spatialVector">
    <xsl:call-template name="entity"/>
  </xsl:template>
  
  <xsl:template match="spatialVector" mode="entityInfo">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Geometry'"/>
      <xsl:with-param name="nodes" select="geometry"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Geometric Objects'"/>
      <xsl:with-param name="value" select="geometricObjectCount"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Topology Level'"/>
      <xsl:with-param name="value" select="topologyLevel"/>
    </xsl:call-template>
    <xsl:apply-templates select="spatialReference" mode="complexfield">
      <xsl:with-param name="label">Spatial Reference</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="horizontalAccuracy" mode="complexfield">
      <xsl:with-param name="label">Horizontal Accuracy</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="verticalAccuracy" mode="complexfield">
      <xsl:with-param name="label">Vertical Accuracy</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="spatialVector/geometry">
    <span class="label label-default"><xsl:value-of select="."/></span>
  </xsl:template>
   
  <xsl:template match="spatialVector/horizontalAccuracy | spatialVector/verticalAccuracy">
    <xsl:call-template name="dataQuality"/>
  </xsl:template>

</xsl:stylesheet>
