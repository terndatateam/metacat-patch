<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="protocol">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="resource">
        <xsl:with-param name="creator">Author</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Procedural Step'"/>
        <xsl:with-param name="nodes" select="proceduralStep"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="proceduralStep">
    <div class="field-group">
      <div class="field-group-heading"><xsl:text>Step </xsl:text><xsl:value-of select="position()"/></div>
      <xsl:call-template name="step"/>
    </div>
  </xsl:template>

  <xsl:template name="step">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Description'"/>
      <xsl:with-param name="nodes" select="description"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Citation'"/>
      <xsl:with-param name="nodes" select="citation"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Protocol'"/>
      <xsl:with-param name="nodes" select="protocol"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Instrument'"/>
      <xsl:with-param name="nodes" select="instrumentation"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Software'"/>
      <xsl:with-param name="nodes" select="software"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Substep'"/>
      <xsl:with-param name="nodes" select="subStep"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="description">
    <xsl:call-template name="text"/>
  </xsl:template>
  
  <xsl:template match="instrumentation">
    <xsl:value-of select="."/>
  </xsl:template>
  
  <xsl:template match="subStep">
    <div class="field-group">
      <div class="field-group-heading"><xsl:text>Substep </xsl:text><xsl:value-of select="position()"/></div>
      <xsl:call-template name="step"/>
    </div>
  </xsl:template>
  
</xsl:stylesheet>
