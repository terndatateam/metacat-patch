<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="physical">
    <xsl:call-template name="physical"/>
  </xsl:template>
  
  <xsl:template match="physical" mode="withoutDistribution">
    <xsl:call-template name="physical">
      <xsl:with-param name="withDistribution" select="false()"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="physical">
    <xsl:param name="withDistribution" select="true()"/>
    
    <xsl:variable name="ref_id" select="references"/>
    <xsl:variable name="node" select=". | $ids[@id=$ref_id]"/>
    <xsl:for-each select="$node[not(references)]">
      <div class="field-group">
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Object Name'"/>
          <xsl:with-param name="value" select="objectName"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Size'"/>
          <xsl:with-param name="value" select="concat(size,' ',size/@unit)"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Authentication'"/>
          <xsl:with-param name="nodes" select="authentication"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Compression Method'"/>
          <xsl:with-param name="value" select="compressionMethod"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Encoding Method'"/>
          <xsl:with-param name="value" select="encodingMethod"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Character Encoding'"/>
          <xsl:with-param name="value" select="characterEncoding"/>
        </xsl:call-template>
        <xsl:apply-templates select="dataFormat/*" mode="field"/>
        <xsl:if test="$withDistribution">
          <xsl:call-template name="apply-distribution"/>
        </xsl:if>
      </div>
    </xsl:for-each>
  </xsl:template>
  

  <xsl:template match="authentication">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'@method'"/>
      <xsl:with-param name="nodes" select="."/>
    </xsl:call-template>
  </xsl:template>

  <!--***********************************************************
      TextFormat templates
      ***********************************************************-->

  <xsl:template match="dataFormat/textFormat" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Text Format'"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="dataFormat/textFormat">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Header Lines'"/>
      <xsl:with-param name="value" select="numHeaderLines"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Footer Lines'"/>
      <xsl:with-param name="value" select="numFooterLines"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Record Delimiter'"/>
      <xsl:with-param name="nodes" select="recordDelimiter"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Line Delimiter'"/>
      <xsl:with-param name="nodes" select="physicalLineDelimiter"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Line Number For One Record'"/>
      <xsl:with-param name="value" select="numPhysicalLinesPerRecord"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Maximum Record Length'"/>
      <xsl:with-param name="value" select="maxRecordLength"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Attribute Orientation'"/>
      <xsl:with-param name="value" select="attributeOrientation"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Simple Delimited'"/>
      <xsl:with-param name="nodes" select="simpleDelimited"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Text Fixed'"/>
      <xsl:with-param name="nodes" select="complex/textFixed"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Text Delimited'"/>
      <xsl:with-param name="nodes" select="complex/textDelimited"/>
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="dataFormat/textFormat/recordDelimiter | dataFormat/textFormat/physicalLineDelimiter">
    <span class="label label-default"><xsl:value-of select="."/></span><xsl:text> </xsl:text>
  </xsl:template>
  
  <xsl:template match="dataFormat/textFormat/simpleDelimited">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Field Delimiter'"/>
      <xsl:with-param name="nodes" select="fieldDelimiter"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Consecutive Delimiters are Single'"/>
      <xsl:with-param name="value" select="collapseDelimiters"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Quote Character'"/>
      <xsl:with-param name="nodes" select="quoteCharacter"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Literal Character'"/>
      <xsl:with-param name="nodes" select="literalCharacter"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="simpleDelimited/fieldDelimiter | complex/textDelimited/fieldDelimiter">
    <span class="label label-default"><xsl:value-of select="."/></span><xsl:text> </xsl:text>
  </xsl:template>

  <xsl:template match="simpleDelimited/quoteCharacter | simpleDelimited/literalCharacter | complex/textDelimited/quoteCharacter | complex/textDelimited/literalCharacter">
    <span class="label label-default"><xsl:value-of select="."/></span><xsl:text> </xsl:text>
  </xsl:template>
  
  <xsl:template match="dataFormat/textFormat/complex/textFixed">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Field Width'"/>
      <xsl:with-param name="value" select="fieldWidth"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Line Number'"/>
      <xsl:with-param name="value" select="lineNumber"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Field Start Column'"/>
      <xsl:with-param name="value" select="fieldStartColumn"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="dataFormat/textFormat/complex/textDelimited">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Field Delimiter'"/>
      <xsl:with-param name="nodes" select="fieldDelimiter"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Consecutive Delimiters are Single'"/>
      <xsl:with-param name="value" select="collapseDelimiters"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Line Number'"/>
      <xsl:with-param name="value" select="lineNumber"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Quote Character'"/>
      <xsl:with-param name="nodes" select="quoteCharacter"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Literal Character'"/>
      <xsl:with-param name="nodes" select="literalCharacter"/>
    </xsl:call-template>
  </xsl:template>
  
 

  <!--***********************************************************
      externallyDefinedFormat templates
      ***********************************************************-->
      
  <xsl:template match="dataFormat/externallyDefinedFormat" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Externally Defined Format'"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="dataFormat/externallyDefinedFormat">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Format Name'"/>
      <xsl:with-param name="value" select="formatName"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Format Version'"/>
      <xsl:with-param name="value" select="formatVersion"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Citation'"/>
      <xsl:with-param name="nodes" select="citation"/>
    </xsl:call-template>
  </xsl:template>
  

  <!--***********************************************************
      binaryRasterFormat templates
      ***********************************************************-->
  <xsl:template match="dataFormat/binaryRasterFormat" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Binary Raster Format'"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="dataFormat/binaryRasterFormat">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Orientation'"/>
      <xsl:with-param name="value" select="rowColumnOrientation"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Spectral Bands'"/>
      <xsl:with-param name="value" select="multiBand/nbands"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Multiple Bands Layout'"/>
      <xsl:with-param name="value" select="multiBand/layout"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Bits (/pixel/band)'"/>
      <xsl:with-param name="value" select="nbits"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Byte Order'"/>
      <xsl:with-param name="value" select="byteorder"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Skipped Bytes'"/>
      <xsl:with-param name="value" select="skipbytes"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Bytes (/band/row)'"/>
      <xsl:with-param name="value" select="bandrowbytes"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Total Number of Byte (/row)'"/>
      <xsl:with-param name="value" select="totalrowbytes"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Number of Bytes between Bands'"/>
      <xsl:with-param name="value" select="bandgapbytes"/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
