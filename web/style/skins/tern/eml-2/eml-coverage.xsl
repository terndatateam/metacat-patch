<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="eml-literature.xsl"/>
  <xsl:output method="html" encoding="utf-8" indent="yes" />
  
  <xsl:template match="coverage">
    <xsl:call-template name="coverage"/>
  </xsl:template>

  <xsl:template name="coverage">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Geographic Coverage'"/>
        <xsl:with-param name="nodes" select="geographicCoverage"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Temporal Coverage'"/>
        <xsl:with-param name="nodes" select="temporalCoverage"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Taxonomic Coverage and Classification'"/>
        <xsl:with-param name="nodes" select="taxonomicCoverage"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  

 <!-- ********************************************************************* -->
 <!-- **************  G E O G R A P H I C   C O V E R A G E  ************** -->
 <!-- ********************************************************************* -->
  
  <xsl:template match="geographicCoverage">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <div class="field-group">
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Geographic Description'"/>
          <xsl:with-param name="value" select="geographicDescription"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Bounding Coordinates'"/>
          <xsl:with-param name="nodes" select="boundingCoordinates"/>
        </xsl:call-template>
        <xsl:apply-templates select="datasetGPolygon"/>
      </div>
    </xsl:if>
  </xsl:template>

  
  <xsl:template match="boundingCoordinates">
    <xsl:apply-templates/>
    <div class="geographicCoverage-map">
      <xsl:attribute name="data-west"><xsl:value-of select="westBoundingCoordinate"/></xsl:attribute>
      <xsl:attribute name="data-east"><xsl:value-of select="eastBoundingCoordinate"/></xsl:attribute>
      <xsl:attribute name="data-north"><xsl:value-of select="northBoundingCoordinate"/></xsl:attribute>
      <xsl:attribute name="data-south"><xsl:value-of select="southBoundingCoordinate"/></xsl:attribute>
    </div>
  </xsl:template>
  
  <xsl:template match="boundingAltitudes">
    <xsl:apply-templates select="altitudeMinimum">
      <xsl:with-param name="label" select="'Minimum Altitude'"/>
      <xsl:with-param name="unit" select="altitudeUnits"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="altitudeMaximum">
      <xsl:with-param name="label" select="'Maximum Altitude'"/>
      <xsl:with-param name="unit" select="altitudeUnits"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="westBoundingCoordinate | eastBoundingCoordinate | northBoundingCoordinate | southBoundingCoordinate | boundingAltitudes/*">
    <xsl:param name="label"/>
    <xsl:param name="unit">degrees</xsl:param>
    <xsl:variable name="nodeName" select="local-name()"/>
    <xsl:if test="normalize-space(.)">
      <dl class="field-label field-label-default {$nodeName}">
        <dt>
          <xsl:if test="normalize-space($label)">
            <xsl:value-of select="$label"/>
          </xsl:if>
          <xsl:if test="not(normalize-space($label))">
            <xsl:variable name="labellow" select="substring-before($nodeName,'BoundingCoordinate')"/>
            <xsl:value-of select="concat(translate(substring($labellow, 1, 1), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), substring($labellow, 2))"/>
          </xsl:if>
        </dt>
        <dd><xsl:value-of select="concat(., ' ', $unit)"/></dd>
      </dl>
      <xsl:text> &#160; </xsl:text>
    </xsl:if>
  </xsl:template>

  
  <xsl:template match="datasetGPolygon">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'G-Ploygon(Outer Ring)'"/>
      <xsl:with-param name="value" select="datasetGPolygonOuterGRing"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'G-Ploygon(Exclusion Ring)'"/>
      <xsl:with-param name="value" select="datasetGPolygonExclusionGRing"/>
    </xsl:call-template>
  </xsl:template>

  
  <xsl:template match="datasetGPolygon/datasetGPolygonOuterGRing|datasetGPolygon/datasetGPolygonExclusionGRing">
    <xsl:apply-templates select="gRingPoint"/>
    <xsl:apply-templates select="gRing"/>
  </xsl:template>


  <xsl:template match="gRing">
    <xsl:value-of select="."/>
  </xsl:template>

  
  <xsl:template match="gRingPoint">
    <xsl:value-of select="gRingLongitude"/><br/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="gRingLatitude"/>
    <xsl:text> </xsl:text>
  </xsl:template>

<!-- ********************************************************************* -->
<!-- ****************  T E M P O R A L   C O V E R A G E  **************** -->
<!-- ********************************************************************* -->

  <xsl:template match="temporalCoverage">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <div class="field-group">
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Date'"/>
          <xsl:with-param name="nodes" select="singleDateTime"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Date Begin'"/>
          <xsl:with-param name="nodes" select="rangeOfDates/beginDate"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Date End'"/>
          <xsl:with-param name="nodes" select="rangeOfDates/endDate"/>
        </xsl:call-template>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="singleDateTime | rangeOfDates/beginDate | rangeOfDates/endDate">
    <xsl:call-template name="singleDateType"/>
  </xsl:template>

  <xsl:template name="singleDateType">
    <xsl:if test="calendarDate">
      <xsl:if test="position() &gt; 1">
        <xsl:text> , </xsl:text>
      </xsl:if>
      <xsl:value-of select="calendarDate"/>
      <xsl:if test="normalize-space(time)">
        <xsl:text>T</xsl:text>
        <xsl:value-of select="time"/>
      </xsl:if>
    </xsl:if>
    <xsl:apply-templates select="alternativeTimeScale"/>
  </xsl:template>


  <xsl:template match="alternativeTimeScale">
    <div class="field-group">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Timescale'"/>
        <xsl:with-param name="value" select="timeScaleName"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Time estimate'"/>
        <xsl:with-param name="value" select="timeScaleAgeEstimate"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Time uncertainty'"/>
        <xsl:with-param name="value" select="timeScaleAgeUncertainty"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Time explanation'"/>
        <xsl:with-param name="value" select="timeScaleAgeExplanation"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Citation'"/>
        <xsl:with-param name="nodes" select="timeScaleCitation"/>
      </xsl:call-template>
    </div>
  </xsl:template>

  
  <xsl:template match="alternativeTimeScale/timeScaleCitation">
    <!-- Using citation module here -->
    <xsl:call-template name="citation"/>
  </xsl:template>

  
<!-- ********************************************************************* -->
<!-- ***************  T A X O N O M I C   C O V E R A G E  *************** -->
<!-- ********************************************************************* -->
  <xsl:template match="taxonomicCoverage">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <div class="field-group">
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Taxonomic System'"/>
          <xsl:with-param name="nodes" select="taxonomicSystem"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'General Coverage'"/>
          <xsl:with-param name="nodes" select="generalTaxonomicCoverage"/>
        </xsl:call-template>
        <xsl:call-template name="apply-field-list">
          <xsl:with-param name="label" select="'Classification'"/>
          <xsl:with-param name="nodes" select="taxonomicClassification"/>
        </xsl:call-template>
      </div>
    </xsl:if>
  </xsl:template>

  
  <xsl:template match="taxonomicCoverage/taxonomicSystem">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Classification Citation'"/>
      <xsl:with-param name="nodes" select="classificationSystem"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'ID Reference'"/>
      <xsl:with-param name="nodes" select="identificationReference"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'ID Name'"/>
      <xsl:with-param name="nodes" select="identifierName"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Procedures'"/>
      <xsl:with-param name="nodes" select="taxonomicProcedures"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Completeness'"/>
      <xsl:with-param name="nodes" select="taxonomicCompleteness"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Vouchers'"/>
      <xsl:with-param name="nodes" select="vouchers"/>
    </xsl:call-template>
  </xsl:template>
  
  
  <xsl:template match="taxonomicSystem/classificationSystem">
    <div class="field-group">
      <xsl:apply-templates select="classificationSystemCitation"/>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Modification'"/>
        <xsl:with-param name="nodes" select="classificationSystemModifications"/>
      </xsl:call-template>
    </div>
  </xsl:template>

  
  <xsl:template match="classificationSystem/classificationSystemCitation|taxonomicSystem/identificationReference">
    <xsl:call-template name="citation"/>
  </xsl:template>

  
  <xsl:template match="taxonomicSystem/vouchers">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Specimen'"/>
      <xsl:with-param name="value" select="specimen"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Repository'"/>
      <xsl:with-param name="nodes" select="repository/originator"/>
    </xsl:call-template>
  </xsl:template>
  

  <xsl:template match="taxonomicSystem/identifierName|vouchers/repository/originator">
    <xsl:call-template name="party"/>
  </xsl:template>


  <xsl:template match="taxonomicClassification">
    <dl class="field-label field-label-default">
      <dt><xsl:value-of select="taxonRankName"/></dt>
      <dd>
        <xsl:value-of select="taxonRankValue"/>
        <xsl:if test="commonName"><xsl:text> (</xsl:text><xsl:value-of select="commonName"/><xsl:text>)</xsl:text></xsl:if>
      </dd>
    </dl>
    <xsl:text> </xsl:text>
    <xsl:if test="taxonomicClassification"><xsl:text> &#160; </xsl:text>
      <xsl:apply-templates select="taxonomicClassification"/>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>