<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />  
  
  <xsl:template name="apply-access">
    <xsl:apply-templates select="access" mode="field"/>
  </xsl:template>
  
  <xsl:template match="access" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Access'"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="access">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <dl class="field">
        <dt>Access Control</dt>
        <dd>
          <dl class="field">
            <dt>Auth System</dt>
            <dd><xsl:value-of select="./@authSystem"/></dd>
          </dl>
          <dl class="field">
            <dt>Order</dt>
            <dd><xsl:value-of select="./@order"/></dd>
          </dl>
        </dd>
      </dl>
      <xsl:if test="normalize-space(./@order)='allowFirst' and allow">
        <xsl:call-template name="apply-allow"/>
      </xsl:if>
      <xsl:if test="deny">
        <xsl:call-template name="apply-deny"/>
      </xsl:if>
      <xsl:if test="normalize-space(./@order)='denyFirst' and allow">
        <xsl:call-template name="apply-allow"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="apply-allow">
    <dl class="field">
      <dt>Allow:</dt>
      <dd>
        <table class="table table-condensed">
          <thead>
            <tr><th>Permission</th><th>Principal</th></tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="allow"/>
          </tbody>
        </table>
      </dd>
    </dl>
  </xsl:template>

  <xsl:template name="apply-deny">
    <dl class="field">
      <dt>Deny:</dt>
      <dd>
        <table class="table table-condensed">
          <thead>
            <tr><th>Permission</th><th>Principal</th></tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="deny"/>
          </tbody>
        </table>
      </dd>
    </dl>
  </xsl:template>

  <xsl:template match="allow|deny">
    <tr>
      <td>
        <xsl:text>[</xsl:text>
        <xsl:for-each select="./permission">
          <xsl:if test="position() > 1"><xsl:text>, </xsl:text></xsl:if>
          <xsl:value-of select="."/>
        </xsl:for-each>
        <xsl:text>] </xsl:text>
      </td>
      <td>
        <xsl:for-each select="principal">
          <xsl:value-of select="."/>
          <xsl:if test="position() > 1"><br/></xsl:if>
        </xsl:for-each>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
