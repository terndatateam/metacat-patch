<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="attributeList">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:apply-templates select="attribute"/>
    </xsl:if>
  </xsl:template>
  
  
  <xsl:template match="attributeList" mode="single">
    <xsl:param name="attributeindex"/>

    <xsl:variable name="attributeListRefId" select="references"/>
    <xsl:variable name="attributeListNode" select=". | $ids[@id=$attributeListRefId]"/>
    <xsl:variable name="attributeSelectedNode" select="$attributeListNode/attribute[position()=$attributeindex]"/>
    <xsl:variable name="attributeRefId" select="$attributeSelectedNode/references"/>
    <xsl:variable name="attributeNode" select="$attributeSelectedNode | $ids[@id=$attributeRefId]"/>

    <xsl:apply-templates select="attributeNode"/>
  </xsl:template>

  
  <xsl:template match="attribute">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <div class="attribute field-group field-group-collapsible">
        <div class="field-group-heading field-group-toggle" data-title-show="Show Attribute" data-title-hide="Hide Attribute">
          <xsl:value-of select="attributeName"/>
        </div>
        <div class="field-group-body">
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Name'"/>
            <xsl:with-param name="nodes" select="attributeName"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Label'"/>
            <xsl:with-param name="nodes" select="attributeLabel"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Definition'"/>
            <xsl:with-param name="nodes" select="attributeDefinition"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Storage Type'"/>
            <xsl:with-param name="nodes" select="storageType"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Measurement Type'"/>
            <xsl:with-param name="value" select="local-name(measurementScale/*)"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Measurement Domain'"/>
            <xsl:with-param name="nodes" select="measurementScale"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Missing Value Code'"/>
            <xsl:with-param name="nodes" select="missingValueCode"/>
          </xsl:call-template>
          <xsl:call-template name="apply-field">
            <xsl:with-param name="label" select="'Accuracy'"/>
            <xsl:with-param name="nodes" select="accuracy"/>
          </xsl:call-template>
          <xsl:call-template name="apply-modal-attribute">
            <xsl:with-param name="label" select="'Coverage'"/>
            <xsl:with-param name="labelLink" select="'Coverage Information'"/>
            <xsl:with-param name="nodes" select="coverage"/>
          </xsl:call-template>
          <xsl:call-template name="apply-modal-attribute">
            <xsl:with-param name="label" select="'Sampling, Processing and Quality Control Methods'"/>
            <xsl:with-param name="labelLink" select="'Methods Information'"/>
            <xsl:with-param name="nodes" select="methods"/>
          </xsl:call-template>
        </div>
      </div>
    </xsl:if>
  </xsl:template>

<!-- Skipped templates definition for attributeName, attributeLabel, attributeDefinition, and storageType, 
     because they are of a plain text value type. -->
     
  <xsl:template match="measurementScale">
    <xsl:apply-templates select="*"/>
  </xsl:template>
  
  
  <xsl:template match="measurementScale/nominal | measurementScale/ordinal">
    <xsl:apply-templates select="nonNumericDomain"/>
  </xsl:template>


  <xsl:template match="measurementScale/interval | measurementScale/ratio">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Standard Unit'"/>
      <xsl:with-param name="value" select="unit/standardUnit"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Custom Unit'"/>
      <xsl:with-param name="value" select="unit/customUnit"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Precision'"/>
      <xsl:with-param name="value" select="precision"/>
    </xsl:call-template>
    <xsl:apply-templates select="numericDomain"/>
  </xsl:template>

  <xsl:template match="numericDomain">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Number Type'"/>
        <xsl:with-param name="value" select="numberType"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Bounds'"/>
        <xsl:with-param name="value" select="bounds"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

 <xsl:template match="measurementScale/dateTime">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Format'"/>
      <xsl:with-param name="value" select="formatString"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Precision'"/>
      <xsl:with-param name="value" select="dateTimePrecision"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Domain'"/>
      <xsl:with-param name="nodes" select="dateTimeDomain"/>
    </xsl:call-template>
 </xsl:template>

  <xsl:template match="dateTimeDomain">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:apply-templates select="bounds"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="dateTimeDomain/bounds|numericDomain/bounds">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Min'"/>
      <xsl:with-param name="nodes" select="minimum"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Max'"/>
      <xsl:with-param name="nodes" select="maximum"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="bounds/minimum|bounds/maximum">
    <xsl:value-of select="."/>
    <xsl:if test="not(exclusive) or exclusive='false'"><xsl:text> (inclusive)</xsl:text></xsl:if>
  </xsl:template>

  <xsl:template match="missingValueCode">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="code"/>
      <xsl:with-param name="value" select="codeExplanation"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="accuracy">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Report'"/>
      <xsl:with-param name="nodes" select="attributeAccuracyReport"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Assessment'"/>
      <xsl:with-param name="nodes" select="quantitativeAttributeAccuracyAssessment"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="quantitativeAttributeAccuracyAssessment">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="attributeAccuracyValue"/>
      <xsl:with-param name="value" select="attributeAccuracyExplanation"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-modal-attribute">
    <xsl:param name="label"/>
    <xsl:param name="labelLink"/>
    <xsl:param name="nodes"/>
    <xsl:param name="classes" select="local-name($nodes)"/>
    <xsl:param name="entityName" select="../../entityName"/>
    <xsl:param name="attributeName" select="attributeName"/>
    <xsl:if test="$nodes">
      <dl class="field {$classes}">
        <dt><xsl:value-of select="$label"/></dt>
        <dd>
          <div class="info-modal">
            <a href="#" class="btn btn-info info-modal-toggle"><xsl:value-of select="$labelLink"/></a>
            <div class="info-modal-title">
              <xsl:value-of select="$labelLink"/><xsl:text> - </xsl:text>
              <xsl:value-of select="$entityName"/><xsl:text> / </xsl:text>
              <xsl:value-of select="$attributeName"/>
            </div>
            <div class="info-modal-body">
              <xsl:apply-templates select="$nodes"/>
            </div>
          </div>
        </dd>
      </dl>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>