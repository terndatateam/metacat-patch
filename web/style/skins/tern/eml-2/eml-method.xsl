<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="methods">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Methods'"/>
        <xsl:with-param name="nodes" select="methodStep"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Sampling'"/>
        <xsl:with-param name="nodes" select="sampling"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Quality Control'"/>
        <xsl:with-param name="nodes" select="qualityControl"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="methods/methodStep">
    <div class="field-group">
      <div class="field-group-heading"><xsl:text>Method Step </xsl:text><xsl:value-of select="position()"/></div>
      
      <xsl:call-template name="step"/>
      
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Data Source'"/>
        <xsl:with-param name="nodes" select="dataSource"/>
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="methodStep/dataSource">
  </xsl:template>
  

  <xsl:template match="methods/sampling">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Sampling Coverage'"/>
      <xsl:with-param name="nodes" select="studyExtent/coverage"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Study Extent Description'"/>
      <xsl:with-param name="nodes" select="studyExtent/description"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Sampling Description'"/>
      <xsl:with-param name="nodes" select="samplingDescription"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Sampling Unit Reference'"/>
      <xsl:with-param name="value" select="spatialSamplingUnits/referenceEntityId"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Sampling Unit Location'"/>
      <xsl:with-param name="nodes" select="spatialSamplingUnits/coverage"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Sampling Citation'"/>
      <xsl:with-param name="nodes" select="citation"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="sampling/studyExtent/description | sampling/samplingDescription">
    <xsl:call-template name="text"/>
  </xsl:template>


  <xsl:template match="methods/qualityControl">
    <div class="field-group">
      <div class="field-group-heading"><xsl:text>Quality Control Step </xsl:text><xsl:value-of select="position()"/></div>
      <xsl:call-template name="step"/>
    </div>
  </xsl:template>

</xsl:stylesheet>

