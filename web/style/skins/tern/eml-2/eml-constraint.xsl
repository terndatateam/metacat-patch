<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <!-- This module is for constraint. And it is self contained-->
  
  <xsl:template match="constraint">
    <xsl:call-template name="constraint"/>
  </xsl:template>
  
  <xsl:template name="constraint">
    <xsl:apply-templates mode="field"/>
  </xsl:template>

  <xsl:template match="constraint/primaryKey" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Primary Key'"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="constraint/uniqueKey" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Unique Key'"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="constraint/checkConstraint" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Checking Constraint'"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="constraint/foreignKey" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Foreign Key'"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="constraint/joinCondition" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Join Condition'"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="constraint/notNullConstraint" mode="field">
    <xsl:call-template name="apply-field-self">
      <xsl:with-param name="label" select="'Not Null Constraint'"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="constraint/*" mode="base">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Name'"/>
      <xsl:with-param name="nodes" select="constraintName"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Description'"/>
      <xsl:with-param name="nodes" select="constraintDescription"/>
    </xsl:call-template>
  </xsl:template>
  
  <!--Keys part-->
  <xsl:template match="constraint/primaryKey | constraint/uniqueKey | constraint/notNullConstraint">
    <xsl:apply-templates select="." mode="base"/>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Key'"/>
      <xsl:with-param name="nodes" select="key/attributeReference"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="constraint/checkConstraint">
    <xsl:apply-templates select="." mode="base"/>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Language'"/>
      <xsl:with-param name="value" select="@language"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Check Condition'"/>
      <xsl:with-param name="value" select="checkCondition"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="constraint/foreignKey">
    <xsl:call-template name="foreignKey"/>
  </xsl:template>
  
  <xsl:template name="foreignKey">
    <xsl:apply-templates select="." mode="base"/>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Key'"/>
      <xsl:with-param name="nodes" select="key/attributeReference"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Entity Reference'"/>
      <xsl:with-param name="value" select="entityReference"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Relationship'"/>
      <xsl:with-param name="value" select="relationshipType"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Cardinality'"/>
      <xsl:with-param name="value" select="cardinality"/>
    </xsl:call-template>
  </xsl:template>
    
  <xsl:template match="foreignKey/cardinality">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Parent'"/>
      <xsl:with-param name="value" select="parentOccurences"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Children'"/>
      <xsl:with-param name="value" select="childOccurences"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="constraint/joinCondition">
    <xsl:call-template name="foreignKey"/>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Referenced Key'"/>
      <xsl:with-param name="nodes" select="referencedKey/attributeReference"/>
    </xsl:call-template>
  </xsl:template>

  
  <xsl:template match="key/attributeReference | referencedKey/attributeReference">
    <span class="label label-default">
      <xsl:value-of select="."/>
    </span>
    <xsl:text> </xsl:text>
  </xsl:template>

</xsl:stylesheet>
