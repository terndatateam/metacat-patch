<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />
  
  <!-- This module only provide some templates. They can be called by other templates-->
  
  <xsl:template name="entity">
    <xsl:param name="entitytype" select="local-name()"/>
    <!--xsl:param name="entityindex" select="position()"/-->
    
    <xsl:variable name="ref_id" select="references"/>
    <xsl:variable name="node" select="(. | $ids[@id=$ref_id])[not(references)]"/>
    
    <div class="{$entitytype} entity">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Identifier'"/>
        <xsl:with-param name="nodes" select="$node/alternateIdentifier"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Name'"/>
        <xsl:with-param name="nodes" select="$node/entityName"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Description'"/>
        <xsl:with-param name="value" select="$node/entityDescription"/>
      </xsl:call-template>
      
      <xsl:if test="$withAttributes='1' or $displaymodule='printall'">
        <xsl:call-template name="apply-field">
          <xsl:with-param name="label" select="'Attribute Information'"/>
          <xsl:with-param name="nodes" select="$node/attributeList"/>
        </xsl:call-template>
      </xsl:if>
      
      <xsl:apply-templates select="$node" mode="entityInfo"/>
      
      <xsl:call-template name="apply-additionalInfo"/>
      
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Physical Structure Description'"/>
        <xsl:with-param name="nodes" select="$node/physical"/>
      </xsl:call-template>
      
      <!-- call physical module without showing distribution -->
      <!--
      <xsl:if test="$node/physical">
        
        <dl class="physical field">
          <dt>Physical Structure Description</dt>
          <dd>
            <xsl:apply-templates select="$node/physical">
              <xsl:with-param name="level">entitylevel</xsl:with-param>
              <xsl:with-param name="entitytype" select="$entitytype"/>
              <xsl:with-param name="entityindex" select="$entityindex"/>
            </xsl:apply-templates>
          </dd>
        </dl>
      </xsl:if>
      -->
      
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Coverage Description'"/>
        <xsl:with-param name="nodes" select="$node/coverage"/>
      </xsl:call-template>
      
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Method Description'"/>
        <xsl:with-param name="nodes" select="$node/methods"/>
      </xsl:call-template>
      
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Constraint'"/>
        <xsl:with-param name="nodes" select="$node/constraint"/>
      </xsl:call-template>

    </div>
  </xsl:template>

    
  <xsl:template match="entityName">
    <strong><xsl:value-of select="."/></strong>
  </xsl:template>


<!--
  <xsl:choose>
  <xsl:when test="$displaymodule='entity' or $displaymodule='printall'">
  </xsl:when>
  <xsl:otherwise>
    <xsl:call-template name="chooseattributelist"/>
  </xsl:otherwise>
  </xsl:choose>
-->

</xsl:stylesheet>
