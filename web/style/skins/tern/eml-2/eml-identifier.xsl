<?xml version="1.0"?>
<!--
  *  Copyright: 2000 Regents of the University of California and the National Center for Ecological Analysis and Synthesis
  *             2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Matthew Brooke, Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />
  
  <xsl:variable name="partialURL">/metacat/<xsl:value-of select="$packageId"/>/html</xsl:variable>
  <xsl:variable name="docURL">
    <xsl:choose>
      <xsl:when test="boolean($registryurl)">
        <xsl:value-of select="concat($registryurl,$partialURL)"/>
      </xsl:when>
      <xsl:when test="boolean($repositoryURL)">
        <xsl:value-of select="concat($repositoryURL,$partialURL)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat($contextURL,$partialURL)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
	<!--*************** displays dataset citation********-->
  <xsl:template name="identifierText">
    <xsl:variable name="mainURL">
      <xsl:choose><xsl:when test="alternateIdentifier[@system='doi']">
        <xsl:text>http://dx.doi.org/</xsl:text><xsl:value-of select="alternateIdentifier[@system='doi']"/>
      </xsl:when><xsl:otherwise>
        <xsl:value-of select="$docURL"/>
      </xsl:otherwise></xsl:choose>
    </xsl:variable>
    <xsl:variable name="publisher">
      <xsl:choose><xsl:when test="publisher/organizationName">
        <xsl:value-of select="publisher/organizationName"/>
      </xsl:when><xsl:otherwise>
        <xsl:value-of select="$repositoryName"/>
      </xsl:otherwise></xsl:choose>
    </xsl:variable>
    
    <xsl:apply-templates select="creator" mode="datasetCitation"/>

    <xsl:variable name="pubDate" select="substring(string(pubDate),1,4)"/>
    <xsl:text> (</xsl:text>
    <span id="pubDate">
      <xsl:if test="$pubDate != ''"><xsl:value-of select="$pubDate"/></xsl:if>
      <xsl:if test="$pubDate = ''"><xsl:value-of select="$updatedate"/></xsl:if>
    </span>
    <xsl:text>): </xsl:text>

    <xsl:variable name="ntitle" select="normalize-space(title)"/>
    <b><xsl:value-of select="$ntitle"/></b>
    <xsl:if test="substring($ntitle, string-length($ntitle)) != '.'">
        <xsl:text>.</xsl:text>
    </xsl:if>
    <xsl:text> </xsl:text>
    <xsl:if test="normalize-space($publisher)">
      <xsl:value-of select="$publisher"/><xsl:text>. </xsl:text>
    </xsl:if>
    <!--xsl:variable name="docLongURL" select="concat($tripleURI,$docid)" /-->
    <a href="{$mainURL}"><xsl:value-of select="$mainURL"/></a>
    
  </xsl:template>
  
  <xsl:template name="identifier">
    <xsl:param name="label"/>
    <dl class="resourceCitation field">
      <dt>
        <xsl:if test="$label"><xsl:value-of select="$label"/><xsl:text> </xsl:text></xsl:if>
        <xsl:text>Citation</xsl:text>
      </dt>
      <dd>
        <p>
          <xsl:call-template name="identifierText"/>
        </p>
      </dd>
    </dl>
  </xsl:template>

  
  <!--************** creates citation for a creator in "Last, FM" format **************-->
  <xsl:template match="creator" mode="datasetCitation">
    <xsl:variable name="ref_id" select="references"/>
    <xsl:variable name="node" select="(. | $ids[@id=$ref_id])[not(references)]"/>
    
    <xsl:if test="normalize-space($node/individualName)">
      <xsl:value-of select="$node/individualName/surName"/>
      <xsl:if test="normalize-space($node/individualName/givenName)">
        <xsl:if test="normalize-space($node/individualName/surName)">
          <xsl:text>, </xsl:text>
          <xsl:for-each select="$node/individualName/givenName">
            <xsl:value-of select="substring(.,1,1)"/>
            <xsl:if test="position() != last()">
              <xsl:text> </xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="not(normalize-space($node/individualName/surName))">
          <xsl:for-each select="$node/individualName/givenName">
            <xsl:value-of select="."/>
            <xsl:if test="position() != last()">
              <xsl:text> </xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
      </xsl:if>
    </xsl:if>
    <xsl:if test="not(normalize-space($node/individualName))"> 
      <xsl:value-of select="$node/organizationName"/>
    </xsl:if>
    <xsl:if test="position() != last()">
      <xsl:text>; </xsl:text>
    </xsl:if>
    
      <!--
    <xsl:if test="string(individualName/surName) != ''"> 
      <xsl:if test="string(organizationName) != ''"> of </xsl:if>
    </xsl:if>
    <xsl:for-each select="organizationName">
      <xsl:value-of select="."/>
    </xsl:for-each>
      -->
  </xsl:template>
  
</xsl:stylesheet>
