<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="nonNumericDomain">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Enumerated Domain'"/>
        <xsl:with-param name="nodes" select="enumeratedDomain"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Text Domain'"/>
        <xsl:with-param name="nodes" select="textDomain"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="nonNumericDomain/textDomain">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Definition'"/>
      <xsl:with-param name="value" select="definition"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Pattern'"/>
      <xsl:with-param name="nodes" select="pattern"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Source'"/>
      <xsl:with-param name="value" select="source"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="textDomain/pattern">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="nonNumericDomain/enumeratedDomain">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Code Definition'"/>
      <xsl:with-param name="nodes" select="codeDefinition"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'External Code Set'"/>
      <xsl:with-param name="nodes" select="externalCodeSet"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Entity Code List'"/>
      <xsl:with-param name="nodes" select="entityCodeList"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="enumeratedDomain/codeDefinition">
    <dl class="field">
      <dt>
        <xsl:if test="normalize-space(@order)">
          <xsl:value-of select="@order"/>
          <xsl:text>: </xsl:text>
        </xsl:if>
        <xsl:value-of select="code"/>
      </dt>
      <dd><xsl:value-of select="definition"/></dd>
      <xsl:if test="normalize-space(source)">
        <dd>(<xsl:value-of select="source"/>)</dd>
      </xsl:if>
    </dl>
  </xsl:template>
  
  <xsl:template match="enumeratedDomain/externalCodeSet">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Set Name'"/>
      <xsl:with-param name="value" select="codesetName"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Citation'"/>
      <xsl:with-param name="nodes" select="citation"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'URL'"/>
      <xsl:with-param name="nodes" select="codesetURL"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="externalCodeSet/citation">
    <xsl:call-template name="citation"/>
  </xsl:template>
  
  <xsl:template match="externalCodeSet/codesetURL">
    <a><xsl:attribute name="href"><xsl:value-of select="."/></xsl:attribute><xsl:value-of select="."/></a>
  </xsl:template>

              
  <xsl:template match="enumeratedDomain/entityCodeList">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Entity Reference'"/>
      <xsl:with-param name="value" select="entityReference"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Attribute Value Reference'"/>
      <xsl:with-param name="value" select="valueAttributeReference"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Attribute Definition Reference'"/>
      <xsl:with-param name="value" select="definitionAttributeReference"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Attribute Definition Order'"/>
      <xsl:with-param name="value" select="orderAttributeReference"/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
