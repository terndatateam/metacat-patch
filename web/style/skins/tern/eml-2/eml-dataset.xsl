<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="references" mode="dataset">
    <xsl:variable name="ref_id" select="."/>
    <xsl:apply-templates select="$ids[@id=$ref_id]" mode="dataset">
      <xsl:with-param name="ref" select="true()"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="dataset" mode="dataset">
    <xsl:apply-templates select="references" mode="dataset"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="apply-datasetidentifier"/>
      <xsl:call-template name="apply-shortName"/>
      <!--
      <xsl:call-template name="title"/>
      <xsl:call-template name="pubDate"/>
      -->
      <xsl:call-template name="apply-language"/>
      <xsl:call-template name="apply-series"/>
      <xsl:call-template name="apply-creator">
        <xsl:with-param name="creator">Data Creators</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="apply-abstract"/>
      <xsl:call-template name="apply-contact"/>
      <xsl:call-template name="apply-project"/>
      <xsl:call-template name="apply-methods"/>
      <xsl:call-template name="apply-associatedParty"/>
      <xsl:call-template name="apply-keywordSet"/>
      <xsl:apply-templates select="coverage"/>
      <xsl:call-template name="apply-additionalInfo"/>
      <xsl:call-template name="apply-intellectualRights"/>
      <xsl:call-template name="apply-distribution">
        <xsl:with-param name="level">toplevel</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="apply-access"/>
      
      <xsl:call-template name="apply-metadataProvider"/>
      <xsl:call-template name="apply-publisher"/>
      <xsl:call-template name="apply-pubPlace"/>
      
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Purpose'"/>
        <xsl:with-param name="nodes" select="purpose"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Maintenance'"/>
        <xsl:with-param name="nodes" select="maintenance"/>
      </xsl:call-template>
      
      <xsl:if test="$withEntityLinks='1' or $displaymodule='printall'">
        <xsl:call-template name="apply-datasetentity"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template name="apply-datasetidentifier">
    <dl class="identifier field">
      <dt>Identifier</dt>
      <dd>
        <dl class="field-label field-label-identifier-primary">
          <dt>docid</dt><dd id="docid"><xsl:value-of select="$packageId"/></dd>
        </dl>
        <xsl:if test="alternateIdentifier">
          <xsl:text> &#160; </xsl:text>
          <xsl:apply-templates select="alternateIdentifier"/>
        </xsl:if>
      </dd>
    </dl>
  </xsl:template>

  <xsl:template match="dataset/purpose">
    <xsl:call-template name="text"/>
  </xsl:template>

  <xsl:template match="dataset/maintenance">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Description'"/>
      <xsl:with-param name="nodes" select="description"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Frequency'"/>
      <xsl:with-param name="value" select="maintenanceUpdateFrequency"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'History'"/>
      <xsl:with-param name="nodes" select="changeHistory"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="maintenance/description">
    <xsl:call-template name="text"/>
  </xsl:template>

  <xsl:template match="maintenance/changeHistory">
    <div class="field-group">
      <dl class="field"><dt>Scope</dt><dd><xsl:value-of select="changeScope"/></dd></dl>
      <dl class="field"><dt>Old value</dt><dd><xsl:value-of select="oldValue"/></dd></dl>
      <dl class="field"><dt>Change date</dt><dd><xsl:value-of select="changeDate"/></dd></dl>
      <xsl:if test="comment and normalize-space(comment)">
        <dl class="field"><dt>Comment</dt><dd><xsl:value-of select="comment"/></dd></dl>
      </xsl:if>
    </div>
  </xsl:template>

  <xsl:template match="contact">
    <xsl:call-template name="party">
      <xsl:with-param name="show-contact-details" select="true()"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="dataset/publisher">
    <xsl:call-template name="party"/>
  </xsl:template>

  <xsl:template name="apply-contact">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Contacts for Questions on the Use and Interpretation of Data'"/>
      <xsl:with-param name="nodes" select="contact"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-publisher">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Publisher'"/>
      <xsl:with-param name="nodes" select="publisher"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-pubPlace">
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Publish Place'"/>
      <xsl:with-param name="nodes" select="pubPlace"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="apply-methods">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Methods and Sampling Information'"/>
      <xsl:with-param name="nodes" select="methods"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-project">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Project Information and Data Owners'"/>
      <xsl:with-param name="nodes" select="project"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="apply-datasetentity" >

  <!--
    <xsl:if test="$displaymodule='printall' or $displaymodule='dataset'">
      <xsl:call-template name="xml"/>
    </xsl:if>
  -->
    <div class="datasetEntity">
      <xsl:call-template name="listEntities">
        <xsl:with-param name="title">Data Table</xsl:with-param>
        <xsl:with-param name="entity" select="dataTable"/>
      </xsl:call-template>
      <xsl:call-template name="listEntities">
        <xsl:with-param name="title">Spatial Raster</xsl:with-param>
        <xsl:with-param name="entity" select="spatialRaster"/>
      </xsl:call-template>
      <xsl:call-template name="listEntities">
        <xsl:with-param name="title">Spatial Vector</xsl:with-param>
        <xsl:with-param name="entity" select="spatialVector"/>
      </xsl:call-template>
      <xsl:call-template name="listEntities">
        <xsl:with-param name="title">Stored Procedure</xsl:with-param>
        <xsl:with-param name="entity" select="storedProcedure"/>
      </xsl:call-template>
      <xsl:call-template name="listEntities">
        <xsl:with-param name="title">View</xsl:with-param>
        <xsl:with-param name="entity" select="view"/>
      </xsl:call-template>
      <xsl:call-template name="listEntities">
        <xsl:with-param name="title">Other Entity</xsl:with-param>
        <xsl:with-param name="entity" select="otherEntity"/>
      </xsl:call-template>
    </div>
  </xsl:template>
  
  <xsl:template name="listEntities">
    <xsl:param name="title"/>
    <xsl:param name="entity"/>
    <xsl:if test="count($entity) > 0">
      <xsl:variable name="entitytype" select="local-name($entity)"/>
      <dl class="{$entitytype}List field">
        <dt><xsl:value-of select="$title"/></dt>
        <dd><xsl:apply-templates select="$entity" mode="datasetentity"/></dd>
      </dl>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="*" mode="datasetentity">
    <div class="field-group field-group-collapsible">
      <div class="field-group-heading">
        <span class="field-group-toggle" data-title-show="Show Metadata" data-title-hide="Hide Metadata">
          <span class="filename"><xsl:value-of select="./entityName"/></span>
        </span>
        <xsl:call-template name="entityurl"/>
      </div>
      <div class="field-group-body">
        <!--xsl:call-template name="entity"/-->
        <xsl:apply-templates select="."/>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="entityurl">
    <xsl:param name="type" select="local-name(.)"/>
    <xsl:param name="index" select="count(preceding-sibling::*[local-name()=$type])+1"/>
    <a class="entity-metadata-link" target="_blank" href="{$tripleURI}{$docid}&amp;displaymodule=entity&amp;entitytype={$type}&amp;entityindex={$index}" title="View entity metadata in new window">
      <!--View Metadata-->
      <span class="glyphicon glyphicon-new-window"></span>
    </a>
  </xsl:template>

<!--
  <xsl:template match="text()" mode="dataset" />
  <xsl:template match="text()" mode="resource" />
    <xsl:template name="listEntities">
      <xsl:param name="title"/>
      <xsl:param name="entity"/>
      <xsl:if test="count($entity) > 0">
        <xsl:variable name="entitytype" select="local-name($entity)"/>
        <dl class="{$entitytype}List field">
          <dt><xsl:value-of select="$title"/></dt>
          <dd>
            <xsl:for-each select="$entity">
              <div class="field-group field-group-collapsible">
                <div class="field-group-heading">
                  <span class="field-group-toggle" data-title-show="Show Metadata" data-title-hide="Hide Metadata">
                    <span class="filename"><xsl:value-of select="./entityName"/></span>
                  </span>
                  <span class="actions"></span>
                </div>
                <div class="field-group-body">
                  <xsl:call-template name="entity"/>
                </div>
              </div>
            </xsl:for-each>
            <xsl:choose>
            <xsl:when test="$displaymodule!='printall'">
              <ul>
              <xsl:for-each select="$entity">
                <li>
                <xsl:call-template name="entityurl">
                  <xsl:with-param name="type" select="$entitytype"/>
                  <xsl:with-param name="showtype" select="$title"/>
                  <xsl:with-param name="index" select="position()"/>
                </xsl:call-template>
                </li>
              </xsl:for-each>
              </ul>
            </xsl:when>
            <xsl:otherwise>
              <xsl:for-each select="$entity">
                <xsl:call-template name="entity"/>
              </xsl:for-each>
                <xsl:for-each select="../.">
                <xsl:call-template name="chooseentity">
                  <xsl:with-param name="entitytype" select="$entitytype"/>
                  <xsl:with-param name="entityindex" select="position()"/>
                </xsl:call-template>
                </xsl:for-each>
            </xsl:otherwise>
            </xsl:choose>
          </dd>
        </dl>
      </xsl:if>
    </xsl:template>
-->    
  
</xsl:stylesheet>
