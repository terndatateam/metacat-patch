<?xml version="1.0" encoding="utf-8"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" />

  <xsl:template match="additionalMetadata" mode="modal">
    <div class="info-modal">
      <a href="#" class="btn btn-info info-modal-toggle"><xsl:value-of select="name(metadata/*)"/></a>
      <div class="info-modal-title">
        <xsl:text>Additional Metadata : </xsl:text><xsl:value-of select="name(metadata/*)"/>
      </div>
      <div class="info-modal-body">
        <xsl:call-template name="additionalMetadata"/>
      </div>
    </div>
  </xsl:template>
<!--
  <xsl:template match="additionalMetadata">
    <div class="field-group">
      <div class="field-group-heading"><xsl:value-of select="name(metadata/*)"/></div>
      <xsl:call-template name="additionalMetadata"/>
    </div>
  </xsl:template>
-->
  <xsl:template name="additionalMetadata">
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Describes'"/>
      <xsl:with-param name="nodes" select="describes"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Metadata'"/>
      <xsl:with-param name="nodes" select="metadata/*"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="additionalMetadata/metadata//*">
    <dl class="field">
      <dt><xsl:value-of select="name(.)"/></dt>
      <dd>
        <xsl:apply-templates select="@*" mode="additionalMetadata"/>
        <xsl:apply-templates select="*"/>
        <xsl:value-of select="text()"/>
      </dd>
    </dl>
  </xsl:template>

  <xsl:template match="additionalMetadata/metadata/*//@*" mode="additionalMetadata">
    <xsl:if test="normalize-space(.)">
      <dl class="field">
        <dt><xsl:text>@</xsl:text><xsl:value-of select="name(.)"/></dt>
        <dd>
          <xsl:value-of select="."/>
        </dd>
      </dl>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
