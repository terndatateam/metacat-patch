<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" />
  
  <!-- This module is for spatialRaster module-->
  
  <xsl:template match="spatialRaster">
    <xsl:call-template name="entity"/>
  </xsl:template>

  <xsl:template match="spatialRaster" mode="entityInfo">
    <xsl:apply-templates select="spatialReference" mode="complexfield">
      <xsl:with-param name="label">Spatial Reference</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="georeferenceInfo" mode="complexfield">
      <xsl:with-param name="label">Grid Position</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="horizontalAccuracy" mode="complexfield">
      <xsl:with-param name="label">Horizontal Accuracy</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="verticalAccuracy" mode="complexfield">
      <xsl:with-param name="label">Vertical Accuracy</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="cellSizeXDirection" mode="simplefield">
      <xsl:with-param name="label">Cell Size(X)</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="cellSizeYDirection" mode="simplefield">
      <xsl:with-param name="label">Cell Size(Y)</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="numberOfBands" mode="simplefield">
      <xsl:with-param name="label">Number of Bands</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="rasterOrigin" mode="simplefield">
      <xsl:with-param name="label">Origin</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="columns" mode="simplefield">
      <xsl:with-param name="label">Max Raster Objects(X)</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="rows" mode="simplefield">
      <xsl:with-param name="label">Max Raster Objects(Y)</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="verticals" mode="simplefield">
      <xsl:with-param name="label">Max Raster Objects(Z)</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="cellGeometry" mode="simplefield">
      <xsl:with-param name="label">Cell Geometry</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="toneGradation" mode="simplefield">
      <xsl:with-param name="label">Number of Colors</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="scaleFactor" mode="simplefield">
      <xsl:with-param name="label">Scale Factor</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="offset" mode="simplefield">
      <xsl:with-param name="label">Offset</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="imageDescription" mode="complexfield">
      <xsl:with-param name="label">Image Information</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>


  <xsl:template match="spatialRaster/spatialReference | spatialVector/spatialReference">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:apply-templates select="horizCoordSysName" mode="simplefield">
        <xsl:with-param name="label">Name of Coordinate System</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="horizCoordSysDef/geogCoordSys" mode="field"/>
      <xsl:apply-templates select="horizCoordSysDef/projCoordSys" mode="field"/>
      <xsl:apply-templates select="vertCoordSys/altitudeSysDef" mode="complexfield">
        <xsl:with-param name="label">Altitude System Definition</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="vertCoordSys/depthSysDef" mode="complexfield">
        <xsl:with-param name="label">Depth System Definition</xsl:with-param>
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>

  <xsl:template match="horizCoordSysDef/geogCoordSys | projCoordSys/geogCoordSys" mode="field">
    <xsl:apply-templates select="geogCoordSys" mode="complexfield">
      <xsl:with-param name="label">
        <xsl:text>Definition of </xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text> (Geographic Coordinate System)</xsl:text>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="horizCoordSysDef/geogCoordSys | projCoordSys/geogCoordSys">
    <xsl:apply-templates select="datum/@name" mode="simplefield">
      <xsl:with-param name="label">Datum</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="spheroid" mode="complexfield">
      <xsl:with-param name="label">Spheroid</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="primeMeridian" mode="complexfield">
      <xsl:with-param name="label">Prime Meridian</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="unit/@name" mode="simplefield">
      <xsl:with-param name="label">Unit</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="geogCoordSys/spheroid">
    <xsl:apply-templates select="@name" mode="simplefield">
      <xsl:with-param name="label">Name</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="@semiAxisMajor" mode="simplefield">
      <xsl:with-param name="label">Semi Axis Major</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="@denomFlatRatio" mode="simplefield">
      <xsl:with-param name="label">Denom Flat Ratio</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="geogCoordSys/primeMeridian">
    <xsl:apply-templates select="@name" mode="simplefield">
      <xsl:with-param name="label">Name</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="@longitude" mode="simplefield">
      <xsl:with-param name="label">Longitude</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="horizCoordSysDef/projCoordSys" mode="field">
    <xsl:apply-templates select="geogCoordSys" mode="field"/>
    <xsl:apply-templates select="projection" mode="complexfield">
      <xsl:with-param name="label">Projection in Geo Coord. System</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="projCoordSys/projection">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="Parameter"/>
      <xsl:with-param name="nodes" select="parameter"/>
    </xsl:call-template>
    <xsl:apply-templates select="unit/@name" mode="simplefield">
      <xsl:with-param name="label" select="'Unit'"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="projection/parameter">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="@name"/>
      <xsl:with-param name="value">
        <xsl:value-of select="@value"/>
        <xsl:if test="@description">
          <xsl:text> (</xsl:text><xsl:value-of select="@description"/><xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="vertCoordSys/altitudeSysDef | vertCoordSys/depthSysDef">
    <xsl:apply-templates select="altitudeDatumName|depthDatumName" mode="simplefield">
      <xsl:with-param name="label">Datum</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="altitudeResolution|depthResolution" mode="simplefield">
      <xsl:with-param name="label">Resolution</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="altitudeDistanceUnits|depthDistanceUnits" mode="simplefield">
      <xsl:with-param name="label">Distance Unit</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="altitudeEncodingMethod|depthEncodingMethod" mode="simplefield">
      <xsl:with-param name="label">Encoding Method</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>


  <xsl:template match="georeferenceInfo">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Corner Point'"/>
      <xsl:with-param name="nodes" select="cornerPoint"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Control Point'"/>
      <xsl:with-param name="nodes" select="controlPoint"/>
    </xsl:call-template>
    <xsl:apply-templates select="bilinearFit" mode="complexfield">
      <xsl:with-param name="label">Bilinear Fit</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="georeferenceInfo/cornerPoint">
    <dl class="field">
      <dt><xsl:value-of select="corner"/></dt>
      <dd>
        <xsl:apply-templates select="xCoordinate" mode="simplefield">
          <xsl:with-param name="label">X Coordinate</xsl:with-param>
        </xsl:apply-templates>
        <xsl:apply-templates select="yCoordinate" mode="simplefield">
          <xsl:with-param name="label">Y Coordinate</xsl:with-param>
        </xsl:apply-templates>
        <xsl:apply-templates select="pointInPixel" mode="simplefield">
          <xsl:with-param name="label">Point in Pixel</xsl:with-param>
        </xsl:apply-templates>
      </dd>
    </dl>
  </xsl:template>

  <xsl:template match="georeferenceInfo/controlPoint">
    <div class="field-group">
      <xsl:apply-templates select="column" mode="simplefield">
        <xsl:with-param name="label">Column Location</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="row" mode="simplefield">
        <xsl:with-param name="label">Row Location</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="xCoordinate" mode="simplefield">
        <xsl:with-param name="label">X Coordinate</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="yCoordinate" mode="simplefield">
        <xsl:with-param name="label">Y Coordinate</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="pointInPixel" mode="simplefield">
        <xsl:with-param name="label">Point in Pixel</xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="georeferenceInfo/bilinearFit">
    <xsl:apply-templates select="xIntercept" mode="simplefield">
      <xsl:with-param name="label">X Intercept</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="xSlope" mode="simplefield">
      <xsl:with-param name="label">X Slope</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="yIntercept" mode="simplefield">
      <xsl:with-param name="label">Y Intercept</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="ySlope" mode="simplefield">
      <xsl:with-param name="label">Y Slope</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="spatialRaster/horizontalAccuracy | spatialRaster/verticalAccuracy">
    <xsl:call-template name="dataQuality"/>
  </xsl:template>
  
  <xsl:template name="dataQuality">
    <xsl:apply-templates select="accuracyReport" mode="simplefield">
      <xsl:with-param name="label">Report</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="quantitativeAccuracyReport" mode="complexfield">
      <xsl:with-param name="label">Quantitative Report</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="quantitativeAccuracyReport">
    <xsl:apply-templates select="quantitativeAccuracyValue" mode="simplefield">
      <xsl:with-param name="label">Accuracy Value</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="quantitativeAccuracyMethod" mode="complexfield">
      <xsl:with-param name="label">Method</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="imageDescription">
    <xsl:apply-templates select="illuminationElevationAngle" mode="simplefield">
      <xsl:with-param name="label">Illumination Elevation</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="illuminationAzimuthAngle" mode="simplefield">
      <xsl:with-param name="label">Illumination Azimuth</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="imageOrientationAngle" mode="simplefield">
      <xsl:with-param name="label">Image Orientation</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="imagingCondition" mode="simplefield">
      <xsl:with-param name="label">Code Affecting Quality of Image</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="imageQualityCode" mode="simplefield">
      <xsl:with-param name="label">Quality</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="cloudCoverPercentage" mode="simplefield">
      <xsl:with-param name="label">Cloud Coverage</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="preProcessingTypeCode" mode="simplefield">
      <xsl:with-param name="label">Pre-processing</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="compressionGenerationQuality" mode="simplefield">
      <xsl:with-param name="label">Compression Quality</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="triangulationIndicator" mode="simplefield">
      <xsl:with-param name="label">Triangulation Indicator</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="radionmetricDataAvailability" mode="simplefield">
      <xsl:with-param name="label">Availability of Radionmetric Data</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="cameraCalibrationInformationAvailability" mode="simplefield">
      <xsl:with-param name="label">Availability of Camera Calibration Correction</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="filmDistortionInformationAvailability" mode="simplefield">
      <xsl:with-param name="label">Availability of Calibration Reseau</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="lensDistortionInformationAvailability" mode="simplefield">
      <xsl:with-param name="label">Availability of Lens Aberration Correction</xsl:with-param>
    </xsl:apply-templates>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Band Description'"/>
      <xsl:with-param name="nodes" select="bandDescription"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="imageDescription/bandDescription">
    <div class="field-group">
      <xsl:apply-templates select="sequenceIdentifier" mode="simplefield">
        <xsl:with-param name="label">Sequence Identifier</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="highWavelength" mode="simplefield">
        <xsl:with-param name="label">High Wave Length</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="lowWaveLength" mode="simplefield">
        <xsl:with-param name="label">Low Wave Length</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="waveLengthUnits" mode="simplefield">
        <xsl:with-param name="label">Wave Length Units</xsl:with-param>
      </xsl:apply-templates>
      <xsl:apply-templates select="peakResponse" mode="simplefield">
        <xsl:with-param name="label">Peak Response</xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

</xsl:stylesheet>
