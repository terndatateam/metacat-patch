<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="eml-text.xsl" />
  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <!-- Distribution Info sections -->
  <xsl:template name="apply-distribution">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Distribution'"/>
      <xsl:with-param name="nodes" select="distribution"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="distribution">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <div class="field-group">
        <xsl:apply-templates select="*" mode="distributionHeading"/>
        <xsl:apply-templates select="*" mode="field"/>
      </div>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="distribution/online" mode="distributionHeading">
    <div class="field-group-heading">Online</div>
  </xsl:template>
  <xsl:template match="distribution/offline" mode="distributionHeading">
    <div class="field-group-heading">Offline</div>
  </xsl:template>
  <xsl:template match="distribution/inline" mode="distributionHeading">
    <div class="field-group-heading">Inline Data</div>
  </xsl:template>
  <xsl:template match="*" mode="distributionHeading">
  </xsl:template>

  <!-- ********************************************************************* -->
  <!-- *******************************  Online data  *********************** -->
  <!-- ********************************************************************* -->
  <xsl:template match="distribution/online" mode="field">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Description'"/>
      <xsl:with-param name="value" select="onlineDescription"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'URL'"/>
      <xsl:with-param name="nodes" select="url"/>
    </xsl:call-template>
    <xsl:apply-templates select="connection"/>
    <xsl:apply-templates select="connectionDefinition"/>
  </xsl:template>
  
  <xsl:template match="online/url">
    <xsl:variable name="docIdRaw">
      <xsl:if test="starts-with(.,'ecogrid')">
        <xsl:value-of select="substring-after(substring-after(., 'ecogrid://'), '/')"/>
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="docId" select="normalize-space($docIdRaw)"/>
    <xsl:variable name="urlLink">
      <xsl:choose>
        <xsl:when test="$docId">
          <xsl:value-of select="$servletURI"/><![CDATA[?action=read&docid=]]><xsl:value-of select="$docId"/>
          <!--xsl:value-of select="$tripleURI"/><xsl:value-of select="$docId"/-->
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="not(contains(.,'://'))">http://</xsl:if><xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="urlLabel">
      <xsl:choose>
        <xsl:when test="$docId">
          <xsl:value-of select="$docId"/>
        </xsl:when>
        <xsl:otherwise>
          <!--xsl:text>External Link</xsl:text-->
          <xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="local">
      <xsl:if test="$docId">-local</xsl:if>
    </xsl:variable>
    
    <xsl:if test="$withHTMLLinks='1'">
      <a href="{$urlLink}" target="_blank" class="download-link{$local}"><xsl:value-of select="$urlLabel"/></a>
    </xsl:if>
    <xsl:if test="$withHTMLLinks='0'">
      <xsl:value-of select="."/>
    </xsl:if>
  </xsl:template>


  <xsl:template match="online/connection">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:apply-templates select="connectionDefinition"/>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Parameter'"/>
        <xsl:with-param name="nodes" select="parameter"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="connection/parameter">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="name"/>
      <xsl:with-param name="value" select="value"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="online/connectionDefinition|connection/connectionDefinition">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Scheme Name'"/>
        <xsl:with-param name="value" select="schemeName"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Scheme Description'"/>
        <xsl:with-param name="nodes" select="description"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Parameter Definition'"/>
        <xsl:with-param name="nodes" select="parameterDefinition"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="connectionDefinition/description">
    <xsl:call-template name="text"/>
  </xsl:template>

  <xsl:template match="connectionDefinition/parameterDefinition">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="name"/>
      <xsl:with-param name="value">
        <xsl:value-of select="definition" />
        <xsl:if test="normalize-space(defaultValue)">
          <xsl:text>(default: </xsl:text>
          <xsl:value-of select="defaultValue" />
          <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- ********************************************************************* -->
  <!-- *******************************  Offline data  ********************** -->
  <!-- ********************************************************************* -->

  <xsl:template match="distribution/offline" mode="field">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Name'"/>
      <xsl:with-param name="value" select="mediumName"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Density'"/>
      <xsl:with-param name="value">
        <xsl:value-of select="mediumDensity"/>
        <xsl:if test="normalize-space(mediumDensityUnits)">
          <xsl:text> (</xsl:text>
          <xsl:value-of select="mediumDensityUnits"/>
          <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Volume'"/>
      <xsl:with-param name="value" select="mediumVol"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Format'"/>
      <xsl:with-param name="nodes" select="mediumFormat"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Notes'"/>
      <xsl:with-param name="value" select="mediumNote"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="distribution/offline/mediumFormat">
    <xsl:if test="position() &gt; 1">
      <xsl:text> , </xsl:text>
    </xsl:if>
    <xsl:value-of select="."/>
  </xsl:template>

  <!-- ********************************************************************* -->
  <!-- *******************************  Inline data  *********************** -->
  <!-- ********************************************************************* -->
  
  <xsl:template match="distribution/inline" mode="field">
    <xsl:param name="level" select="'entitylevel'"/>
    <xsl:param name="entitytype" select="local-name(../../..)"/>
    <xsl:param name="entityindex" select="count(../../../preceding-sibling::*[local-name()=$entitytype])+1"/>
    <xsl:param name="physicalindex" select="count(../../preceding-sibling::physical)+1"/>
    <xsl:param name="distributionindex" select="count(../preceding-sibling::distribution)+1"/>
    <xsl:variable name="urlbase" select="concat($tripleURI,$docid,'&amp;displaymodule=inlinedata&amp;distributionlevel=',$level,'&amp;distributionindex=',$distributionindex)"/>
    <xsl:variable name="url">
      <xsl:value-of select="$urlbase"/>
      <xsl:if test="$level='entitylevel'">
        <xsl:text>&amp;entitytype=</xsl:text>
        <xsl:value-of select="$entitytype"/>
        <xsl:text>&amp;entityindex=</xsl:text>
        <xsl:value-of select="$entityindex"/>
        <xsl:text>&amp;physicalindex=</xsl:text>
        <xsl:value-of select="$physicalindex"/>
      </xsl:if>
    </xsl:variable>

    <xsl:if test="$withHTMLLinks='1'">
      <a target="_blank" href="{$url}"><b>View inline data in new window</b><xsl:text> </xsl:text><span class="glyphicon glyphicon-new-window"></span></a>
    </xsl:if>
    <xsl:if test="$withHTMLLinks='0'">
      <xsl:value-of select="$url"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="inline" mode="data">
    <pre><xsl:value-of select="."/></pre>
  </xsl:template>

</xsl:stylesheet>
