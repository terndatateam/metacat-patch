<?xml version="1.0"?>
<!--
  *  Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
  *     Author: Alvin Sebastian
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * This is an XSLT (http://www.w3.org/TR/xslt) stylesheet designed to
  * module of the Ecological Metadata Language (EML) into an HTML format
  * suitable for rendering with modern web browsers.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="utf-8" indent="yes" />

  <xsl:template match="software">
    <xsl:apply-templates select="references"/>
    <xsl:if test="not(references)">
      <xsl:call-template name="resource">
        <xsl:with-param name="creator">Author</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Implementation Information'"/>
        <xsl:with-param name="nodes" select="implementation"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Dependency'"/>
        <xsl:with-param name="nodes" select="dependency"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field-list">
        <xsl:with-param name="label" select="'License URL'"/>
        <xsl:with-param name="nodes" select="licenseURL"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field-list">
        <xsl:with-param name="label" select="'License'"/>
        <xsl:with-param name="nodes" select="license"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Version Number'"/>
        <xsl:with-param name="value" select="version"/>
      </xsl:call-template>
      <xsl:call-template name="apply-field">
        <xsl:with-param name="label" select="'Project Information'"/>
        <xsl:with-param name="nodes" select="project"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="software/implementation">
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Distribution'"/>
      <xsl:with-param name="nodes" select="distribution"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Size'"/>
      <xsl:with-param name="value" select="size"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Language'"/>
      <xsl:with-param name="nodes" select="language"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Operating System'"/>
      <xsl:with-param name="nodes" select="operatingSystem"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Processor'"/>
      <xsl:with-param name="nodes" select="machineProcessor"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Virtual Machine'"/>
      <xsl:with-param name="value" select="virtualMachine"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Disk Usage'"/>
      <xsl:with-param name="value" select="diskUsage"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Run Time Memory Usage'"/>
      <xsl:with-param name="value" select="runtimeMemoryUsage"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field-list">
      <xsl:with-param name="label" select="'Programming Language'"/>
      <xsl:with-param name="nodes" select="programmingLanguage"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Check Sum'"/>
      <xsl:with-param name="value" select="checksum"/>
    </xsl:call-template>
    <xsl:call-template name="apply-field">
      <xsl:with-param name="label" select="'Dependency'"/>
      <xsl:with-param name="nodes" select="dependency"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="implementation/language">
    <xsl:value-of select="LanguageValue"/>
    <xsl:if test="LanguageCodeStandard">
      <xsl:text> (</xsl:text><xsl:value-of select="LanguageCodeStandard"/><xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="software/dependency | implementation/dependency">
    <div class="field-group">
      <div class="field-group-heading"><xsl:value-of select="action"/></div>
      <xsl:apply-templates select="software"/>
    </div>
  </xsl:template>
  
</xsl:stylesheet>
