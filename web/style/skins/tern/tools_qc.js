jQuery(function($){
  //console.log('tools qc');
  var $modal = $('#toolsModal');
  var $modalTitle = $('#toolsModalTitle');
  var $modalBody = $('.modal-body-main', $modal);
  var $modalBodyWait = $('.modal-body-wait', $modal);
  var $okButton = $('.btn-primary', $modal);
  var $cancelButton = $('.btn-default', $modal);
  
  /** @param {boolean} show */
  var toggleWait = function(show, msg) {
    var methods = ['removeClass', 'addClass'];
    $modalBody[methods[+show]]('hidden');
    $modalBodyWait[methods[+(!show)]]('hidden');
    $('button', $modal)[methods[+show]]('hidden disabled');
    if (msg) $('.wait-message', $modalBodyWait).html(msg);
  }
  
  var updateEml = function(cb) {
    if (!metacat.username) return cb(new Error('You must be logged in.'));
    metacat.eml.get(function(err, eml){
      var packageId;
      if (err == null && eml != null) {
        try {
          packageId = $.trim(eml.documentElement.getAttribute('packageId'));
          if (packageId == metacat.docid) {
            packageId = metacat.increaseRevId(packageId);
            eml.documentElement.setAttribute('packageId', packageId);
            var qc = eml.createElement('qualityControl');
            qc.setAttribute('username', metacat.username);
            qc.setAttribute('timestamp', (new Date()).toJSON());
            var metadata = eml.createElement('metadata');
            metadata.appendChild(qc);
            var additionalMetadata = eml.createElement('additionalMetadata');
            additionalMetadata.appendChild(metadata);
            var emlNode = eml.getElementsByTagName('eml')[0] || eml.getElementsByTagName('eml:eml')[0];
            emlNode.appendChild(additionalMetadata);
            //console.log(emlNode);
          } else {
            err = new Error('docid is out of sync');
          }
        } catch (ex) {
          err = ex;
        }
      }
      if (err) return cb(err);
      else return metacat.eml.upload(packageId, eml, cb);
    });
  }
  
  var approveQC = function(event){
    event.preventDefault();
    toggleWait(false);
    $okButton.one('click', function(){
      toggleWait(true, 'Updating EML. Please wait..');
      updateEml(function(err, ndocid) {
        toggleWait(false);
        $cancelButton.addClass('hidden disabled');
        if (err) {
          var errMsg = err.stack || 'Unknown Error';
          $modalBody.empty().append('<p>Error:</p>').append('<p>' + errMsg + '</p>');
          $okButton.one('click', function() { $modal.modal('hide'); });
        } else {
          $modalBody.empty().append('<p>Success!</p><p>EML has been updated to: ' + ndocid + '</p>');
          $okButton.one('click', function() {
            window.location.href = metacat.url.servlet + '/' + ndocid + '/html';
          });
        }
      });
    });
    $modalTitle.text(oplabels.approve);
    $modalBody.html('<p>Please confirm that you want to approve QC for data package <strong>' + metacat.docidNoRev + '</strong>.</p>');
    $modal.modal('show');
  };
  
  var oplabels = {
    title : "QC Service",
    approve : "Approve Quality Control",
    cancel : "Cancel Quality Control"
  };
  
  var tools = {};
  tools.label = oplabels.title;
  tools.items = [{label:oplabels.approve, handler:approveQC}];
  tools.hasAccess = function(cb){
    var qc = $('.flag-qc-yes');
    if (qc.length) return cb(false);
    // test if logged-in user has write access permission
    metacat.hasWriteAccess(function(hasWriteAccess){
      if (!hasWriteAccess) return cb(false);
      // test if displayed document is the last revision
      metacat.isLastRevision(function(isLastRevision){
        if (!isLastRevision) return cb(false);
        //test if document is public
        metacat.isPublic(function(isPublic){
          return cb(isPublic);
        });
      });
    });
  };
  
  metacat.tools.push(tools);
  
});