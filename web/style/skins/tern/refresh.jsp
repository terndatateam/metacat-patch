<%@ page import="java.util.*, java.util.regex.*" %>
<%!
private static String attrPattern(String attrName, String attrValue) {
    return attrName + "\\s*=\\s*\"\\s*" + attrValue + "\\s*\"";
}
%>
<%
boolean firstTimeRefresh = (application.getAttribute("head") == null);

if (request.getParameter("refresh") != null || firstTimeRefresh) {
    
    String[] removePatterns = {"<title.*?>.*?</title\\s*?>", "<base.*?>"};

    application.setAttribute("bodyBeforeContent", "<body><div id=\"" + LAYOUT_CONTAINERID + "\">");
    application.setAttribute("bodyAfterContent", "</div></body>");
    
    try {
        java.net.URL url = new java.net.URL(LAYOUT_URL);
        java.net.URLConnection uc = url.openConnection();
        java.util.Scanner s = new java.util.Scanner(uc.getInputStream()).useDelimiter("\\A");
        String html = s.hasNext() ? s.next() : "";
        Pattern pattern;
        Matcher matcher;

        //get head part
        String head = "";
        pattern = Pattern.compile("<head\\s*?>(.*?)</head\\s*?>", Pattern.DOTALL);
        matcher = pattern.matcher(html);
        if (matcher.find()) {
            head = matcher.group(1);
        }
        //System.out.println(head);
        if (head.length() > 0) {
          for (int i = 0; i < removePatterns.length; ++i) {
              head = Pattern.compile(removePatterns[i], Pattern.DOTALL).matcher(head).replaceAll("");
          }
        }
        application.setAttribute("head", head);

        //get body part
        String body = "";
        pattern = Pattern.compile("<body.*?</body\\s*?>", Pattern.DOTALL);
        matcher = pattern.matcher(html);
        if (matcher.find()) {
            body = matcher.group();
        }
        //System.err.println(body);
        String bodyDivPattern = "<div.*?\\s+" + attrPattern("id", LAYOUT_CONTAINERID) + ".*?>([^<>]*?)</div";
        String bodySpanPattern = "<span.*?\\s+" + attrPattern("id", LAYOUT_CONTAINERID) + ".*?>([^<>]*?)</span";
        int start = -1;
        int end = -1;
        pattern = Pattern.compile(bodyDivPattern, Pattern.DOTALL);
        matcher = pattern.matcher(body);
        if (matcher.find()) {
            start = matcher.start(1);
            end = matcher.end(1);
        } else {
            pattern = Pattern.compile(bodySpanPattern, Pattern.DOTALL);
            matcher = pattern.matcher(body);
            if (matcher.find()) {
                start = matcher.start(1);
                end = matcher.end(1);
            }
        }
        if (start > 0) {
            String bodyUpper = body.substring(0, start);
            String bodyLower = body.substring(end, body.length());
            application.setAttribute("bodyBeforeContent", bodyUpper);
            application.setAttribute("bodyAfterContent", bodyLower);
        }
    } catch (Exception ex) {
        ex.printStackTrace();
    }
}
%>
