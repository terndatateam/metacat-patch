<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="template_xslt.jsp?search=true"/>
  <xsl:output method="html" doctype-system="about:legacy-compat" encoding="utf-8" indent="yes" />
  <xsl:param name="sessid"/>
  <xsl:param name="qformat">tern</xsl:param>
  <xsl:param name="enableediting">false</xsl:param>
  <xsl:param name="contextURL"/>
  <xsl:param name="cgi-prefix"/>

  <xsl:template name="xsltTitle">
    <xsl:text>Data Packages</xsl:text>
  </xsl:template>
  
  <xsl:template name="xsltContent">
    <div id="searchResultArea">
      <p class="results-info">
        <xsl:variable name="resultsCount" select="count(resultset/document)" />
        <xsl:number value="$resultsCount" />
        <xsl:text> data package</xsl:text>
        <xsl:if test="$resultsCount &gt; 1"><xsl:text>s</xsl:text></xsl:if>
        <xsl:text> found.</xsl:text>
      </p>
      <!-- This tests to see if there are returned documents, if there are not then don't show the query results -->
      <xsl:if test="count(resultset/document) &gt; 0">
        <table class="results-table" width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
          <tr class="table-head">
            <th class="results-table-col-button">
              <div class="btn-group" role="group">
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#" id="resultsExpandAll">Expand all</a></li>
                  <li><a href="#" id="resultsCollapseAll">Collapse all</a></li>
                </ul>
              </div>
            </th>
            <th class="results-table-col-mid"> Title and Description </th>
            <th class="results-table-col-creator"> Creator (People and Organisation)</th>
          </tr>
          <xsl:apply-templates select="/resultset/document">
            <xsl:sort select="param[@name='dataset/title']" />
            <!--xsl:sort select="updatedate" /-->
          </xsl:apply-templates>
        </table>
        <!--
        <xsl:if test="/resultset/previouspage != '-1'">
          <a href="#" class="btn btn-link" id="searchResultPrevPageLink">
            <xsl:attribute name="data-page"><xsl:value-of select="/resultset/previouspage"/></xsl:attribute>
            <span class="glyphicon glyphicon-chevron-left"></span> Previous
          </a>
        </xsl:if>
        <xsl:if test="/resultset/lastpage != 'true'">
          <a href="#" class="btn btn-link" id="searchResultNextPageLink">
            <xsl:attribute name="data-page"><xsl:value-of select="/resultset/nextpage"/></xsl:attribute>
            Next <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </xsl:if>
        -->
      </xsl:if>
    </div> <!-- searchResultArea -->
  </xsl:template>
  
  <xsl:template match="document">
    <xsl:variable name="pubDate">
      <xsl:choose>
        <xsl:when test="param[@name='pubDate']"><xsl:value-of select="param[@name='pubDate']"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="createdate"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <tr class="table-row">
      <td class="results-table-col-button">
        <a href="#" class="collapsible-row-expand-button hidden"><i class="fa fa-plus-square"></i></a>
        <a href="#" class="collapsible-row-collapse-button hidden"><i class="fa fa-minus-square"></i></a>
      </td>
      <td class="results-table-col-mid">
        <xsl:if test="not(param[@name='access/allow/principal']='public')">
          <span class="label label-warning" title="Private Package: This data package is not visible to general public">Private</span>
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="param[@name='dataset/title']">
          <a class="title">
            <xsl:attribute name="href">
              <xsl:call-template name="packageURL"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test="param[@name='dataset/title']!=''"><xsl:value-of select="param[@name='dataset/title']" /> </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of select="param[@name='citation/title']" />
                  <xsl:value-of select="param[@name='software/title']" />
                  <xsl:value-of select="param[@name='protocol/title']" />
                  <xsl:value-of select="param[@name='idinfo/citation/citeinfo/title']" />
              </xsl:otherwise>
            </xsl:choose>
          </a>
        </xsl:if>
        <div class="more-info">
          <xsl:if test="param[@name='creator/individualName/surName'] or param[@name='creator/organizationName']">
            <div class="creator">
            <xsl:if test="param[@name='creator/individualName/surName']">
              <div class="creator-individual"><xsl:apply-templates select="param[@name='creator/individualName/surName']"/></div>
            </xsl:if>
            <xsl:if test="param[@name='creator/organizationName']">
              <div class="creator-organisation"><xsl:apply-templates select="param[@name='creator/organizationName' and not(.=preceding-sibling::*)]"/></div>
            </xsl:if>
            </div>
          </xsl:if>
          <xsl:if test="param[@name='abstract/para']">
            <div class="collapsible" data-collapsible-lines="2">
              <xsl:value-of select="param[@name='abstract/para']" />
            </div>
          </xsl:if>
          <xsl:if test="param[@name='keyword']">
            <div class="collapsible">
              <ul class="keyword">
              <xsl:for-each select="param[@name='keyword']">
                <!--xsl:if test="not(position() = 1)">, </xsl:if-->
                <li><xsl:value-of select="." /></li>
              </xsl:for-each>
              <!--xsl:if test="param[@name='idinfo/keywords/theme/themekey']"><br /></xsl:if-->
              <xsl:for-each select="param[@name='idinfo/keywords/theme/themekey']">
                <!--xsl:if test="not(position() = 1)">, </xsl:if-->
                <li><xsl:value-of select="." /></li>
              </xsl:for-each>
              </ul>
            </div>
          </xsl:if>
          <div class="more-info-extra">
            <xsl:call-template name="qcIndicator">
              <xsl:with-param name="timestamp" select="param[@name='additionalMetadata/metadata/qualityControl/@timestamp']"/>
            </xsl:call-template>
            <xsl:text> &#160; </xsl:text>
            <span title="Number of available data distributions" class="badge"><xsl:number value="count(./param[@name='distribution'])" /></span>
            <xsl:text> &#160; </xsl:text>
            <dl class="field-label field-label-default"><dt>Published</dt><dd><xsl:value-of select="$pubDate"/></dd></dl>
            <xsl:text> &#160; </xsl:text>
            <dl class="field-label field-label-default"><dt>Last updated</dt><dd><xsl:value-of select="updatedate"/></dd></dl>
            <xsl:text> &#160; </xsl:text>
            <dl class="field-label field-label-identifier-primary"><dt>docid</dt><dd><xsl:value-of select="docid"/></dd></dl>
            <xsl:apply-templates select="param[@name='dataset/alternateIdentifier']"/>
          </div>
        </div>
      </td>
      <td class="creator">
        <xsl:if test="param[@name='creator/individualName/surName']">
          <p class="creator-individual collapsible" data-collapsible-lines="2"><xsl:apply-templates select="param[@name='creator/individualName/surName']"/></p>
        </xsl:if>
        <xsl:if test="param[@name='creator/organizationName']">
          <p class="creator-organisation collapsible" data-collapsible-lines="2"><xsl:apply-templates select="param[@name='creator/organizationName' and not(.=preceding-sibling::*)]"/></p>
        </xsl:if>
        <!--xsl:apply-templates select="param[@name='originator/individualName/surName']"/-->
        <!--xsl:apply-templates select="param[@name='originator/organizationName' and not(.=preceding::*)]"/-->
        <!--xsl:apply-templates select="param[@name='idinfo/citation/citeinfo/origin']"/-->
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template match="param[@name='dataset/alternateIdentifier']">
    <xsl:text> &#160; </xsl:text>
    <dl class="field-label field-label-identifier-alternate"><dt>doi</dt><dd><xsl:value-of select="." /></dd></dl>
  </xsl:template>
  
  <xsl:template match="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>; </xsl:text>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
