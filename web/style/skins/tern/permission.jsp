<%@ page language="java" trimDirectiveWhitespaces="true" contentType="application/json; charset=UTF-8" import="java.io.File,edu.ucsb.nceas.metacat.PermissionController,edu.ucsb.nceas.metacat.util.SessionData,edu.ucsb.nceas.metacat.util.RequestUtil"%>
<%@ include file="settings.jsp"%>
<%@ include file="session_vars.jsp"%>
<%!
  public boolean isAuthorized(HttpServletRequest request, String docid, String permission) {
    SessionData sessionData = RequestUtil.getSessionData(request);
    if (sessionData != null) {
      try {
        PermissionController controller = new PermissionController(docid);
        return controller.hasPermission(sessionData.getUserName(), sessionData.getGroupNames(), permission);
      } catch (Exception ex) {}
    }
    return false;
  }
%>
<%
  String[] docid = request.getParameterValues("docid");
  if (docid == null) docid = request.getParameterValues("docid[]");
  //String docidWithoutRev = docid;
  //String[] docidParts = docid.split("\\.");
  //if (docidParts.length > 2) docidWithoutRev = docidParts[0] + "." + docidParts[1];
  String permission = request.getParameter("permission");
  
  if (docid == null || docid.length == 0) {
    response.sendError(400, "docid parameter is required" );
    return;
  }
  if (permission == null || permission.length() == 0) {
    response.sendError(400, "permission parameter is required" );
    return;
  }
  
  String filepath = DATA_FILE_PATH;
  File baseDir = new File(filepath);

  String separator = "";
  out.print("{");
  for (int i = 0; i < docid.length; ++i) {
    if (isAuthorized(request, docid[i], permission)) {
      File dataFile = new File(baseDir, docid[i]);
      long filesize = 0;
      try {
        filesize = dataFile.length();
      } catch(Exception e) {
      }
      out.print(separator + "\"" + docid[i] + "\":");
      out.print("\"" + filesize + "\"");
      separator = ",";
    }
  }
  out.println("}");
%>
