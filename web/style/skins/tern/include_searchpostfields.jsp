<%@ page trimDirectiveWhitespaces="true" %>
<input type="hidden" name="action" value="query" />
<input type="hidden" name="returnfield" value="creator/individualName/surName" />
<input type="hidden" name="returnfield" value="creator/individualName/givenName" />
<input type="hidden" name="returnfield" value="creator/organizationName" />
<input type="hidden" name="returnfield" value="dataset/title" />
<input type="hidden" name="returnfield" value="dataset/alternateIdentifier" />
<input type="hidden" name="returnfield" value="keyword" />
<input type="hidden" name="returnfield" value="abstract/para" />
<input type="hidden" name="returnfield" value="distribution" />
<input type="hidden" name="returnfield" value="additionalMetadata/metadata/qualityControl/@timestamp" />
<input type="hidden" name="returnfield" value="dataset/pubDate" />
<input type="hidden" name="returnfield" value="access/allow/principal" />
<input type="hidden" name="returndoctype" value="metadata" />
<input type="hidden" name="returndoctype" value="eml://ecoinformatics.org/eml-2.1.1" />
<input type="hidden" name="returndoctype" value="eml://ecoinformatics.org/eml-2.1.0" />
<input type="hidden" name="returndoctype" value="eml://ecoinformatics.org/eml-2.0.1" />
<input type="hidden" name="returndoctype" value="eml://ecoinformatics.org/eml-2.0.0" />
<input type="hidden" name="qformat" value="<%=SKIN_NAME%>" />

<% if (SEARCH_TYPE == "advanced") { %>
<input type="hidden" name="operator" value="INTERSECT" />
<% } else { %>
<input type="hidden" name="operator" value="UNION" />
<% } %>
