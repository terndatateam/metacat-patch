var osmAttrib='Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';

function initMainMap(mapNode, mapForm){
  if (!mapNode) return;
  var GEOSERVER_URL = mapForm.getAttribute("data-geoserver-url") + '/wfs';
  var map = L.map(mapNode, {scrollWheelZoom: false}).setView([-27, 140], 3);
  L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution:osmAttrib}).addTo(map);
  L.control.scale().addTo(map);
  var pointStyle = {
    radius: 4,
    fillColor: "#00f",
    stroke: false,
    fillOpacity: 0.5,
    clickable : false
  };
  var boundStyle = {fill:false, weight:1, clickable:false};
  var pointToLayerFn = function (feature, latlng) {
      return L.circleMarker(latlng, pointStyle);
  };
  var geoJsonLayerPoints = L.geoJson(null, {pointToLayer:pointToLayerFn}).addTo(map);
  var geoJsonLayerBounds = L.geoJson(null, {style:boundStyle}).addTo(map);
  //var bounds = [[0,0],[0,0]];
  var parameters = {
    service: 'WFS',
    version: '1.0.0',
    request: 'getFeature',
    typeName: 'metacat:data_points',
    maxFeatures: 3000,
    outputFormat: 'json'
  };
  /*
  var updateBounds = function(bbox) {
    var updated = false;
    if (bounds[0][0] > bbox[1]) {
      updated = true;
      bounds[0][0] = bbox[1]; //south
    }
    if (bounds[0][1] > bbox[0]) {
      updated = true;
      bounds[0][1] = bbox[0]; //west
    }
    if (bounds[1][0] < bbox[3]) {
      updated = true;
      bounds[1][0] = bbox[3]; //north
    }
    if (bounds[1][1] < bbox[2]) {
      updated = true;
      bounds[1][1] = bbox[2]; //east
    }
    if (updated) map.fitBounds(bounds);
  };
  */
  parameters.typeName='metacat:data_points';
  var getPoints = jQuery.ajax({type:"GET", url: GEOSERVER_URL + L.Util.getParamString(parameters), datatype: 'json'});
  parameters.typeName='metacat:data_bounds';
  var getBounds = jQuery.ajax({type:"GET", url: GEOSERVER_URL + L.Util.getParamString(parameters), datatype: 'json'});
  jQuery.when(getPoints, getBounds).done(function(a1, a2){
    var dataPoints = a1[0];
    var dataBounds = a2[0];
    dataPoints.features.forEach(function(feature){
      feature.geometry.type = "Point";
      feature.geometry.coordinates = feature.geometry.coordinates[0][0][0];
    });
    geoJsonLayerPoints.addData(dataPoints);
    geoJsonLayerBounds.addData(dataBounds);
    if (dataPoints.bbox) {
      setTimeout(function(){ 
        map.fitBounds([[dataPoints.bbox[1],dataPoints.bbox[0]],[dataPoints.bbox[3],dataPoints.bbox[2]]]); 
      }, 0);
    }
  });

  map.on('click', function(e){
    if (!mapForm) return;
    var tolerance = 15; // tolerance radius in pixel
    var p = e.containerPoint;
    var nw = map.containerPointToLatLng(L.point(p.x - tolerance, p.y - tolerance));
    var se = map.containerPointToLatLng(L.point(p.x + tolerance, p.y + tolerance));
    mapForm.xmin.value = nw.lng; // western
    mapForm.ymin.value = se.lat; // southern
    mapForm.xmax.value = se.lng; // eastern
    mapForm.ymax.value = nw.lat; //northern
    mapForm.submit();
  });
}

function initSingleMap(mapNode, bounds) {
  if (!mapNode) return;
  var geographicCoverageMap = L.map(mapNode, {scrollWheelZoom: false}).fitBounds(bounds, {maxZoom:10});
  L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution:osmAttrib}).addTo(geographicCoverageMap);
  L.control.scale().addTo(geographicCoverageMap);
  var marker;
  if (bounds[0][0] == bounds[1][0]) {
    marker = L.marker(bounds[0]);
  } else {
    marker = L.rectangle(bounds, {color: "#ff7800", weight: 1});
  }
  marker.addTo(geographicCoverageMap);
}
