<%@ page language="java" trimDirectiveWhitespaces="true" %>
<%@ include file="settings.jsp"%>

<%
pageContext.setAttribute("title", "<xsl:value-of select=\"$pageTitle\"/>");
pageContext.setAttribute("dcTitle", "{$pageTitle}");
pageContext.setAttribute("showSearch", Boolean.valueOf(request.getParameter("search")));
pageContext.setAttribute("showMap", Boolean.valueOf(request.getParameter("map")));
pageContext.setAttribute("content", "<xsl:call-template name=\"xsltContent\"/>");
String textBefore = "<xsl:text disable-output-escaping=\"yes\"><![CDATA[" + application.getAttribute("bodyBeforeContent") + "]]></xsl:text>";
String textAfter = "<xsl:text disable-output-escaping=\"yes\"><![CDATA[" + application.getAttribute("bodyAfterContent") + "]]></xsl:text>";
pageContext.setAttribute("bodyBeforeContent", textBefore);
pageContext.setAttribute("bodyAfterContent", textAfter);
pageContext.setAttribute("xslt", true);
%>
<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" doctype-system="about:legacy-compat" encoding="utf-8" indent="yes" />
  <xsl:variable name="repositoryName"><%=REPOSITORY_NAME%></xsl:variable>
  <xsl:variable name="repositoryURL"><%=REPOSITORY_URL%></xsl:variable>
  <xsl:variable name="servletURL"><%=SERVLET_URL%></xsl:variable>
  <xsl:variable name="contextURL"><%=CONTEXT_URL%></xsl:variable>
  <xsl:variable name="useSimpleURL" select="<%=USE_SIMPLE_URL?"true()":"false()"%>"/>

  <xsl:template match="/">
    <xsl:variable name="pageTitle"><xsl:call-template name="xsltTitle"/></xsl:variable>
    <%@ include file="template_common.jsp" %>
  </xsl:template>
  
  <xsl:template name="qcIndicator">
    <xsl:param name="timestamp"/>
<% if (TOOLS_QC) { %>
    <xsl:text> </xsl:text>
    <xsl:choose>
      <xsl:when test="normalize-space($timestamp)">
        <span class="flag-qc flag-qc-yes" title="This data package has passed quality control"></span>
      </xsl:when>
      <xsl:otherwise>
        <span class="flag-qc flag-qc-no" title="This data package has been published prior to final review by data custodian and has not yet passed quality control"></span>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
<% } %>    
  </xsl:template>
  
  <xsl:template name="packageURL">
<% if (USE_SIMPLE_URL) { %>
    <xsl:text><%=SERVLET_URL%>/</xsl:text><xsl:value-of select="docid" /><xsl:text>/html</xsl:text>
<% } else { %>    
    <xsl:text><%=SERVLET_URL%>?action=read&qformat=html&docid=</xsl:text><xsl:value-of select="docid" />
<% } %>
  </xsl:template>
</xsl:stylesheet>
