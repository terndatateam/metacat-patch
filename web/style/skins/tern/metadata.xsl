<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="template_xslt.jsp"/>
  <xsl:import href="./eml-2/emlroot.xsl"/>
   
  <xsl:variable name="docTitle">
    <xsl:apply-templates select="/*[local-name()='eml']/*" mode="title"/>
  </xsl:variable>
  
  <xsl:template name="xsltTitle">
    <xsl:value-of select="$docTitle"/>
  </xsl:template>
  
  <xsl:template match="*" mode="title">
  </xsl:template>
  <xsl:template match="dataset | citation | software | protocol" mode="title">
    <xsl:value-of select="title"/>
  </xsl:template>
  
  <xsl:template match="*" mode="citationIdentifier">
  </xsl:template>
  <xsl:template match="dataset | citation | software | protocol" mode="citationIdentifier">
    <h2><xsl:value-of select="$docTitle"/></h2>
    <xsl:call-template name="identifier"/>
  </xsl:template>
  
  <xsl:template name="xsltContent">
    <div id="documentArea" class="metadata-root">
      <xsl:call-template name="shortcutNav"/> 
      <xsl:apply-templates select="*[local-name()='eml']/*" mode="citationIdentifier"/>
      <xsl:call-template name="tabs"/>
      <xsl:call-template name="shortcutNav"/>
    </div>
  </xsl:template>

  <xsl:template name="shortcutNav">
    <!--div class="shortcutNav">
      <div class="pull-left"><a href="javascript:history.back()">Back</a></div>
      <div class="pull-right"><a href="{$contextURL}">New Search</a></div>
      <div style="clear: both;"></div>
    </div-->
  </xsl:template>
  
  <xsl:template name="tabs">
    <div id="tabs" class="tabs">
      <xsl:call-template name="metadataTools"/>
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tabMetadata" data-toggle="tab">Metadata</a></li>
        <xsl:if test="$displaymodule='dataset' or $displaymodule='printall'">
          <li><a href="#tabData" data-toggle="tab">
            <xsl:text>Data </xsl:text>
            <span class="badge" title="Number of available data distributions">
              <xsl:number value="count(//distribution)"/>
            </span>
          </a></li>
        </xsl:if>
      </ul>
      <div class="tab-content">
        <div class="document-root tab-pane active" id="tabMetadata">
          <xsl:apply-templates select="*[local-name()='eml']"/>
        </div>
        <div class="document-root tab-pane" id="tabData"></div>      
      </div>
    </div> <!--tabs-->
  </xsl:template>
  
  <xsl:template name="metadataTools">
    <div class="metadata-tools-container">
      <xsl:if test="not(*[local-name()='eml']/access/allow[principal='public'])">
        <xsl:text> </xsl:text>
        <span class="flag-access-private label label-warning" title="Private Package: This data package is not visible to general public">Private</span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:call-template name="qcIndicator">
        <xsl:with-param name="timestamp" select="*[local-name()='eml']/additionalMetadata/metadata/qualityControl/@timestamp"/>
      </xsl:call-template>
      <xsl:if test="$displaymodule='printall' or $displaymodule='dataset'">
        <a href="{$xmlURI}{$docid}" class="btn btn-default btn-sm btn-download-eml">Download EML</a>
      </xsl:if>
      <div id="metadataTools" class="btn-group">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
          Tools <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
        </ul>
      </div>
    </div>
  </xsl:template>
  
</xsl:stylesheet>
