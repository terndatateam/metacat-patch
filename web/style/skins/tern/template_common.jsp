<%
String title = (String) pageContext.getAttribute("title");
String dcTitle = (String) pageContext.getAttribute("dcTitle");
String description = (String) pageContext.getAttribute("description");
String keywords = (String) pageContext.getAttribute("keywords");
String headTitle = SITE_TITLE + (title == null ? "" : (" - " + title));
Boolean showSearch = (Boolean) pageContext.getAttribute("showSearch");
Boolean showMap = (Boolean) pageContext.getAttribute("showMap");
boolean search = showSearch == null ? true : showSearch.booleanValue();
boolean map = showMap == null ? true : showMap.booleanValue();
Boolean xsltAttr = (Boolean) pageContext.getAttribute("xslt");
boolean xslt = xsltAttr == null ? false : xsltAttr.booleanValue();
%>
<html>
  <head>
    ${head}
    <%--<base href="<%=CONTEXT_URL%>"/>--%>
<% if (dcTitle != null && !dcTitle.isEmpty()) { %>
    <meta name="DC.Title" content="<%=dcTitle%>"/>
<% } %>
<% if (description != null && !description.isEmpty()) { %>
    <meta name="description" content="<%=description%>"/>
<% } %>
<% if (keywords != null && !keywords.isEmpty()) { %>
    <meta name="keywords" content="<%=keywords%>"/>
<% } %>

    <title><%=headTitle%></title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" />
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.js"></script>
    <script type="text/javascript" src="<%=SKIN_URL%>/map.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=SKIN_URL%>/<%=SKIN_NAME%>.css"/>
    <script type="text/javascript" src="<%=SKIN_URL%>/<%=SKIN_NAME%>.js"></script>
<% if (!search && TOOLS_DOI) { %>
    <script type="text/javascript" src="<%=SKIN_URL%>/tools_doi.js"></script>
<% } %>
<% if (!search && TOOLS_QC) { %>
    <script type="text/javascript" src="<%=SKIN_URL%>/tools_qc.js"></script>
<% } %>
  </head>
  ${bodyBeforeContent}
      <div class="login-area">
        <%@ include file="include_login.jsp"%>
      </div>
      <div class="main-area">
        <h1 class="site-title"><a href="<%=CONTEXT_URL%>"><%=SITE_TITLE%></a></h1>
<% if (search) { %>
        <%@ include file="include_searchbox.jsp"%>
<% } %>
<% if (map) { %>
        <%@ include file="include_map.jsp"%>
        <a href="<%=SERVER_URL%>/knb/dataProvider?verb=Identify">OAI-PMH Data Provider</a>
<% } %>
        ${content}
      </div>
      <input type="hidden" id="urlSettings" 
          data-context="<%=CONTEXT_URL%>" data-servlet="<%=SERVLET_URL%>" data-skin="<%=SKIN_URL%>" data-permission="<%=PERMISSION_URL%>" data-licences="<%=LICENCES_URL%>"
          data-request-account="<%=REQUEST_ACCOUNT_URL.replace("&", "&amp;")%>" data-request-access="<%=REQUEST_ACCESS_URL.replace("&", "&amp;")%>"/>
      <input type="hidden" id="miscSettings" data-publisher="<%=REPOSITORY_NAME%>" data-licence="<%=DEFAULT_LICENCE%>"/>
      <%@ include file="include_login_modal.jsp"%>
<% if (!search) { %>
      <%@ include file="include_modal.jsp"%>
<% } %>
  ${bodyAfterContent}
</html>
