<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalTitle" aria-hidden="true">
  <div class="modal-dialog login-box">
    <form class="form-horizontal" role="form" id="loginformmodal" name="loginformmodal" method="post" action="<%=SERVLET_URL%>">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="btn close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
          <h4 class="modal-title" id="loginModalTitle">Login</h4>
        </div>
        <div class="modal-body">
          <p>If you have an account please sign in. Otherwise, please register <a target="_blank" href="<%=REQUEST_ACCOUNT_URL%>">here</a> to request a new account.</p>
          <div class="form-group">
            <label class="col-xs-3 control-label" for="loginModalUsername">Username:</label>
            <div class="col-xs-9">
              <input class="form-control" name="username" id="loginModalUsername" type="text" value=""/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-xs-3 control-label" for="loginModalPassword">Password:</label>
            <div class="col-xs-9">
              <input class="form-control" name="password" id="loginModalPassword" type="password" maxlength="50" value="" />
            </div>
          </div>
          <div class="login-alert alert alert-danger hidden" role="alert">
            <button type="button" class="close"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>  
            <div class="message-denied hidden" >Incorrect username or password. Please try again</div>
            <div class="message-problem hidden" role="alert">ERROR logging in: connection problems. Please try again later</div>
          </div>
          <input type="hidden" name="organization" value="<%=ORGANIZATION%>" />
          <input type="hidden" name="ldapUserPattern" value="<%=LDAP_PATTERN%>" />
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" name="btnlogin" value="Sign in"/>
          <button type="button" class="btn btn-primary login-wait disabled hidden"><img src="<%=SKIN_URL%>/images/spinner.gif" height="16" width="16"/></button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </form>
  </div>
</div>
