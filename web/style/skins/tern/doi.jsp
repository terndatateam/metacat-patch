<%@ page language="java" trimDirectiveWhitespaces="true" contentType="text/xml; charset=UTF-8" import="au.org.tern.doi.DoiService,edu.ucsb.nceas.metacat.PermissionController,edu.ucsb.nceas.metacat.util.SessionData,edu.ucsb.nceas.metacat.util.RequestUtil,java.util.regex.*"%>
<%@ include file="settings.jsp"%>
<%@ include file="session_vars.jsp"%>
<%!
  public boolean isAuthorized(HttpServletRequest request, String docid) {
    SessionData sessionData = RequestUtil.getSessionData(request);
    if (sessionData != null) {
      try {
        PermissionController controller = new PermissionController(docid);
        return controller.hasPermission(sessionData.getUserName(), sessionData.getGroupNames(), "WRITE");
      } catch (Exception ex) {}
    }
    return false;
  }
  
  public DoiService.Metadata createMetadata(String url, String title, String[] creators, String publisher, String year, String size, String format) {
    DoiService.Metadata metadata = new DoiService.Metadata();
    metadata.add(DoiService.Metadata.TARGET, url.trim());
    metadata.add(DoiService.Metadata.TITLE, title.trim());
    for (String creator : creators) {
      metadata.add(DoiService.Metadata.CREATOR, creator.trim());
    }
    metadata.add(DoiService.Metadata.PUBLISHER, publisher);
    metadata.add(DoiService.Metadata.PUBLICATION_YEAR, year.trim());
    metadata.add(DoiService.Metadata.RESOURCE_TYPE, "Dataset/metadata");
    if (size != null) metadata.add(DoiService.Metadata.SIZE, size.trim());
    if (format != null) metadata.add(DoiService.Metadata.FORMAT, format.trim());
    return metadata;
  }
  
  public boolean checkParams(String docid, String title, String[] creators, String year) {
    if (docid == null || docid.length() == 0) return false;
    if (title == null || title.length() == 0) return false;
    if (year == null || year.length() == 0) return false;
    if (creators == null || creators.length == 0 || creators[0] == null || creators[0].length() == 0) return false;
    return true;
  }
  
%>
<%
  if (sessionUsername.isEmpty()) {
    response.sendError(403, "authentication is needed to access this resource" );
    return ;
  }
  
  String action = request.getParameter("action");
  if (action == null) action = "";
  String docid = request.getParameter("docid");
  String docidWithoutRev = docid;
  String[] docidParts = docid.split("\\.");
  System.out.println(docidParts.length);
  if (docidParts.length > 2) docidWithoutRev = docidParts[0] + "." + docidParts[1];
  System.out.println(docidWithoutRev);
  String url = SERVLET_URL + "/" + docidWithoutRev + "/html";
  String title = request.getParameter("title");
  String[] creators = request.getParameterValues("creator");
  if (creators == null) creators = request.getParameterValues("creator[]");
  String publisher = request.getParameter("publisher");
  if (publisher == null) publisher = REPOSITORY_NAME;
  String year = request.getParameter("year");
  String size = request.getParameter("size");
  String format = request.getParameter("format");
  
  if (action.equals("query")) {
    if (docid == null || docid.length() == 0) {
      response.sendError(400, "docid parameter is required" );
      return;
    }
    String metadata = "";
    try {
      DoiService ds = new DoiService();
      metadata = ds.query(url);
    } catch (Exception ex) {
      response.sendError(503, ex.toString());
      return;
    }
    //System.out.println(metadata);
    out.println(metadata);
    
  } else if (action.equals("auth")) {
    if (docid == null || docid.length() == 0) {
      response.sendError(400, "docid parameter is required" );
      return;
    }
    if (isAuthorized(request, docid)) {
      out.println("<?xml version=\"1.0\"?><auth>ok</auth>");
      return;
    } else {
      response.sendError(403, "permission denied" );
      return;
    }
    
  } else if (action.equals("create")) {
    if (checkParams(docid, title, creators, year)) {
      if (isAuthorized(request, docid)) {
        try {
          DoiService ds = new DoiService();
          String metadata = ds.query(url);
          if (metadata.indexOf("identifier") > 0) {
            response.sendError(400, "DOI exists for the given docid" );
            return;
          } else {
            String doi = ds.mint(createMetadata(url, title, creators, publisher, year, size, format));
            out.println("<?xml version=\"1.0\"?><doi>" + doi + "</doi>");
            return;
          }
        } catch (Exception ex) {
          response.sendError(503, ex.toString());
          return;
        }
      } else {
        response.sendError(403, "permission denied" );
        return;
      }
    } else {
      response.sendError(400, "docid, url, title, creator, and year parameters are required" );
      return;
    }
    
  } else if (action.equals("update")) {
    if (checkParams(docid, title, creators, year)) {
      if (isAuthorized(request, docid)) {
        try {
          DoiService ds = new DoiService();
          String doi;
          String metadata = ds.query(url);
          Pattern p = Pattern.compile("<identifier.*?>(.*)</identifier");
          Matcher m = p.matcher(metadata);
          if (m.find() && m.groupCount() > 0) {
            doi = m.group(1).trim();
            System.out.println(url);
            ds.update(doi, createMetadata(url, title, creators, publisher, year, size, format));
            out.println("<?xml version=\"1.0\"?><doi>" + doi + "</doi>");
            return;
          } else {
            response.sendError(400, "DOI does not exist for the given docid" );
            return;
          }
        } catch (Exception ex) {
          response.sendError(503, ex.toString());
          return;
        }
      } else {
        response.sendError(403, "permission denied" );
        return;
      }
    } else {
      response.sendError(400, "docid, url, title, creator, and year parameters are required" );
      return;
    }
  
  } else {
    response.sendError(400, "unknown action parameter" );
    return ;
  }
%>
