<%@ page language="java" %>
<%@ include file="settings.jsp"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Metacat Repository</title>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=SKIN_URL%>/layout.css"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container container-wrapper">
      <div class="header">
        <a id="mainLogo" href="<%=CONTEXT_URL%>">KNB Metacat</a>
      </div>
      <div id="metacatContainer" class="main-inside">
      </div>
      <div class="footer">
      </div>
    </div>  
  </body>
</html>
