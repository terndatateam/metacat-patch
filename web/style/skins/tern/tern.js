/* jQuery plugins */
(function($) {
  $.fn.linkify = function() {
    return this.each(function() {
      var el = $(this);
      if (!el.html()) return;  // bail early if no data
      var inputText = el.html();

      //URLs starting with http://, https://, or ftp://
      var replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
      var replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

      //URLs starting with www. (without // before it, or it'd re-link the ones done above)
      var replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
      replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

      //Change email addresses to mailto:: links
      var replacePattern3 = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gim;
      replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$&">$&</a>');

      el.html(replacedText);
    });
  };
  
  var collapsibleUpdateDom = function(shownButton, hiddenButton) {
    if (hiddenButton) hiddenButton.addClass('hidden');
    if (shownButton) shownButton.removeClass('hidden');
  };
  var collapsibleAction = {};
  collapsibleAction.expand = function($this) {
    if (!$this.hasClass('collapsible-collapsed')) return;
    $this.removeClass('collapsible-collapsed');
    collapsibleUpdateDom($this.data('collapseButton'), $this.data('expandButton'));
    $this.height('auto');
    $this.data('indicator').addClass('hidden');
  };
  collapsibleAction.collapse = function($this) {
    if ($this.hasClass('collapsible-collapsed')) return;
    $this.addClass('collapsible-collapsed');
    collapsibleUpdateDom($this.data('expandButton'), $this.data('collapseButton'));
    $this.height($this.attr('data-collapsible-height'));
    $this.data('indicator').removeClass('hidden');
  };
  collapsibleAction.toggle = function($this) {
    var ops = ['collapse', 'expand'];
    collapsibleAction[ops[~~$this.hasClass('collapsible-collapsed')]]($this);
  };
  var collapsibleParams = {
    expand : {
      buttonName : 'expandButton',
      buttonClassName : 'collapsible-expand-button',
      iconClassName : 'fa-angle-double-down',
      handler : function(event) {
        event.preventDefault();
        $(this).data('collapsible').collapsible('expand');
      }
    },
    collapse : {
      buttonName : 'collapseButton',
      buttonClassName : 'collapsible-collapse-button',
      iconClassName : 'fa-angle-double-up',
      handler : function(event) {
        event.preventDefault();
        $(this).data('collapsible').collapsible('collapse');
      }
    }
  };
  $.fn.collapsible = function(action, options) {
    if ('object' === typeof action) {
      options = action;
      action = '';
    }
    options = options || {};
    action = action || 'collapse';
    return this.each(function() {
      var $this = $(this);
      var collapsedHeight = $this.attr('data-collapsible-height');
      if (collapsedHeight) collapsedHeight = parseInt(collapsedHeight);
      if (!collapsedHeight) {
        var linesNum = parseInt($this.attr('data-collapsible-lines')) || 1;
        var $clone = $this.clone().html('');
        var lines = 'line';
        for (var i=1; i<linesNum; ++i) lines += '<br>line';
        $clone.html(lines).css('visibility', 'hidden');
        $this.after($clone);
        collapsedHeight = $clone.height();
        $clone.remove();
        if ($this.height() > collapsedHeight) $this.attr('data-collapsible-height', collapsedHeight);
        else return;
      }
      if (!$this.data('indicator')) {
        var autoCreateButtons = options.autoCreateButtons;
        if (autoCreateButtons == null) autoCreateButtons = true;
        var $indicator = options.indicator || $this.find('.collapsible-indicator');
        if (!($indicator && $indicator.length > 0)) {
          $indicator = $('<span class="collapsible-indicator hidden">...</span>').appendTo($this);
        }
        $this.data('indicator', $indicator);
        $.each(['expand', 'collapse'], function(index, item){
          var param = collapsibleParams[item];
          var $button = $this.data(param.buttonName);
          if (!$button || $button.length === 0) {
            $button = options[param.buttonName] || $this.find('.' + param.buttonClassName);
            if (autoCreateButtons && !($button && $button.length > 0)) {
              $button = $('<a href="#" class="' + param.buttonClassName + ' hidden"><i class="fa ' + param.iconClassName + '"></i></a>').appendTo($this);
            }
            if ($button && $button.length) {
              $button.on('click', param.handler);
              $button.data('collapsible', $this);
              $this.data(param.buttonName, $button);
            }
          }
        });
      }
      collapsibleAction[action]($this);
    });
  };
  
  var toggleCollapsibleFieldGroup = function() {
    var $this = $(this);
    $this.attr('title', $this.attr('data-title-' + $this.data('state')));
    if ($this.data('state') == 'hide') $this.data('state', 'show'); else $this.data('state', 'hide');
    var parent = $this.closest('.field-group');
    parent.children('.field-group-body').toggleClass('hidden');
    parent.children('.field-group-heading').find('.field-group-collapsible-icon').toggleClass('hidden');
  };
  
  $.fn.collapsibleFieldGroup = function() {
    return this.each(function() {
      var $this = $(this);
      var expandIcon = $('<i class="fa fa-plus-square field-group-collapsible-icon"></i>');
      var collapseIcon = $('<i class="fa fa-minus-square field-group-collapsible-icon"></i>');
      collapseIcon.addClass('hidden');
      $this.children('.field-group-body').addClass('hidden');
      var toggle = $this.children('.field-group-heading').find('.field-group-toggle');
      if (!toggle.length) toggle = $this.children('.field-group-heading');
      toggle.
        on('click', toggleCollapsibleFieldGroup).
        prepend(expandIcon, collapseIcon).
        data('state', 'hide').
        attr('title', toggle.attr('data-title-show'));
    });
  };
  
  var infoModalToggle = function(event){
    event.preventDefault();
    var $infoModal = $(this).closest('.info-modal');
    var title = $infoModal.find('.info-modal-title').first().html() || $(this).text() || '';
    var body = $infoModal.find('.info-modal-body').first().html() || '';
    $('#infoModalTitle').html(title);
    $('#infoModalBody').html(body);
    $('#infoModal').modal('show');
  };
  
  $.fn.infoModal = function() {
    return this.each(function() {
      var $this = $(this);
      $this.find('.info-modal-title').addClass('hidden');
      $this.find('.info-modal-body').addClass('hidden');
      $this.find('.info-modal-toggle').click(infoModalToggle);
    });
  };

})(jQuery);

/* Globals */
var metacat = { url : {}, tools : [], eml : {} };

(function($) {

metacat.eml.get = function(cb) {
  var emlUrl = metacat.url.servlet + '/' + metacat.docid + '/xml';
  $.get(emlUrl).always(function(res, status, obj){
    var eml, err;
    if (status == 'success') {
      eml = res;
    } else {
      err = new Error('Error getting ' + emlUrl);
    }
    return cb(err, eml);
  });
};

metacat.eml.upload = function(docid, xml, cb) {
  var reqBody = {};
  reqBody.action = 'update';
  reqBody.docid = docid;
  reqBody.doctext = (new XMLSerializer()).serializeToString(xml);
  $.post(metacat.url.servlet, reqBody).always(function(res, status, obj){
    var rdocid, err;
    if (status == 'success') {
      try {
        var docidElement = res.getElementsByTagName('docid')[0];
        rdocid = docidElement.textContent || docidElement.text;
      } catch (ex) {
        err = ex;
        try {
          var errElement = res.getElementsByTagName('error')[0];
          err = new Error(errElement.textContent || errElement.text);
        } catch (ex2) {
        }
      }
    } else {
      err = new Error(obj);
    }
    return cb(err, $.trim(rdocid));
  });
};

metacat.isLastRevision = function(cb) {
  if (metacat._isLastRevision == null) {
    $.post(metacat.url.servlet, {action:'getrevisionanddoctype', docid:metacat.docid}, 'text').
      done(function(resBody){
        var lastRev = resBody.split(';')[0];
        if (!metacat.revid) {
          metacat.revid = lastRev;
          metacat.docid = metacat.docidNoRev + '.' + lastRev;
        }
        metacat._isLastRevision = (metacat.revid == lastRev);
        return cb(metacat._isLastRevision);
      }).fail(function(){ cb(false); } );
  } else {
    return cb(metacat._isLastRevision);
  }
};

metacat.isPublic = function(cb) {
  if (metacat._isPublic == null) {
    $.ajax({type:"HEAD", url:metacat.url.servlet + '?action=read&sessionid=0&docid=' + metacat.docid}).
      done(function(body, textStatus, jqXHR){
        metacat._isPublic = (jqXHR.getResponseHeader('Content-Disposition') != null);
        return cb(metacat._isPublic);
      }).fail(function(){ cb(false); } );
  } else {
    return cb(metacat._isPublic);
  }
};

metacat.hasWriteAccess = function(cb) {
  if (metacat._hasWriteAccess == null) {
    //console.log(metacat.docid);
    $.ajax({type:'POST', url:metacat.url.permission, data:{permission:'write', docid:[metacat.docid]}}).
      done(function(permissions) {
        metacat._hasWriteAccess = (metacat.docid in permissions);
        return cb(metacat._hasWriteAccess);
      }).fail(function(){ cb(false); } );
  } else {
    return cb(metacat._hasWriteAccess);
  }
};

metacat.increaseRevId = function(packageId) {
  var docidParts = packageId.split('.');
  var lastIdx = docidParts.length - 1;
  docidParts[lastIdx] = parseInt(docidParts[lastIdx]) + 1;
  return docidParts.join('.');
};

metacat.logout = function() {
  metacat._hasWriteAccess = null;
};


function humanReadableFileSize(bytes) {
  var exp = Math.log(bytes) / Math.log(1024) | 0;
  var result = (bytes / Math.pow(1024, exp)).toFixed(2);
  return result + ' ' + (exp == 0 ? 'bytes': 'KMGTPEZY'[exp - 1] + 'iB');
}

function validateSearchForm() {
  var formObj = this;
  var searchBox = $("#searchBox");
  var searchString = $.trim(searchBox.val());
  if (searchString == "") {
    if (confirm("This search will show ALL available data in portal and may take some time.\n Continue?")) {
      searchString = "%";
    } else {
      return false;
    }
  }
  searchBox.val(searchString);

  var searchRadio = $("input:radio[name=search]:checked");
  var searchType = $("#searchType");
  var fieldValue;
  if(searchRadio.val() == "quick" && searchString != "%") {
    searchType.attr('name', 'title');
    formObj.operator.value="UNION";
    fieldValue = searchString;
  } else {
    searchType.attr('name', 'anyfield');
    formObj.operator.value="INTERSECT";
    fieldValue = "";
  }
  searchType.val(searchString);
  formObj.surName.value = fieldValue;
  formObj.givenName.value = fieldValue;
  formObj.keyword.value = fieldValue;
  formObj.organizationName.value = fieldValue;
  formObj.para.value = fieldValue;
  formObj.geographicDescription.value = fieldValue;
  formObj.literalLayout.value = fieldValue;
  $("#abstract").val(fieldValue);
  
  var search = $("input[name=search]");
  search.attr("name","");
  searchBox.attr("name","");
  
  return true;
}

function validateLoginForm() {
  var formObj = this;
  var AUTH_URL = formObj.action || '';
  var popupMsg = 'Please contact us if you need to create a new account.';
  var messages = {
    username : "You must type a username. \n" + popupMsg,
    organization : "You must select an organization.\n" + popupMsg,
    password : "You must type a password. \n" + popupMsg
  };
  var data = {};
  var username;
  var $btnAction;
  var $loginBox = $(formObj).closest('.login-box');
  if (!$(formObj.btnlogin).hasClass('hidden')) {
    //trim username & password
    data = { username:'', organization:'', password:'' };
    for (var key in data) {
      data[key] = $.trim(formObj[key].value);
      if (data[key].length == 0) {
        alert(messages[key]);
        formObj[key].focus();
        return false;
      } 
    }
    data.action = 'login';
    username = data.username;
    data.username = formObj.ldapUserPattern.value.
        replace('username', data.username).
        replace('organization', data.organization);
    delete data.organization;
    $btnAction = $(formObj.btnlogin);
  } else {
    data.action = 'logout';
    $btnAction = $(formObj.btnlogout);
  }
  $btnAction.addClass('hidden');
  $('.login-wait', $loginBox).removeClass('hidden');
  $('.login-alert', $loginBox).addClass('hidden').fadeTo(1, 0).slideUp(1);
  $.ajax({type:"POST", url:AUTH_URL, cache: false, data: data}).done(function(data){
    $('.login-wait', $loginBox).addClass('hidden');
    $btnAction.removeClass('hidden');
    if (data.getElementsByTagName('login').length) return refreshSession(username);
    if (data.getElementsByTagName('logout').length) {
      if ($('#searchResultArea').length) {
        window.location.href = "/knb";
        return;
      } else {
        return refreshSession();
      }
    }
    if (data.getElementsByTagName('unauth_login').length) showLoginAlert($loginBox, 'denied');
    else showLoginAlert($loginBox, 'problem');
  });
  return false;
}

function showLoginAlert(context, type) {
  $('.login-alert', context).removeClass('hidden');
  $('.login-alert .message-'+type, context).removeClass('hidden');
  $('.login-alert', context).fadeTo(200,1).slideDown(200, function(){
    setTimeout(function(){ hideLoginAlert.call(context); }, 3000);
  });
}

function hideLoginAlert() {
  var $e = $('.login-alert', this);
  if (!$e.hasClass('hidden')) {
    $e.fadeTo(200, 0).slideUp(200).addClass('hidden');
  }
}

function refreshSession(username) {
  metacat.logout();
  var isLoggedIn = !!username;
  $('#loginModal').modal('hide');
  var loginform = document.forms["loginform"];
  var loginformmodal = document.forms["loginformmodal"];
  loginform.username.value = loginformmodal.username.value = '';
  loginform.password.value = loginformmodal.password.value = '';
  loginform.username.disabled = isLoggedIn;
  loginform.password.disabled = isLoggedIn;
  if (isLoggedIn) {
    $('.message-login-username').text(username);
    var matches = document.cookie.match(/JSESSIONID=(.*?);/);
    var sessionId = matches ? matches[1] : '';
    $('input[name="sessionid"]').val(sessionId);
  }
  
  var m = ['addClass', 'removeClass'];
  var m1 = m[~~isLoggedIn];
  var m2 = m[~~(!isLoggedIn)];
  $('.login-status .message-login')[m1]('hidden');
  $('.login-status .message-logout')[m2]('hidden');
  $(loginform.btnlogout)[m1]('hidden');
  $(loginform.btnlogin)[m2]('hidden');
  $(".request-account")[m2]('hidden');
  $('.form-group', loginform)[m2]('hidden');
  $(".login-btn-modal")[m2]('hidden');
  
  metacat.username = username;
  //console.log('username: ' + username);
  var ga_username = username || '';
  ga('set', 'dimension1', ga_username);
  
  // Setup metadata page
  if ($('#documentArea').length) {
    updateDataDownloadLinks(username);
    refreshTools();
  }
  //$(document).trigger(state);
}

function seriesEach(container, onEachItem, onFinish) {
  var i = 0;
  if (!$.isArray(container) || container.length === 0) return;
  (function next(err) {
    if (err) return onFinish(err);
    if (i < container.length) {
      var item = container[i++];
      if (item != null) onEachItem(item, next, onFinish);
      else next();
    } else {
      onFinish();
    }
  })();
}

// Setup tools menu
function refreshTools() {
  var hasAccess = function(fn, cbYes, cbNo) {
    if (typeof fn === 'function') {
      fn(function(ok){ if (ok) cbYes(); else cbNo(); });
    } else {
      cbYes();
    }
  };
  var $menu = $('#metadataTools>ul.dropdown-menu');
  var $button = $('#metadataTools button');
  $button.addClass('hidden');
  $menu.empty();
  seriesEach(metacat.tools, function(toolset, next){
    hasAccess(toolset.hasAccess, function(){
      var $divider, $header;
      if ($menu.children().length) $divider = $('<li role="presentation" class="divider hidden"></li>').appendTo($menu);
      if (toolset.label) $header = $('<li role="presentation" class="dropdown-header hidden">' + toolset.label + '</li>').appendTo($menu);
      seriesEach(toolset.items, function(item, next2){
        hasAccess(item.hasAccess, function(){
          // create dropdown buttons
          $('<li><a href="#">' + item.label + '</a></li>').click(item.handler).appendTo($menu);
          if ($divider) $divider.removeClass('hidden');
          if ($header) $header.removeClass('hidden');
          next2();
        }, next2);
      }, next);
    }, next);
  }, function(){
    if ($menu.children().length) $button.removeClass('hidden');
  });
}

var requestAccessLinkTooltip = 'Access to this data is restricted. You must be logged in and have been granted access privileges to download this data. If you do not have appropriate access privileges you may submit a request for access';

/* Provide a validated download link for each data file. This function checks access to a file with the current session using permission.jsp. */
function updateDataDownloadLinks(username) {
  var $downloadLinks = $('.download-link-local');
  var docids = $downloadLinks.map(function(){
    return $.trim($(this).text());
  }).get();
  if (!docids || docids.length === 0) return;
  $.ajax({type:'POST', url:metacat.url.permission, data:{permission:'read', docid:docids}}).done(function(permissions) {
    $downloadLinks.each(function(){
      var $this = $(this);
      var datadocid = $.trim($(this).text());
      $this.siblings().remove('.entity-data-actions');
      var $container = $('<span class="entity-data-actions"></span>').insertAfter($this);
      var filename = $.trim($this.closest('.entity').find('.entityName>dd').text());
      if (datadocid in permissions) {
        // current user has download access
        var filesize = permissions[datadocid];
        $('<a href="' + $this.attr('href') + '"><span class="glyphicon glyphicon-download"></span> Download</a>')
        .click(showLicence)
        .data('filename', filename)
        .data('filesize', parseInt(filesize))
        .appendTo($container);
        $('<span class="entity-data-size"> (' + humanReadableFileSize(filesize) + ')</span>').appendTo($container);
      } else { // no access
        if (username) {
          // no download access, user is logged-in, request restricted access
          var reqUrl = metacat.url.requestAccess.
                         replace('$username', encodeURIComponent(username)).
                         replace('$docid', encodeURIComponent(metacat.docid)).
                         replace('$filename', encodeURIComponent(filename));
          $('<a href="'+reqUrl+'" target="_blank">Request Access</a>').appendTo($container);
        } else {
          // no download access, user is not logged-in, ask user to log in or register
          var loginLink = $('<a href="#">Login</a>').click(showLogin);
          var registerLink = $('<a href="' + metacat.url.requestAccount + '" target="_blank">Register</a>');
          $container.append(loginLink).append(' or ').append(registerLink);
        }
        $container.append(' to Download').attr('title', requestAccessLinkTooltip);
      }
    });
    
    $('.datasetEntity .entity').each(function(){
      var $entity = $(this);
      var $heading = $entity.closest('.field-group').find('.field-group-heading:first');
      $heading.find('.entity-data-actions').remove();
      $entity.find('.entity-data-actions').first().clone(true, true).appendTo($heading);
    });
  });
  
}

function showLogin(event) {
  event.preventDefault();
  $('#loginModal').modal('show');
}

function showLicence(event) {
  event.preventDefault();
  var $this = $(this);
  $('#downloadModalFileName').text($this.data('filename'));
  $('#downloadModal .download-check').prop('checked', false);
  $('#downloadModal .alert').removeClass('hidden');
  $('#downloadButton').attr('href', $this.attr('href'));
  if ($('#downloadModal .download-check').length === 0) {
    $('#downloadButton').removeClass('hidden');
  }
  $('#downloadButton').data('filename', $this.data('filename'));
  $('#downloadButton').data('filesize', $this.data('filesize'));
  $('#downloadModal').modal('show');
}

function downloadData() {
}

function toggleDownloadButton() {
  var allChecked = 1;
  var alertChecks = {};
  $('#downloadModal .download-check').each(function(){
    allChecked &= this.checked;
    var name = this.getAttribute('data-alert-class');
    if (alertChecks[name] == null) alertChecks[name] = 1;
    alertChecks[name] &= this.checked;
  });
  var m = ['addClass', 'removeClass'];
  for (var name in alertChecks) {
    $('#downloadModal .'+name)[m[alertChecks[name]^1]]('hidden');
  }
  $('#downloadButton')[m[allChecked]]('hidden');
}

function formatLicence() {
  var container = $('.intellectualRights dd');
  var element = container.find('p, div').first();
  if (element.length === 0) {
    var ctext = container.text().trim();
    if (ctext.length === 0) {
      $('#downloadModal .modal-body h4').remove();
      $('#downloadModalLicence').remove();
      $('#downloadModal .checkbox.check-licence').remove();
      $('#downloadModal .checkbox.check-special').remove();
      $('#downloadModal .alert-check-licence').remove();
      return;
    }
    element = $('<p>' + ctext + '</p>');
    container.empty();
    container.append(element);
  }
  var text = $.trim(element.html());
  // handle special conditions
  var startSpecial = text.search(/^\s*special\s+condition/im);
  var specialText;
  if (startSpecial >= 0) {
    specialText = $.trim(text.slice(startSpecial));
    text = $.trim(text.slice(0, startSpecial));
    element.html(text.replace(/\n/g, '<br/>'));
    var specialTextStart = specialText.indexOf('\n');
    if (specialTextStart > 0) specialText = $.trim(specialText.slice(specialTextStart+1));
  }
  if (specialText) {
    $('<p>' + specialText.replace(/\n/g, '<br/>') + '</p>').insertAfter(element);
    element.after('<h5>Special Conditions</h5>');
  } else {
    $('#downloadModal .checkbox.check-special').remove();
  }
  // handle built in licences
  var licence = text || metacat.defaultLicence;
  var endDefLicence = text.indexOf('\n');
  if (endDefLicence > 0) {
    licence = $.trim(text.slice(0, endDefLicence));
  }
  $.ajax({type:'GET', url: metacat.url.licences + '/index.json'}).done(function(licenceMap){
    var licenceCode, licenceText, key, pattern;
    if (licence.length < 30 && licenceMap[licence.toLowerCase()]) {
      licenceCode = licence.toLowerCase();
    } else {
      for (key in licenceMap) {
        pattern = licenceMap[key];
        if (text.indexOf(pattern) >= 0) {
          licenceCode = key;
          licenceText = text;
          break;
        }
      }
    }
    if (licenceCode) {
      var licenceBaseUrl = metacat.url.licences + '/' + encodeURIComponent(licenceCode);
      $.ajax({type:'GET', url: licenceBaseUrl + '.txt'}).done(function(body){
        var lines = body.split('\n');
        var licenceUrl = lines[0];
        var licenceTitle = lines[1];
        if (!licenceText) {
          licenceText = lines[2];
          if (endDefLicence > 0) {
            text = $.trim(text.slice(endDefLicence));
            text = text.replace(/\n/g, '<br/>');
            element.html(text);
          } else {
            element.remove();
          }
          $('<p>' + licenceText.replace(/\n/g, '<br/>') + '</p>').prependTo(container);
        }
        $('<h5><a href="' + licenceUrl + '" target="_blank">' + licenceTitle + '</a></h5>').prependTo(container);
        $('<img src="' + licenceBaseUrl + '.png" alt="licence logo" class="licence-logo"/>').prependTo(container);
        $('#downloadModalLicence').html($('.field.intellectualRights dd').html());
      });
    }
  });
  $('dl.intellectualRights dd p').linkify();
  $('#downloadModalLicence').html($('.field.intellectualRights dd').html());
}


// On document ready
$(function() {
  // check for google analytics
  if (typeof ga !== 'function') ga = function(){};
  
  var $urlSettings = $('#urlSettings');
  metacat.url.context = $urlSettings.attr('data-context');
  metacat.url.servlet = $urlSettings.attr('data-servlet');
  metacat.url.authinfo = metacat.url.servlet + '?action=getloggedinuserinfo';
  metacat.url.skin = $urlSettings.attr('data-skin');
  metacat.url.permission = $urlSettings.attr('data-permission');
  metacat.url.requestAccount = $urlSettings.attr('data-request-account');
  metacat.url.requestAccess = $urlSettings.attr('data-request-access');
  metacat.url.requestAccess = metacat.url.requestAccess + (metacat.url.requestAccess.indexOf('?') < 0 ? '?' : '&');
  metacat.url.licences = $urlSettings.attr('data-licences');
  var $miscSettings = $('#miscSettings');
  metacat.defaultPublisher = $('#miscSettings').attr('data-publisher');
  metacat.defaultLicence = $('#miscSettings').attr('data-licence');
    
  // Setup login area
  $('.login-area .login-btn-modal').on('click', showLogin);
  $('.login-alert .close').on('click', hideLoginAlert);
  $('#loginform').on('submit', validateLoginForm);
  $('#loginformmodal').on('submit', validateLoginForm);

  // Setup search area
  $('#searchForm').on('submit', validateSearchForm);

  // Setup metadata page
  if ($('#documentArea').length) {
    metacat.docid = metacat.docidNoRev = $.trim($('#docid').text());
    metacat.revid = '';
    var lastDotIndex = metacat.docid.lastIndexOf('.');
    if (lastDotIndex > 0) {
      metacat.docidNoRev = metacat.docid.slice(0, lastDotIndex);
      metacat.revid = metacat.docid.slice(lastDotIndex+1);
    }

    // Adjust layout
    $('.intellectualRights').appendTo('#tabData');
    $('.emlDownload').appendTo('#tabData');
    $('.dataset>.distribution').appendTo('#tabData');
    $('.datasetEntity').appendTo('#tabData');
    
    // Add accessed on to Citation
    var cd = new Date();
    var fd = cd.toLocaleDateString(); //cd.getDate() + ' ' + cd.getMonth() + cd.getFullYear();
    var $citation = $('.resourceCitation dd p')
    $citation.html($.trim($citation.html()) + '. Accessed on ' + fd + '.');
    
    // Fetch last created date to be used as publication date
    var pubDate = $.trim($('#pubDate').text());
    if (pubDate.length === 0) {
      $.ajax({type:"GET", url: metacat.url.servlet + '?action=query&qformat=xml&%40packageId=' + metacat.docidNoRev, cache: false}).done(function(data){
        try {
          var node = data.getElementsByTagName('createdate')[0];
          var text = node.textContent || node.text;
          var date = text.slice(0,4);
          $('#pubDate').text(date);
        } catch (ex) {}
      });
    }
    
    // init download modal
    $('#downloadModal .download-check').on('change', toggleDownloadButton);
    $('#downloadButton').click(function() {
      ga('set', 'dimension2', metacat.docid);
      ga('send', 'event', 'Data', 'Download', $(this).data('filename'), $(this).data('filesize'));
      $('#downloadModal').modal('hide'); 
    });
    formatLicence();
    
    // misc styling
    $('dl.abstract dd p').linkify().addClass('collapsible').attr('data-collapsible-lines', '10');
    $('#documentArea .field-group-collapsible').collapsibleFieldGroup();
   
    // geographic coverage map
    $('.geographicCoverage-map').each(function(){
      var $gcmap = $(this);
      var bounds = [[$gcmap.attr('data-south'), $gcmap.attr('data-west')], [$gcmap.attr('data-north'), $gcmap.attr('data-east')]];
      initSingleMap(this, bounds);
    });
    $('.collapsible').collapsible();
  }
  
  // Setup search result page
  var $searchResultArea = $('#searchResultArea');
  if ($searchResultArea.length) {
    var rowExpandHandler = function(event){
      event.preventDefault();
      var $row = $(this).closest('tr');
      $row.find('.collapsible').collapsible('expand');
      $row.find('.collapsible-row-collapse-button').removeClass('hidden');
      $(this).addClass('hidden');
    };
    var rowCollapseHandler = function(event){
      event.preventDefault();
      var $row = $(this).closest('tr');
      $row.find('.collapsible').collapsible('collapse');
      $row.find('.collapsible-row-expand-button').removeClass('hidden');
      $(this).addClass('hidden');
    };
    $('.results-table .table-row').each(function(){
      var $this = $(this);
      var expandButton = $this.find('.collapsible-row-expand-button');
      var collapseButton = $this.find('.collapsible-row-collapse-button');
      collapseButton.addClass('hidden').on('click', rowCollapseHandler);
      expandButton.removeClass('hidden').on('click', rowExpandHandler);
      $this.find('.collapsible').collapsible({autoCreateButtons:false});
      //console.log(this);
      //linkify()
    });
    $('#resultsExpandAll').on('click', function(){
      $('.results-table .table-row').each(function(){
        $(this).find('.collapsible-row-expand-button').trigger('click');
      });
    });
    $('#resultsCollapseAll').on('click', function(){
      $('.results-table .table-row').each(function(){
        $(this).find('.collapsible-row-collapse-button').trigger('click');
      });
    });
    
  }
  
  if ($('#mapArea').length) {
    initMainMap($('#map')[0], $('#mapForm')[0]);
  }

  // Setup interactive component on metadata and search result page
  $('.info-modal').infoModal();
  
  // fetch session (login state) info from server
  setTimeout(function() {
    //console.log('check login info');
    $.ajax({type:"GET", url: metacat.url.authinfo, cache: false}).done(function(data){
      var usernameNode = data.getElementsByTagName('username')[0];
      var username = $.trim(usernameNode.textContent || usernameNode.text);
      if (username && username != 'public') {
        username = username.slice(username.indexOf('=') + 1, username.indexOf(','));
        refreshSession(username);
      } else {
        refreshSession();
      }
    });
  }, 1);  
  
});

})(jQuery);
