<%@ page import="java.util.*" %>
<% 
 /**
  *  '$RCSfile$'
  * Authors: Alvin Sebastian
  * Copyright: 2012 Queensland University of Technology
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  */

  //////////////////////////////////////////////////////////////////////////////
  //
  // NOTE: GLOBAL CONSTANTS (SETTINGS SUCH AS METACAT URL, LDAP DOMAIN AND DEBUG 
  //       SWITCH) ARE ALL IN THE INCLUDE FILE "settings.jsp"
  //
  //////////////////////////////////////////////////////////////////////////////

  // GLOBAL VARIABLES //////////////////////////////////////////////////////////
  String sessionId = "";
  String sessionLdapUsername = "";
  String sessionUsername = "";
  String sessionPassword = "";
  HttpSession rfSession;
  //////////////////////////////////////////////////////////////////////////////	
  try {
    rfSession = request.getSession(true);
    sessionId = rfSession.getId();
    Object attr = rfSession.getAttribute("username");
    if (attr != null) sessionLdapUsername = (String) attr;
    attr = rfSession.getAttribute("password");
    if (attr != null) sessionPassword = (String) attr;
    int start = sessionLdapUsername.indexOf("=") + 1;
    int end = sessionLdapUsername.indexOf(",");
    if (end > 0 && start > 0) sessionUsername = sessionLdapUsername.substring(start, end).trim();
  } catch(Exception e) {
    throw new ServletException("trying to get session: "+e);
  }

  if (DEBUG_TO_BROWSER) {
%>
<table>
  <tr> 
    <td colspan="4" align="left" valign="top" class="text_plain" bgcolor="#ffff00">
      <ul><li>sessionId:&nbsp;<%=sessionId%></li></ul>
    </td>
  </tr>
</table>
<%
  }
%>