<%@ page language="java" trimDirectiveWhitespaces="true" %>
<div id="searchArea">
  <h2>Search by Keyword</h2>
  <p> 
    Logging into your account enables you to search any additional, non-public data for which you may have access privileges.
  </p>
  <p>
    Enter a search phrase (e.g. biodiversity) to search for data packages in the data portal. The search phrase can contain an optional percentage (%) as wild card (e.g. biodiversity % rainforest).
  </p>
  <form id="searchForm" name="searchForm" method="post" action="<%=SERVLET_URL%>">
    <input type="hidden" id="searchFormSessionId" name="sessionid" value="" />
    <%@ include file="include_searchpostfields.jsp" %>
    <input type="hidden" name="organizationName"/>
    <input type="hidden" name="surName"/>
    <input type="hidden" name="givenName"/>
    <input type="hidden" name="keyword"/>
    <input type="hidden" name="para"/>
    <input type="hidden" name="geographicDescription"/>
    <input type="hidden" name="literalLayout"/>
    <input type="hidden" name="abstract/para" id="abstract"/>
    <input type="hidden" name="" id="searchType" />
    <!--input type="hidden" name="pagestart" value="0" /-->
    <!--input type="hidden" name="pagesize" value="5" /-->
    <div class="input-group">
      <input class="form-control" type="text" name="searchBox" size="30" maxlength="200" id="searchBox" value=""/>
      <span class="input-group-btn">
        <button id="searchFormButton" class="btn btn-primary" type="submit" value="Search">
          <span class="glyphicon glyphicon-search"></span>
          <span class="search-btn-label"> Search </span>
        </button>
      </span>
    </div>
    <div <%=SEARCH_OPT_CLASS%>>
      <label class="search-option"><input name="search" type="radio" value="quick" <%=SEARCH_OPT_QUICK%> /> Search Title, Abstract, Keywords, Personnel (quicker)</label>
      <label class="search-option"><input name="search" type="radio" value="all" <%=SEARCH_OPT_ALL%> /> Search all Fields (slower)</label>
    </div>
  </form>
  <!--<div><a href="javascript_required.html" onclick="document.searchForm.submit(); return false;">Browse ALL datasets available</a></div>-->
</div> <!--searchArea-->
