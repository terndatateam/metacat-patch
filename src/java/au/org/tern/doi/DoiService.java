/**
 *     Authors: Alvin Sebastian
 *   Copyright: 2014 James Cook University and the Terrestrial Ecosystem Research Network
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package au.org.tern.doi;

import edu.ucsb.nceas.metacat.properties.PropertyService;
import edu.ucsb.nceas.utilities.PropertyNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

/**
 *
 * This class provides access to the TERN DOI Service.
 * @author Alvin Sebastian
 */
public class DoiService {
    
    public static class Metadata {
        public static final String CREATOR = "creator";
        public static final String TITLE = "title";
        public static final String PUBLISHER = "publisher";
        public static final String PUBLICATION_YEAR = "publicationYear";
        public static final String RESOURCE_TYPE = "resourceType";
        public static final String FORMAT = "format";
        public static final String SIZE = "size";
        public static final String TARGET = "target";
        
        private final HashMap<String, List<String>> map;
        
        public Metadata() {
            this.map = new HashMap<String, List<String>>();
        }
        public void add(String key, String value) {
            List<String> list = map.get(key);
            if (list == null) {
                list = new ArrayList<String>();
                map.put(key, list);
            }
            list.add(value);
        }
        public String get(String key) {
            List<String> list = map.get(key);
            if (list != null && list.size() > 0) return list.get(0);
            else return null;
        }
        public List<String> getAll(String key) {
            return map.get(key);
        }
    }
    
    protected static Logger log = Logger.getLogger(DoiService.class);
    
    private static final int GET = 1;
    private static final int PUT = 2;
    private static final int POST = 3;
    private static final int DELETE = 4;
    private static final int CONNECTIONS_PER_ROUTE = 8;
    
    private DefaultHttpClient httpclient = null;
    private String serviceBaseUrl = "http://doi.tern.org.au/index.php?r=api/";
    private String updateServiceEndpoint = null;
    private String mintServiceEndpoint = null;
    private String queryServiceEndpoint = null;
    private String username = "";
    private String password = "";

    public DoiService() throws Exception {
		String baseUrl = null;
        String username = "";
        String password = "";
        boolean doiEnabled = false;
        
		try {
			baseUrl = PropertyService.getProperty("tern.doi.baseurl");
            doiEnabled = Boolean.parseBoolean(PropertyService.getProperty("tern.doi.enabled"));
			username = PropertyService.getProperty("tern.doi.username");
			password = PropertyService.getProperty("tern.doi.password");
		} catch (PropertyNotFoundException e) {
			log.warn("Using default TERN DOI baseUrl");
		}
		if (!doiEnabled) {
			throw new Exception("TERN DOI Service is not enabled.");
		}
        init(baseUrl, username, password);
    }
    
    public DoiService(String baseUrl, String username, String password) throws Exception {
        init(baseUrl, username, password);
    }
    
    private void init(String baseUrl, String username, String password) {
        this.username = username;
        this.password = password;
        httpclient = createThreadSafeClientNoSSLCert();
        if (baseUrl != null) {
            serviceBaseUrl = baseUrl;
        }
        updateServiceEndpoint = serviceBaseUrl + "update";
        mintServiceEndpoint = serviceBaseUrl + "create";
        queryServiceEndpoint = serviceBaseUrl + "query";
    }
    
    public String mint(Metadata metadata) throws Exception {
        String target = metadata.get(Metadata.TARGET);
        if (target == null || target.isEmpty()) {
            throw new Exception("Metadata must contains TARGET URL.");
        }
        try {
            target = URLEncoder.encode(target, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        }
        String endpoint = mintServiceEndpoint + "&user_id=" + username + "&app_id=" + password + "&url=" + target;
        String body = serializeAsXML(metadata);
        //log.debug(body);
        byte[] response = sendRequest(POST, endpoint, body);
        String responseMsg = new String(response);
        log.debug(responseMsg);
        return parseIdentifierResponse(responseMsg, "<doi>(.*)</doi>");
    }
    
    public void update(String doi, Metadata metadata) throws Exception {
        String encodedDoi = "";
        String target = metadata.get(Metadata.TARGET);
        if (target == null || target.isEmpty()) {
            throw new Exception("Metadata must contains TARGET URL.");
        }
        try {
            encodedDoi = URLEncoder.encode(doi, "UTF-8");
            target = URLEncoder.encode(target, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        }
        String endpoint = updateServiceEndpoint + "&user_id=" + username + "&app_id=" + password + "&url=" + target + "&doi=" + encodedDoi;
        String body = serializeAsXML(doi, metadata);
        //log.debug(body);
        byte[] response = sendRequest(POST, endpoint, body);
        //String responseMsg = new String(response);
        //log.debug(responseMsg);
    }
    
    public String query(String landingUrl) throws Exception {
        if (landingUrl == null || landingUrl.isEmpty()) {
            throw new Exception("landingUrl must not be empty.");
        }
        try {
            landingUrl = URLEncoder.encode(landingUrl, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        }
        String endpoint = queryServiceEndpoint + "&user_id=" + username + "&app_id=" + password + "&url=" + landingUrl;
        byte[] response = sendRequest(GET, endpoint);
        String responseMsg = new String(response);
        log.debug(responseMsg);
        return responseMsg;
    }
    
    /**
     * Serialize a collection of metadata name/value pairs as an XML String value. If the
     * metadata is null, or if it has no entries, then return a null string.
     * @param metadata the Map of metadata name/value pairs
     * @return an DataCite formatted serialize String
     */
    private String serializeAsXML(String identifier, Metadata metadata) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        sb.append("<resource xmlns=\"http://datacite.org/schema/kernel-2.1\"");
        sb.append("          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
        sb.append("          xsi:schemaLocation=\"http://datacite.org/schema/kernel-2.1 http://schema.datacite.org/meta/kernel-2.1/metadata.xsd\">");
        sb.append("<identifier identifierType=\"DOI\">");
        sb.append(identifier);
        sb.append("</identifier>");
        sb.append("<creators>");
        for (String creator : metadata.getAll(Metadata.CREATOR)) {
            sb.append("<creator><creatorName>");
            sb.append(creator);
            sb.append("</creatorName></creator>");
        }
        sb.append("</creators>");
        sb.append("<titles><title>");
        sb.append(metadata.get(Metadata.TITLE));
        sb.append("</title></titles>");
        sb.append("<publisher>");
        sb.append(metadata.get(Metadata.PUBLISHER));
        sb.append("</publisher>");
        sb.append("<publicationYear>");
        sb.append(metadata.get(Metadata.PUBLICATION_YEAR));
        sb.append("</publicationYear>");
        String resourceType = metadata.get(Metadata.RESOURCE_TYPE);
        if (resourceType != null) {
            String[] resourceTypes = resourceType.split("/", 2);
            sb.append("<resourceType resourceTypeGeneral=\"");
            sb.append(resourceTypes[0]);
            sb.append("\">");
            if (resourceTypes.length > 1) sb.append(resourceTypes[1]);
            sb.append("</resourceType>");
        }
        String size = metadata.get(Metadata.SIZE);
        if (size != null) {
            sb.append("<sizes><size>");
            sb.append(size);
            sb.append("</size></sizes>");
        }
        String format = metadata.get(Metadata.FORMAT);
        if (format != null) {
            sb.append("<formats><format>");
            sb.append(format);
            sb.append("</format></formats>");
        }        
        sb.append("</resource>");
        
        String result = "xml=";
        //log.debug(sb.toString());
        try {
            result += URLEncoder.encode(sb.toString(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        }
        
        return result;
    }
    
    private String serializeAsXML(Metadata metadata) {
        return serializeAsXML("", metadata);
    }

    /**
     * Generate an HTTP Client for communicating with web services that is
     * thread safe and can be used in the context of a multi-threaded application.
     * @return DefaultHttpClient
     */
    private static DefaultHttpClient createThreadSafeClientNoSSLCert()  {
        X509TrustManager tm = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {}
            public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {}
            public X509Certificate[] getAcceptedIssuers() { return null; }
        };
        DefaultHttpClient client = new DefaultHttpClient();
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, new TrustManager[]{tm}, null);
            SSLSocketFactory ssf = new SSLSocketFactory(ctx,SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            ClientConnectionManager ccm = client.getConnectionManager();
            SchemeRegistry sr = ccm.getSchemeRegistry();
            sr.register(new Scheme("https", 443, ssf));
            HttpParams params = client.getParams();
            ThreadSafeClientConnManager connManager = new ThreadSafeClientConnManager(sr);
            connManager.setDefaultMaxPerRoute(CONNECTIONS_PER_ROUTE);
            client = new DefaultHttpClient(connManager, params);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return client;
    }

    /**
     * Send an HTTP request to the TERN DOI service without a request body.
     * @param requestType the type of the service as an integer
     * @param uri endpoint to be accessed in the request
     * @return byte[] containing the response body
     */
    private byte[] sendRequest(int requestType, String uri) throws Exception {
        return sendRequest(requestType, uri, null);
    }
    
    /**
     * Send an HTTP request to the TERN DOI service with a request body (for POST and PUT requests).
     * @param requestType the type of the service as an integer
     * @param uri endpoint to be accessed in the request
     * @param requestBody the String body to be encoded into the body of the request
     * @return byte[] containing the response body
     */
    private byte[] sendRequest(int requestType, String uri, String requestBody) throws Exception {
        HttpUriRequest request = null;
        //log.debug("Trying uri: " + uri);
        switch (requestType) {
        case GET:
            request = new HttpGet(uri);
            break;
        case PUT:
            request = new HttpPut(uri);
            if (requestBody != null && requestBody.length() > 0) {
                StringEntity myEntity = new StringEntity(requestBody, "UTF-8");
                ((HttpPut) request).setEntity(myEntity);
            }
            break;
        case POST:
            request = new HttpPost(uri);
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            if (requestBody != null && requestBody.length() > 0) {
                StringEntity myEntity = new StringEntity(requestBody, "UTF-8");
                ((HttpPost) request).setEntity(myEntity);
            }
            break;
        case DELETE:
            request = new HttpDelete(uri);
            break;
        default:
            throw new Exception("Unrecognized HTTP method requested.");
        }
        
        ResponseHandler<byte[]> handler = new ResponseHandler<byte[]>() {
            @Override
            public byte[] handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    return EntityUtils.toByteArray(entity);
                } else {
                    return null;
                }
            }
        };
        byte[] body = null;
        
        if (request != null) {
            request.setHeader("Accept", "application/xml");
            body = httpclient.execute(request, handler);
        }
        return body;
    }

    /**
     * Parse the response from TERN DOI and extract out the identifier that is returned
     * as part of the 'success' message.
     * @param responseMsg the response from EZID
     * @return the identifier from the message
     * @throws Exception if the response contains an error message
     */
    private String parseIdentifierResponse(String responseMsg, String doiPattern) throws Exception {
        Pattern p = Pattern.compile(doiPattern);
        Matcher m = p.matcher(responseMsg);
        if (m.find() && m.groupCount() > 0) {
            return m.group(1);
        } else {
            p = Pattern.compile("<verbosemessage>(.*)</verbosemessage>");
            m = p.matcher(responseMsg);
            String msg = "Unknown Error";
            if (m.find() && m.groupCount() > 0) msg = m.group(1);
            throw new Exception(msg);
        }
    }

}
