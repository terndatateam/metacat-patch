/**
 * Copyright 2014 Terrestrial Ecosystem Research Network (TERN) Licensed under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except in compliance with the License. You
 * may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */
package edu.ucsb.nceas.metacat.oaipmh.provider.server.crosswalk;

import ORG.oclc.oai.server.crosswalk.Crosswalk;
import ORG.oclc.oai.server.verb.CannotDisseminateFormatException;
import ORG.oclc.oai.server.verb.OAIInternalServerError;
//import edu.ucsb.nceas.metacat.util.SystemUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Convert native "item" to oai format. This class can be configured to support various metadata
 * formats. In this case, the native "item" is assumed to already be formatted as an OAI <record>
 * element, with the possible exception that multiple metadataFormats may be present in the
 * <metadata>
 * element. The "crosswalk", merely involves pulling out the one that is requested.
 *
 * @author Alvin Sebastian
 */
public class Eml2oai_any extends Crosswalk {

    /* Class fields */
    private static final Logger logger = Logger.getLogger(Eml2oai_any.class);
    private static final HashMap<String, Transformer> transformerCache = new HashMap<String, Transformer>();

    /* Instance fields */
    private HashMap<String, Transformer> transformers;

    /* Constructors */
    /**
     * The constructor assigns the schemaLocation associated with this crosswalk. Since the
     * crosswalk is trivial in this case, no properties are utilized.
     *
     * @param schemaLabel
     * @param properties properties that are needed to configure the crosswalk.
     * @throws ORG.oclc.oai.server.verb.OAIInternalServerError
     */
    public Eml2oai_any(String schemaLabel, Properties properties) throws OAIInternalServerError {
        super(properties.getProperty("oaipmh.SchemaLocation." + schemaLabel));
        String repositoryIdentifier = "";
        String serverUrl = "";
        String contextUrl = "";
        String servletUrl = "";
        try {
            repositoryIdentifier = properties.getProperty("oaipmh.repositoryIdentifier");
            serverUrl = properties.getProperty("server.url");
            contextUrl = serverUrl + '/' + properties.getProperty("application.context");
            servletUrl = contextUrl + "/metacat";
        } catch (Exception ex) {
        }
        String defaultLicence = properties.getProperty("tern.defaultLicence").toLowerCase();
        String licencesDirPath = properties.getProperty("tern.licencesDirPath");
        Object predefinedLicences = fetchLicenceInfo(licencesDirPath);
        String dirPath = properties.getProperty("oaipmh.xsltDirPath");
        String defaultXsltPath = (new File(dirPath, properties.getProperty("oaipmh.xsl." + schemaLabel))).getPath();
        logger.debug("-----------");
        logger.debug("schemaLabel: " + schemaLabel);
        logger.debug("licencesDirPath: " + licencesDirPath);
        logger.debug("defaultLicence: " + defaultLicence);
        //logger.debug("predefinedLicences: " + predefinedLicences);
        logger.debug("defaultXsltPath: " + defaultXsltPath);
        transformers = new HashMap<String, Transformer>();
        for (String pname : getPropertyNamesByGroup(properties, "xml.eml")) {
            String emlVersion = properties.getProperty(pname);
            String xsltFile = properties.getProperty("oaipmh.xsl." + schemaLabel + "." + emlVersion);
            String xsltPath = defaultXsltPath;
            if (xsltFile != null && !xsltFile.trim().isEmpty()) {
                xsltPath = (new File(dirPath, xsltFile)).getPath();
            }
            //logger.debug("pname: " + pname);
            logger.debug("emlVersion: " + emlVersion);
            logger.debug("xsltPath: " + xsltPath);

            Transformer transformer = transformerCache.get(xsltPath);
            if (transformer == null) {
                try {
                    TransformerFactory tFactory = TransformerFactory.newInstance();
                    StreamSource xslSource = new StreamSource(new File(xsltPath));
                    transformer = tFactory.newTransformer(xslSource);
                    transformer.setParameter("groupName", properties.getProperty("Identify.repositoryName"));
                    transformer.setParameter("repositoryIdentifier", repositoryIdentifier);
                    //transformer.setParameter("serverName", properties.getProperty("server.name"));
                    transformer.setParameter("serverUrl", serverUrl);
                    transformer.setParameter("contextUrl", contextUrl);
                    transformer.setParameter("servletUrl", servletUrl);
                    //transformer.setParameter("qformat", propertyService.getProperty("application.default-style"));
                    transformer.setParameter("predefinedLicences", predefinedLicences);
                    if (defaultLicence != null && !defaultLicence.isEmpty()) transformer.setParameter("defaultLicence", defaultLicence);
                    transformerCache.put(xsltPath, transformer);
                } catch (Exception e) {
                    throw new OAIInternalServerError(e.getMessage());
                }
            }

            logger.debug("transformer: " + transformer);
            transformers.put(emlVersion, transformer);
        }
        
    }

    /* Class methods */

    /**
     * Get a list of all property names that start with the groupName prefix.
     *
     * @param groupName the key prefix to look for.
     * @return ArrayList of property names
     */
    private static ArrayList<String> getPropertyNamesByGroup(Properties properties, String groupName) {
        ArrayList<String> groupKeySet = new ArrayList<String>();
        for (Object key : properties.keySet()) {
            String strKey = (String) key;
            if (strKey.startsWith(groupName)) {
                groupKeySet.add(strKey);
            }
        }
        return groupKeySet;
    }

    /* Instance methods */
    /**
     * Perform the actual crosswalk.
     *
     * @param nativeItem the native "item". In this case, it is already formatted as an OAI
     * <record> element, with the possible exception that multiple metadataFormats are present in
     * the <metadata> element.
     * @return a String containing the FileMap to be stored within the <metadata>
     * element.
     * @exception CannotDisseminateFormatException nativeItem doesn't support this format.
     */
    @Override
    public String createMetadata(Object nativeItem) throws CannotDisseminateFormatException {
        HashMap recordMap = (HashMap) nativeItem;
        try {
            String xmlRec = (String) recordMap.get("recordBytes");
            xmlRec = xmlRec.trim();

            if (xmlRec.startsWith("<?")) {
                int offset = xmlRec.indexOf("?>");
                xmlRec = xmlRec.substring(offset + 2);
            }
            String emlNamespace = (String) recordMap.get("doctype");
            logger.debug("emlNamespace: " + emlNamespace);

            Transformer transformer = transformers.get(emlNamespace);
            logger.debug("transformer: " + transformer);

            StringReader stringReader = new StringReader(xmlRec);
            StreamSource streamSource = new StreamSource(stringReader);
            StringWriter stringWriter = new StringWriter();
            synchronized (transformer) {
                transformer.setParameter("emlNamespace", emlNamespace);
                //transformer.setParameter("doi", recordMap.get("doi"));
                transformer.setParameter("lastModified", recordMap.get("lastModified"));
                transformer.setParameter("dateCreated", recordMap.get("dateCreated"));
                transformer.transform(streamSource, new StreamResult(stringWriter));
            }
            return stringWriter.toString();
        } catch (Exception e) {
            throw new CannotDisseminateFormatException(e.getMessage());
        }
    }

    /**
     * Can this nativeItem be represented in this format?
     *
     * @param nativeItem a record in native format
     * @return true if the requested format is possible, false otherwise.
     */
    @Override
    public boolean isAvailableFor(Object nativeItem) {
        HashMap recordMap = (HashMap) nativeItem;
        String doctype = (String) recordMap.get("doctype");
        logger.debug("isAvailableFor doctype: " + doctype);
        return transformers.containsKey(doctype);
    }

    /**
     * Replaces the licence code (eg TERN_BY) with an extended textual description.
     */
    private Object fetchLicenceInfo(String licencesDirPath) {
        Object element = new String();
        try {
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("licences");
            doc.appendChild(rootElement);
            File dir = new File(licencesDirPath);
            for (File f : dir.listFiles()) {
                if (f.isFile() && f.getName().endsWith(".txt")) {
                    String licenceType = FilenameUtils.getBaseName(f.getName()).toLowerCase();
                    String content = FileUtils.readFileToString(f);
                    String[] lines = content.split("\\r?\\n");
                    Element licence = doc.createElement(licenceType);
                    rootElement.appendChild(licence);
                    licence.setAttribute("uri", lines[0]);
                    licence.setAttribute("name", lines[1]);
                    licence.appendChild(doc.createTextNode(lines[2]));
                }
            }
//            DOMImplementationLS domImplementation = (DOMImplementationLS) doc.getImplementation();
//            LSSerializer lsSerializer = domImplementation.createLSSerializer();
//            logger.debug(lsSerializer.writeToString(doc));
            element = doc.getDocumentElement();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        return element;
    }
    
    
}
